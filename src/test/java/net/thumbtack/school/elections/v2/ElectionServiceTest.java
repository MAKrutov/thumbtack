package net.thumbtack.school.elections.v2;

import com.google.gson.Gson;
import net.thumbtack.school.elections.v2.models.Person;
import net.thumbtack.school.elections.v2.request.election.*;
import net.thumbtack.school.elections.v2.response.election.*;
import net.thumbtack.school.elections.v2.service.ElectionService;
import org.junit.Test;

import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

public class ElectionServiceTest extends TestBase {

    private static final double DOUBLE_EPS = 1E-6;
    Gson gson = new Gson();
    ElectionService electionService = new ElectionService();

    @Test
    public void addCandidateTest() {
        String personToken = insertPerson(vika);
        AddCandidateDtoRequest addCandidateDtoRequest = new AddCandidateDtoRequest(personToken, personToken);
        insertOffer(personToken, offer1);
        assertEquals(0, electionDao.getCandidatesWithProgram().size());
        electionService.addCandidate(gson.toJson(addCandidateDtoRequest));
        candidateDao.includeOfferToProgram(offer1.getOfferString(), personToken);
        assertEquals(1, electionDao.getCandidatesWithProgram().size());

    }

    @Test
    public void makeOffer() {
        String offerorToken = insertPerson(vika);
        OfferDtoRequest offerDtoRequest = new OfferDtoRequest(offerorToken, "I WANNA ROCK");
        String jsonRequest = gson.toJson(offerDtoRequest);
        electionService.makeOffer(jsonRequest);
        assertEquals(electionDao.getAllOffers().size(), 1);
    }

    @Test
    public void raitOfferTest() {
        String offerorToken = insertPerson(vika);
        String raiterToken = insertPerson(offeror);
        insertOffer(offerorToken, offer1);
        assertEquals(1, electionDao.getAllOffers().iterator().next().getRateList().size());

        RaitOfferDtoRequest raitOfferDtoRequest = new RaitOfferDtoRequest(offer1.getOfferString(), raiterToken, 5);
        String jsonRequest = gson.toJson(raitOfferDtoRequest);
        electionService.raitOffer(jsonRequest);
        assertEquals(2, electionDao.getAllOffers().iterator().next().getRateList().size());
        assertEquals(5, electionDao.getAllOffers().iterator().next().getRating(), DOUBLE_EPS);

        //rerateOffer
        raitOfferDtoRequest = new RaitOfferDtoRequest(offer1.getOfferString(), raiterToken, 1);
        jsonRequest = gson.toJson(raitOfferDtoRequest);
        electionService.raitOffer(jsonRequest);
        assertEquals(2, electionDao.getAllOffers().iterator().next().getRateList().size());
        assertEquals(3, electionDao.getAllOffers().iterator().next().getRating(), DOUBLE_EPS);

        RemoveRaitingDtoRequest removeRaitingDtoRequest = new RemoveRaitingDtoRequest(offer1.getOfferString(), raiterToken);
        electionService.removeRaiting(gson.toJson(removeRaitingDtoRequest));
        assertEquals(1, electionDao.getAllOffers().iterator().next().getRateList().size());
        assertEquals(5, electionDao.getAllOffers().iterator().next().getRating(), DOUBLE_EPS);

    }


    @Test
    public void getOffersByAuthors() {
        String token1 = insertPerson(petr),
                token2 = insertPerson(ivan),
                token3 = insertPerson(vika);
        Set<Person> personSet = new HashSet<>();
        personSet.add(petr);
        personSet.add(ivan);
        personSet.add(vika);
        insertOffer(token1, offer1);
        insertOffer(token2, offer2);
        insertOffer(token3, offer3);
        GetOfferWithAuthorDtoRequest getOfferWithAuthorDtoRequest = new GetOfferWithAuthorDtoRequest(personSet, token1);
        String jsonRequest = gson.toJson(getOfferWithAuthorDtoRequest);
        String jsonResponse = electionService.getOffersByAuthors(jsonRequest);
        GetOfferWithAuthorDtoResponse response = gson.fromJson(jsonResponse, GetOfferWithAuthorDtoResponse.class);
        assertEquals(3, response.getOfferAuthorMap().size());
        assertEquals(offer1.getOfferString(), response.getOfferAuthorMap().get(petr.getFIO()).get(0));
        assertEquals(offer2.getOfferString(), response.getOfferAuthorMap().get(ivan.getFIO()).get(0));
        assertEquals(offer3.getOfferString(), response.getOfferAuthorMap().get(vika.getFIO()).get(0));

    }


    @Test
    public void getAllOffersDescendantTest() {
        String token1 = insertPerson(petr),
                token2 = insertPerson(ivan),
                token3 = insertPerson(vika);
        insertOffer(token1, offer1);
        insertOffer(token2, offer2);
        insertOffer(token3, offer3);
        electionDao.rateOffer(offer1.getOfferString(), 1, token2);
        electionDao.rateOffer(offer1.getOfferString(), 1, token3);
        electionDao.rateOffer(offer2.getOfferString(), 5, token1);
        electionDao.rateOffer(offer2.getOfferString(), 5, token3);
        electionDao.rateOffer(offer3.getOfferString(), 3, token1);
        electionDao.rateOffer(offer3.getOfferString(), 3, token2);
        GetOfferSortedDtoResponse getOfferSortedDtoResponse = gson.fromJson(electionService.getAllOffersDescendant(), GetOfferSortedDtoResponse.class);
        List<String> offersResponse = getOfferSortedDtoResponse.getOfferList();
        offersResponse.get(0).equals(offer2.getOfferString());
        offersResponse.get(1).equals(offer3.getOfferString());
        offersResponse.get(2).equals(offer1.getOfferString());
    }

    @Test
    public void getCandidatesWithProgram() {
        String candidateToken1 = insertPerson(candidate);
        vika.setCandidate(true);
        String candidateToken2 = insertPerson(vika);
        electionDao.moveToCandidates(candidateToken2);
        insertOffer(candidateToken1, offer1);
        insertOffer(candidateToken1, offer2);
        insertOffer(candidateToken1, offer3);
        insertOffer(candidateToken2, offer4);
        Set<String> offersSet1 = new HashSet<>(), offersSet2 = new HashSet<>();
        offersSet1.add(offer1.getOfferString());
        offersSet1.add(offer2.getOfferString());
        offersSet1.add(offer3.getOfferString());

        offersSet2.add(offer2.getOfferString());
        offersSet2.add(offer4.getOfferString());

        candidateDao.includeOfferToProgram(offer1.getOfferString(), candidateToken1);
        candidateDao.includeOfferToProgram(offer2.getOfferString(), candidateToken1);
        candidateDao.includeOfferToProgram(offer3.getOfferString(), candidateToken1);

        candidateDao.includeOfferToProgram(offer2.getOfferString(), candidateToken2);
        candidateDao.includeOfferToProgram(offer4.getOfferString(), candidateToken2);

        Set<Person> candidates = new HashSet<>();
        candidates.add(vika);
        candidates.add(candidate);
        String responseString = electionService.getCandidatesWithProgram(gson.toJson(candidates));
        Map<String, Set<String>> response = gson.fromJson(responseString, GetCandidatesWithProgramDtoResponse.class).getResponse();
        assertEquals(response.get(candidate.getFIO()), offersSet1);
        assertEquals(response.get(vika.getFIO()), offersSet2);
    }

    @Test
    public void getActiveVoters() {
        String token1 = insertPerson(petr),
                token2 = insertPerson(ivan),
                token3 = insertPerson(vika);
        String responseString = electionService.getActiveVoters();
        GetActiveVotersDtoResponse getActiveVotersDtoResponse = gson.fromJson(responseString, GetActiveVotersDtoResponse.class);
        assertTrue(getActiveVotersDtoResponse.getResponse().contains(petr));
        assertTrue(getActiveVotersDtoResponse.getResponse().contains(ivan));
        assertTrue(getActiveVotersDtoResponse.getResponse().contains(vika));


        voterDao.logOut(token1);
        responseString = electionService.getActiveVoters();
        getActiveVotersDtoResponse = gson.fromJson(responseString, GetActiveVotersDtoResponse.class);
        assertFalse(getActiveVotersDtoResponse.getResponse().contains(petr));


    }


    @Test
    public void getWinner() {
        String token1 = insertPerson(petr),
                token2 = insertPerson(ivan),
                token3 = insertPerson(vika),
                token4 = insertPerson(raiter),
                token5 = insertPerson(offeror),
                candidateToken = insertPerson(candidate);
        electionDao.insertOffer(offer4, token5);
        candidateDao.includeOfferToProgram(offer4.getOfferString(), candidateToken);

        electionDao.vote(token1, candidateToken);
        electionDao.vote(token2, candidateToken);
        electionDao.vote(token3, candidateToken);
        electionDao.vote(token4, candidateToken);
        electionDao.vote(token5, candidateToken);

        String responseString = electionService.getWinner();
        WinnerDtoResponse winnerDtoResponse = gson.fromJson(responseString, WinnerDtoResponse.class);

        assertEquals(candidate, winnerDtoResponse.getResult());
    }
}
