package net.thumbtack.school.elections.v2;

import net.thumbtack.school.elections.v2.models.Person;
import org.apache.commons.collections4.bidimap.DualHashBidiMap;
import org.junit.Test;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

import static org.junit.Assert.*;

public class TestPersonOperations extends TestBase {

    @Test
    public void insert() {
        insertPerson("Вася", "Михайлович", "Пятеркин", "десятьзнаков", "девятьзнаков", "Московия", "22", "3", true);
        assertEquals(1, electionDao.getActiveVoters().size());
        insertPerson("Вася", "Михайлович", "Кликович", "сороктри", "сорокдва", "Apовия", "11", "13", true);
        assertEquals(2, electionDao.getActiveVoters().size());
    }

    @Test
    public void logIn() {
        Map<String, Person> tokensPersonMap = new DualHashBidiMap<>();
        tokensPersonMap.putAll(insertPerson("Вася", "Михайлович", "Пятеркин", "десятьзнаков", "девятьзнаков", "Московская", "22", "3", false));
        tokensPersonMap.putAll(insertPerson("Петя", "Петрович", "Кликовин", "сороктри", "сорокдва", "Ладонская", "12", "13", false));
        assertEquals(2, electionDao.getActiveVoters().size());

        Set<Person> persons = new HashSet<>(tokensPersonMap.values());
        Set<String> tokens = new HashSet<>(tokensPersonMap.keySet());

        for (String token : tokens) {
            voterDao.logOut(token);
        }
        assertEquals(0, electionDao.getActiveVoters().size());


        voterDao.logIn("десятьзнаков", "девятьзнаков", UUID.randomUUID().toString());

        Set<Person> personsFromDB = new HashSet<>(electionDao.getActiveVoters());

        assertTrue(persons.containsAll(personsFromDB));

    }

    @Test
    public void iWillTheCandidate() {
        String firstToken = insertPerson("Вася", "Михайлович", "Пятеркин", "десятьзнаков", "девятьзнаков", "Московия", "22", "3", false).keySet().iterator().next();
        Set<Person> persons = electionDao.getActiveVoters();
        for (Person person : persons) {
            assertFalse(person.isCandidate());
        }


        voterDao.iWillTheCandidate(firstToken);
        persons = electionDao.getActiveVoters();
        for (Person person : persons) {
            assertTrue(person.isCandidate());
        }
    }
}