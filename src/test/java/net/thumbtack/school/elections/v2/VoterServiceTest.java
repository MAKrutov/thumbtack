package net.thumbtack.school.elections.v2;

import com.google.gson.Gson;
import net.thumbtack.school.elections.v2.models.Person;
import net.thumbtack.school.elections.v2.request.election.RegisterVoterDtoRequest;
import net.thumbtack.school.elections.v2.request.voter.LogOutDtoRequest;
import net.thumbtack.school.elections.v2.request.voter.LoginDtoRequest;
import net.thumbtack.school.elections.v2.response.ErrorResponse;
import net.thumbtack.school.elections.v2.service.VoterService;
import org.apache.commons.collections4.bidimap.DualHashBidiMap;
import org.junit.Test;

import java.util.Map;

import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.assertEquals;

public class VoterServiceTest extends TestBase {

    private VoterService voterService = new VoterService();
    private Gson gson = new Gson();

    @Test

    public void registerVoter() {
        Person ivan = new Person("Иван", "Петрович", "Кликовин", "сороктри", "сорокдва", "Ладонская", "12", "13", false);
        Gson gson = new Gson();
        RegisterVoterDtoRequest request = new RegisterVoterDtoRequest("Margo", "Liza", "Ortyz", "1111111111", "2222222222", "Green street", "11", "14", false);
        String Json = gson.toJson(request);
        RegisterVoterDtoRequest request2 = new RegisterVoterDtoRequest("Antony", "Josh", "Simpson", "2222222222", "3333333333", "Basila street", "19", "14", false);
        String Json2 = gson.toJson(request2);
        RegisterVoterDtoRequest request3 = new RegisterVoterDtoRequest("Franc", "Liza", "Simpson", "33333333333", "1111111111", "Summer street", "22", "", false);
        String Json3 = gson.toJson(request3);
        RegisterVoterDtoRequest request4 = new RegisterVoterDtoRequest("Franc", "Liza", "Simpson", "777777777777", "888888888888", "Summer street", "22", "", true);
        String Json4 = gson.toJson(request4);
        voterService.registerVoter(Json);
        voterService.registerVoter(Json2);
        voterService.registerVoter(Json3);
        //register person again with other password and login
        try {
            voterService.registerVoter(Json4);
        } catch (RuntimeException ex) {
            assertEquals(ex.getMessage(), "Can't register");
        }

        assertEquals(3, electionDao.getActiveVoters().size());
    }

    @Test
    public void registerVoterWithRepeatedLoginAndPassword() {
        //Регистрация с повторяющимся логином и паролем
        VoterService voterService = new VoterService();
        Gson gson = new Gson();

        RegisterVoterDtoRequest request;
        request = new RegisterVoterDtoRequest("Margo", "Viktorian", "Simpson", "qwe111151rty", "Q1w32411ax",
                "Basila street", "19", "14", false);
        String Json = gson.toJson(request);
        voterService.registerVoter(Json);
        RegisterVoterDtoRequest request2 = new RegisterVoterDtoRequest("Josh", "Parting", "Simpson", "qwe111151rty", "17171771fbc",
                "Basila street", "19", "14", false);
        String Json2 = gson.toJson(request2);

        RegisterVoterDtoRequest request3 = new RegisterVoterDtoRequest("Franc", "Liza", "Simpson", "ttttttttttttt", "Q1w32411ax",
                "Basila street", "19", "14", false);
        String Json3 = gson.toJson(request3);

        String response2 = voterService.registerVoter(Json2),
                response3 = voterService.registerVoter(Json3);

        assertTrue(gson.fromJson(response2, ErrorResponse.class).getError().equals("Can't register"));
        assertTrue(gson.fromJson(response3, ErrorResponse.class).getError().equals("Can't register"));

    }


    @Test
    public void registerVoterWithErrors3() {
        //Повторная регистрация пользователя с другим логином и паролем
        VoterService voterService = new VoterService();
        Gson gson = new Gson();
        RegisterVoterDtoRequest request = new RegisterVoterDtoRequest("Margo", "Liza", "Ortyz", "1111111111", "2222222222", "Green street", "11", "14", false);
        String Json = gson.toJson(request);
        RegisterVoterDtoRequest request2 = new RegisterVoterDtoRequest("Antony", "Josh", "Simpson", "2222222222", "3333333333", "Basila street", "19", "14", false);
        String Json2 = gson.toJson(request2);
        RegisterVoterDtoRequest request3 = new RegisterVoterDtoRequest("Franc", "Liza", "Simpson", "33333333333", "1111111111", "Summer street", "22", "", false);
        String Json3 = gson.toJson(request3);
        RegisterVoterDtoRequest request4 = new RegisterVoterDtoRequest("Franc", "Liza", "Simpson", "777777777777", "888888888888", "Summer street", "22", "", true);
        String Json4 = gson.toJson(request4);
        voterService.registerVoter(Json);
        voterService.registerVoter(Json2);
        voterService.registerVoter(Json3);

        String response = voterService.registerVoter(Json4);
        assertTrue(gson.fromJson(response, ErrorResponse.class).getError().equals("Can't register"));

    }


    @Test
    public void testLoginLogout() {
        VoterService voterService = new VoterService();
        Gson gson = new Gson();

        Map<String, Person> tokensPersonMap = new DualHashBidiMap<>();
        tokensPersonMap.putAll(
                insertPerson("Вася", "Михайлович", "Пятеркин", "десятьзнаков",
                        "девятьзнаков", "Московская", "22", "3", false));
        tokensPersonMap.putAll(
                insertPerson("Петя", "Петрович", "Кликовин", "сороктри",
                        "сорокдва", "Ладонская", "12", "13", false));
        String token = tokensPersonMap.keySet().iterator().next();
        LogOutDtoRequest logOutRequest = new LogOutDtoRequest(token);

        voterService.logOut(gson.toJson(logOutRequest));
        assertEquals(1, electionDao.getActiveVoters().size());

        String password = tokensPersonMap.get(token).getPassword(),
                login = tokensPersonMap.get(token).getLogin();

        LoginDtoRequest loginRequest = new LoginDtoRequest(login, password);
        voterService.logIn(gson.toJson(loginRequest));
        assertEquals(2, electionDao.getActiveVoters().size());
    }

}
