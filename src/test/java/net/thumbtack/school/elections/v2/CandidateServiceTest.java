package net.thumbtack.school.elections.v2;

import com.google.gson.Gson;
import net.thumbtack.school.elections.v2.request.candidate.AddOfferToProgramDtoRequest;
import net.thumbtack.school.elections.v2.request.candidate.RemoveOfferFromMyProgramDtoRequest;
import net.thumbtack.school.elections.v2.response.ErrorResponse;
import net.thumbtack.school.elections.v2.service.CandidateService;
import net.thumbtack.school.elections.v2.service.ServiceErrorCode;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class CandidateServiceTest extends TestBase {

    CandidateService candidateService = new CandidateService();
    Gson gson = new Gson();

    @Test
    public void candidateProgramTest() {
        String offerorToken = insertPerson(offeror);
        insertOffer(offerorToken, offer1);
        insertOffer(offerorToken, offer2);
        String candidateToken = insertPerson(candidate);
        AddOfferToProgramDtoRequest addOfferToProgramDtoRequest1 = new AddOfferToProgramDtoRequest(offer1.getOfferString(), candidateToken);
        AddOfferToProgramDtoRequest addOfferToProgramDtoRequest2 = new AddOfferToProgramDtoRequest(offer2.getOfferString(), candidateToken);
        candidateService.addOfferToMyProgram(gson.toJson(addOfferToProgramDtoRequest1));
        assertEquals(electionDao.getCandidatesWithProgram().get(candidate).size(), 1);
        candidateService.addOfferToMyProgram(gson.toJson(addOfferToProgramDtoRequest2));
        assertEquals(electionDao.getCandidatesWithProgram().get(candidate).size(), 2);

    }


    @Test
    public void candidateProgramRemoveOfferTest() {
        String offerorToken = insertPerson(offeror);
        insertOffer(offerorToken, offer1);
        insertOffer(offerorToken, offer2);
        String candidateToken = insertPerson(candidate);
        candidateDao.includeOfferToProgram(offer1.getOfferString(), candidateToken);
        candidateDao.includeOfferToProgram(offer2.getOfferString(), candidateToken);
        assertEquals(electionDao.getCandidatesWithProgram().get(candidate).size(), 2);
        RemoveOfferFromMyProgramDtoRequest removeOfferFromMyProgramDtoRequest1 = new RemoveOfferFromMyProgramDtoRequest(offer1.getOfferString(), candidateToken);
        candidateService.removeOfferFromMyProgram(gson.toJson(removeOfferFromMyProgramDtoRequest1));
        assertEquals(electionDao.getCandidatesWithProgram().get(candidate).size(), 1);

        RemoveOfferFromMyProgramDtoRequest removeOfferFromMyProgramDtoRequest2 = new RemoveOfferFromMyProgramDtoRequest(offer2.getOfferString(), candidateToken);
        candidateService.removeOfferFromMyProgram(gson.toJson(removeOfferFromMyProgramDtoRequest2));
        assertEquals(electionDao.getCandidatesWithProgram().get(candidate).size(), 0);
    }

    @Test
    public void candidateProgramRemoveOfferTestError() {
        ErrorResponse errorResponse = null;
        String candidateToken = insertPerson(candidate);
        insertOffer(candidateToken, offer1);
        insertOffer(candidateToken, offer2);
        candidateDao.includeOfferToProgram(offer1.getOfferString(), candidateToken);
        candidateDao.includeOfferToProgram(offer2.getOfferString(), candidateToken);
        assertEquals(electionDao.getCandidatesWithProgram().get(candidate).size(), 2);
        RemoveOfferFromMyProgramDtoRequest removeOfferFromMyProgramDtoRequest1 = new RemoveOfferFromMyProgramDtoRequest(offer1.getOfferString(), candidateToken);
        errorResponse = gson.fromJson(candidateService.removeOfferFromMyProgram(gson.toJson(removeOfferFromMyProgramDtoRequest1)), ErrorResponse.class);
        assertEquals(errorResponse.getError(), ServiceErrorCode.AUTHORS_BANS.getErrorString());

        RemoveOfferFromMyProgramDtoRequest removeOfferFromMyProgramDtoRequest2 = new RemoveOfferFromMyProgramDtoRequest(offer2.getOfferString(), candidateToken);
        errorResponse = gson.fromJson(candidateService.removeOfferFromMyProgram(gson.toJson(removeOfferFromMyProgramDtoRequest2)), ErrorResponse.class);
        assertEquals(electionDao.getCandidatesWithProgram().get(candidate).size(), 2);
        assertEquals(errorResponse.getError(), ServiceErrorCode.AUTHORS_BANS.getErrorString());
    }
}
