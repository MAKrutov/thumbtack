package net.thumbtack.school.elections.v2;

import net.thumbtack.school.elections.v2.models.Offer;
import org.junit.Test;

import java.util.Set;
import java.util.UUID;

import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

public class TestOfferOperations extends TestBase {

    private static final double DOUBLE_EPS = 1E-6;

    @Test
    public void addOffer() {
        String personToken = voterDao.insert(offeror, UUID.randomUUID().toString());
        insertOffer(personToken, offer1);
        insertOffer(personToken, offer2);
        insertOffer(personToken, offer3);
        insertOffer(personToken, offer4);
        Set<Offer> offersFromDB = electionDao.getAllOffers();
        assertEquals(4, offersFromDB.size());
    }

    @Test
    public void rateOffer() {
        String personToken = voterDao.insert(offeror, UUID.randomUUID().toString());
        insertOffer(personToken, offer1);
        insertOffer(personToken, offer2);
        insertOffer(personToken, offer3);
        insertOffer(personToken, offer4);

        String raiterToken = voterDao.insert(raiter, UUID.randomUUID().toString());
        electionDao.rateOffer(offer1.getOfferString(), 5, raiterToken);
        electionDao.rateOffer(offer2.getOfferString(), 4, raiterToken);
        electionDao.rateOffer(offer3.getOfferString(), 3, raiterToken);
        electionDao.rateOffer(offer4.getOfferString(), 2, raiterToken);
        Set<Offer> offersFromDB = electionDao.getAllOffers();

        for (Offer offer : offersFromDB) {
            assertFalse(offer.getRateList().isEmpty());
        }
        voterDao.logOut(personToken);
        voterDao.logOut(raiterToken);

        offersFromDB = electionDao.getAllOffers();

        for (Offer offer : offersFromDB) {
            assertTrue(offer.getRateList().isEmpty());
        }

    }

    @Test
    public void reRateOffer() {
        String personToken = voterDao.insert(offeror, UUID.randomUUID().toString());
        String raiterToken = voterDao.insert(raiter, UUID.randomUUID().toString());
        insertOffer(personToken, offer1);
        electionDao.rateOffer(offer1.getOfferString(), 5, raiterToken);
        Offer offer = electionDao.getAllOffers().iterator().next();
        assertEquals(5, offer.getRating(), DOUBLE_EPS);

        electionDao.rateOffer(offer1.getOfferString(), 1, raiterToken);
        offer = electionDao.getAllOffers().iterator().next();
        assertEquals(3, offer.getRating(), DOUBLE_EPS);
    }

    @Test
    public void includeOfferToProgram() {
        String personToken = voterDao.insert(offeror, UUID.randomUUID().toString());
        insertOffer(personToken, offer1);

        String raiterToken = voterDao.insert(raiter, UUID.randomUUID().toString());
        String candidateToken = voterDao.insert(candidate, UUID.randomUUID().toString());
        candidateDao.includeOfferToProgram(offer1.getOfferString(), candidateToken);
        electionDao.getCandidatesWithProgram().values().contains(electionDao.getAllOffers());

        Set<Offer> offersFromDB = electionDao.getAllOffers(),
                candidatesOffers = electionDao.getCandidatesWithProgram().get(candidate);

        assertEquals(offersFromDB.iterator().next(), candidatesOffers.iterator().next());

        candidateDao.removeOfferFromProgram(offer1.getOfferString(), candidateToken);
        assertTrue(electionDao.getCandidatesWithProgram().values().iterator().next().isEmpty());
    }


}
