package net.thumbtack.school.elections.v2;


import net.thumbtack.school.elections.v2.daoimpl.*;
import net.thumbtack.school.elections.v2.models.Offer;
import net.thumbtack.school.elections.v2.models.Person;
import net.thumbtack.school.elections.v2.utils.MyBatisUtils;
import org.junit.Assume;
import org.junit.Before;
import org.junit.BeforeClass;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import static org.junit.Assert.assertNotEquals;

public class TestBase {
    private static boolean setUpIsDone = false;
    protected CandidateDaoImpl candidateDao = new CandidateDaoImpl();
    protected ElectionDaoImpl electionDao = new ElectionDaoImpl();
    protected ServicesDaoImpl servicesDao = new ServicesDaoImpl();
    protected VoterDaoImpl voterDao = new VoterDaoImpl();
    private CommonDaoImpl commonDao = new CommonDaoImpl();

    protected Person ivan = new Person("Иван", "Иванович", "Иванов", "1111111111", "1111111111", "Ивановская", "1", "1", false),
            petr = new Person("Петр", "Петрович", "Петров", "2222222222", "2222222222", "Петровская", "1", "1", false),
            offeror = new Person("Вася", "Васильевич", "Пупкин", "3333333333", "3333333333", "Инициативная", "1", "1", false),
            raiter = new Person("Олег", "Олегович", "Олегов", "4444444444", "4444444444", "Оценочная", "1", "1", false),
            vika = new Person("Виктория", "Викторовна", "Весенняя", "5555555555", "5555555555", "Ивановская", "1", "1", false),
            candidate = new Person("Степан", "Васильевич", "Теркин", "6666666666", "6666666666", "Президентская", "1", "1", true);

    protected Offer offer1 = new Offer("Работу рабочим"),
            offer2 = new Offer("Больше снега летом"),
            offer3 = new Offer("Меньше тепла зимой"),
            offer4 = new Offer("Повысить пенсионный возраст");

    @BeforeClass()
    public static void setUp() {
        if (!setUpIsDone) {
            Assume.assumeTrue(MyBatisUtils.initSqlSessionFactory());
            setUpIsDone = true;
        }
    }

    @Before()
    public void clearDatabase() {
        commonDao.clear();
    }

    protected Map<String, Person> insertPerson(String firstName, String middleName, String lastName, String login, String password, String adress, String home, String apartment, Boolean isCandidate) {
        Person person = new Person(0, firstName, lastName, middleName, login, password, adress, home, apartment, isCandidate);
        String token = UUID.randomUUID().toString();
        String response = voterDao.insert(person, token);
        assertNotEquals(0, person.getId());
        Map<String, Person> map = new HashMap();
        map.put(response, person);
        return map;
    }

    protected String insertPerson(Person person) {
        String token = UUID.randomUUID().toString();
        String response = voterDao.insert(person, token);
        return response;
    }

    protected void insertOffer(String token, Offer offer) {
        electionDao.insertOffer(offer, token);
    }
}
