package net.thumbtack.school.elections.v1.service;

import com.google.gson.Gson;
import net.thumbtack.school.elections.v1.Filler;
import net.thumbtack.school.elections.v1.database.DataBase;
import net.thumbtack.school.elections.v1.database.DataBaseErrorCode;
import net.thumbtack.school.elections.v1.models.Offer;
import net.thumbtack.school.elections.v1.request.election.*;
import net.thumbtack.school.elections.v1.request.voter.IWillTheCandidateRequest;
import net.thumbtack.school.elections.v1.request.voter.LogOutDtoRequest;
import net.thumbtack.school.elections.v1.response.ErrorResponse;
import net.thumbtack.school.elections.v1.response.election.*;
import org.junit.Test;

import java.util.Arrays;
import java.util.HashSet;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class ElectionServiceTest {
    int a = 100;
    private Filler filler = new Filler();
    private Gson gson = new Gson();
    private DataBase dataBase;
    @Test
    public void vote() {
        dataBase = filler.fillDataBase(a);
        ElectionService electionService = new ElectionService(dataBase);
        electionService.startElection();
        VoteDtoRequest voteDtoRequest;
        String json;
        int j = 1;
        int i = dataBase.getActiveTokens().size();
        while (i > 0) {
            if (i % 7 == 0) voteDtoRequest = new VoteDtoRequest(Integer.toString(j), "Against all");
            else
                voteDtoRequest = new VoteDtoRequest(Integer.toString(j), Integer.toString(a - i % 10));
            json = gson.toJson(voteDtoRequest);
            electionService.vote(json);
            j++;
            i--;
        }
        electionService.stopElection();
        WinnerDtoResponse response = gson.fromJson(electionService.getWinner(), WinnerDtoResponse.class);
        assertEquals("No winner", response.getResult());
    }

    @Test
    public void voteErrorTest() {
        dataBase = filler.fillDataBase(51);
        ElectionService electionService = new ElectionService(dataBase);
        VoteDtoRequest voteDtoRequest;
        String json;
        voteDtoRequest = new VoteDtoRequest(Integer.toString(50), Integer.toString(50));
        json = gson.toJson(voteDtoRequest);
        assertEquals(gson.toJson(new ErrorResponse(new ServiceException(ServiceErrorCode.ELECTIONS_NOT_STARTED))), electionService.vote(json));
        electionService.startElection();
        assertEquals(gson.toJson(new ErrorResponse(new ServiceException(ServiceErrorCode.VOTE_FOR_U))), electionService.vote(json));
    }

    @Test
    public void addCandidate() {
        dataBase = filler.fillDataBase(a);
        VoterService voterService = new VoterService(dataBase);
        ElectionService electionService = new ElectionService(dataBase);
        assertEquals(10, dataBase.getCandidates().size());
        assertEquals(90, dataBase.getVoters().size());
        String json = electionService.addCandidate(gson.toJson((new AddCandidateDtoRequest("1", "1"))));
        AddCandidateDtoResponse addCandidateDtoResponse = gson.fromJson(json, AddCandidateDtoResponse.class);
        assertEquals(11, dataBase.getCandidates().size());
        assertEquals(89, dataBase.getVoters().size());
        json = electionService.addCandidate(gson.toJson((new AddCandidateDtoRequest("10", "15"))));
        ErrorResponse errorResponse = gson.fromJson(json, ErrorResponse.class);
        assertEquals(DataBaseErrorCode.HE_DONT_WANT_TO_BE_THE_CANDIDATE.getErrorString(), errorResponse.getError());
        assertEquals(11, dataBase.getCandidates().size());
        assertEquals(89, dataBase.getVoters().size());
        voterService.iWillTheCandidateIfTheyWant(gson.toJson(new IWillTheCandidateRequest("15")));
        json = electionService.addCandidate(gson.toJson((new AddCandidateDtoRequest("10", "15"))));
        assertEquals(12, dataBase.getCandidates().size());
        assertEquals(88, dataBase.getVoters().size());
    }

    @Test
    public void makeOffer() {
        dataBase = filler.fillDataBase(15);
        ElectionService electionService = new ElectionService(dataBase);
        OfferDtoRequest offerDtoRequest;
        assertEquals(9, dataBase.getOfferMap().size());
        offerDtoRequest = new OfferDtoRequest("10", "Reduce taxes");
        String json = electionService.makeOffer(gson.toJson(offerDtoRequest));
        OfferDtoResponse response = gson.fromJson(json, OfferDtoResponse.class);
        assertEquals("Reduce taxes", response.getOffer().getOfferString());
        assertEquals(10, dataBase.getOfferMap().size());
    }

    @Test
    public void makeOfferError() {
        dataBase = filler.fillDataBase(15);
        ElectionService electionService = new ElectionService(dataBase);
        OfferDtoRequest offerDtoRequest;
        electionService.startElection();
        assertEquals(9, dataBase.getOfferMap().size());
        offerDtoRequest = new OfferDtoRequest("10", "Reduce taxes");
        String json = electionService.makeOffer(gson.toJson(offerDtoRequest));
        ErrorResponse response = gson.fromJson(json, ErrorResponse.class);
        assertEquals(ServiceErrorCode.ELECTIONS_STARTED.getErrorString(), response.getError());
    }

    @Test
    public void removeAddRaitingTest() {
        dataBase = filler.fillDataBase(15);
        ElectionService electionService = new ElectionService(dataBase);
        OfferDtoRequest offerDtoRequest;
        assertEquals(9, dataBase.getOfferMap().size());
        offerDtoRequest = new OfferDtoRequest("10", "Reduce taxes");
        String json = electionService.makeOffer(gson.toJson(offerDtoRequest));
        RaitOfferDtoRequest request;
        request = new RaitOfferDtoRequest("Reduce taxes", "1", 5);
        electionService.raitOffer(gson.toJson(request));
        request = new RaitOfferDtoRequest("Reduce taxes", "2", 4);
        electionService.raitOffer(gson.toJson(request));
        assertEquals(3, dataBase.getOfferMap().getKey("Reduce taxes").getSetOfRaiting().size());
        RemoveRaitingDtoRequest removeRaitingDtoRequest = new RemoveRaitingDtoRequest("Reduce taxes", "2");
        electionService.removeRaiting(gson.toJson(removeRaitingDtoRequest));
        assertEquals(2, dataBase.getOfferMap().getKey("Reduce taxes").getSetOfRaiting().size());
    }

    @Test
    public void raitOfferError() {
        dataBase = filler.fillDataBase(15);
        ElectionService electionService = new ElectionService(dataBase);
        OfferDtoRequest offerDtoRequest;
        assertEquals(9, dataBase.getOfferMap().size());
        offerDtoRequest = new OfferDtoRequest("10", "Reduce taxes");
        String json = electionService.makeOffer(gson.toJson(offerDtoRequest));
        RaitOfferDtoRequest request;
        request = new RaitOfferDtoRequest("Reduce taxes", "1", 10);
        String jsonError = electionService.raitOffer(gson.toJson(request));
        ErrorResponse error = gson.fromJson(jsonError, ErrorResponse.class);
        assertEquals(ServiceErrorCode.WRONG_RAITING.getErrorString(), error.getError());
        request = new RaitOfferDtoRequest("Reduce taxes", "10", 4);
        jsonError = electionService.raitOffer(gson.toJson(request));
        error = gson.fromJson(jsonError, ErrorResponse.class);
        assertEquals(DataBaseErrorCode.WRONG_OPERATION.getErrorString(), error.getError());
    }

    @Test
    public void getOffersByAuthors() {
        dataBase = filler.fillDataBase(15);
        ElectionService electionService = new ElectionService(dataBase);
        String response;
        GetOfferWithAuthorDtoRequest gowar = new GetOfferWithAuthorDtoRequest(new HashSet<>(Arrays.asList("1")), "10");
        response = electionService.getOffersByAuthors(gson.toJson(gowar));
        GetOfferWithAuthorDtoResponse gowarResponse = gson.fromJson(response, GetOfferWithAuthorDtoResponse.class);
        assertEquals(gowarResponse.getOfferAuthorMap().get("1").size(), dataBase.getAuthorOfferMap().get("1").size());

    }

    @Test
    public void getAllOffersDescendant() {
        dataBase = filler.fillDataBase(15);
        ElectionService electionService = new ElectionService(dataBase);
        assertEquals(9, dataBase.getOfferMap().size());
        OfferDtoRequest offerDtoRequest = new OfferDtoRequest("10", "Reduce taxes");
        String json = electionService.makeOffer(gson.toJson(offerDtoRequest));
        assertEquals(10, dataBase.getOfferMap().size());
        for (String string : dataBase.getActiveTokens()) {
            RaitOfferDtoRequest request;
            request = new RaitOfferDtoRequest("Reduce taxes", string, 1);
            String jsonResponse = electionService.raitOffer(gson.toJson(request));
            if (string.equals("10")) {
                assertTrue(jsonResponse.contains(DataBaseErrorCode.WRONG_OPERATION.getErrorString()));
            }
        }
        Offer offer = dataBase.getOfferMap().getKey("Reduce taxes");
        GetOfferSortedDtoResponse gosResponse = gson.fromJson(electionService.getAllOffersDescendant(), GetOfferSortedDtoResponse.class);
        assertEquals(9, gosResponse.getOfferList().indexOf(offer));
    }

    @Test
    public void getCandidatesWithProgram() {
        dataBase = filler.fillDataBase(15);
        ElectionService electionService = new ElectionService(dataBase);
        GetCandidatesWithProgramDtoRequest gcwpRequest = new GetCandidatesWithProgramDtoRequest(new HashSet<String>(Arrays.asList("15")), "1");
        String response;
        GetCandidatesWithProgramDtoResponse gcwpResponse;
        response = electionService.getCandidatesWithProgram(gson.toJson(gcwpRequest));
        gcwpResponse = gson.fromJson(response, GetCandidatesWithProgramDtoResponse.class);
        assertEquals(8, gcwpResponse.getResponse().get("15").size());
    }

    @Test
    public void getActiveVoters() {
        dataBase = filler.fillDataBase(20);
        ElectionService electionService = new ElectionService(dataBase);
        LogOutDtoRequest logOutRequest;
        VoterService voterService = new VoterService(dataBase);
        for (String string : dataBase.getVoters().values()) {
            logOutRequest = new LogOutDtoRequest(string);
            voterService.logOut(gson.toJson(logOutRequest));
        }
        GetActiveVotersDtoResponse gavResponse = gson.fromJson(electionService.getActiveVoters(), GetActiveVotersDtoResponse.class);
        assertEquals(0, gavResponse.getResponse().size());
    }

}