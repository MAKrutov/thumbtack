package net.thumbtack.school.elections.v1.service;

import com.google.gson.Gson;
import net.thumbtack.school.elections.v1.Filler;
import net.thumbtack.school.elections.v1.database.DataBase;
import net.thumbtack.school.elections.v1.request.candidate.AddOfferToProgramDtoRequest;
import net.thumbtack.school.elections.v1.request.candidate.MakeMeAVoterDtoRequest;
import net.thumbtack.school.elections.v1.request.candidate.RemoveOfferFromMyProgramDtoRequest;
import org.junit.Test;

import java.util.HashSet;
import java.util.Set;

import static junit.framework.TestCase.assertEquals;

public class CandidateServiceTest {

    Gson gson = new Gson();

    @Test
    public void addOfferToMyProgram() {
        Filler filler = new Filler();
        DataBase dataBase = filler.fillDataBase(35);
        CandidateService candidateService = new CandidateService(dataBase);
        assertEquals(9, dataBase.getOfferMap().size());
        assertEquals(24, dataBase.getCandidateProgramMap().size());
        int i = 1;
        for (String token : dataBase.getCandidates().values()) {
            AddOfferToProgramDtoRequest addOfferToProgramDtoRequest = new AddOfferToProgramDtoRequest(("Build " + i + " factory(s)"), token);
            String json = gson.toJson(addOfferToProgramDtoRequest);
            candidateService.addOfferToMyProgram(json);
            i++;
        }
        assertEquals(12, dataBase.getOfferMap().size());
        assertEquals(27, dataBase.getCandidateProgramMap().size());
    }

    @Test
    public void makeMeAVoter() {
        Filler filler = new Filler();
        DataBase dataBase = filler.fillDataBase(40);
        CandidateService candidateService = new CandidateService(dataBase);
        Set<String> buffSet = new HashSet<>(dataBase.getCandidates().values());
        assertEquals(32, dataBase.getCandidateProgramMap().size());
        assertEquals(4, dataBase.getCandidates().size());
        for (String token : buffSet) {
            MakeMeAVoterDtoRequest makeMeAVoterDtoRequest = new MakeMeAVoterDtoRequest(token);
            String json = gson.toJson(makeMeAVoterDtoRequest);
            candidateService.makeMeAVoter(json);
        }
        assertEquals(0, dataBase.getCandidateProgramMap().size());
        assertEquals(0, dataBase.getCandidates().size());
    }

    @Test
    public void removeOfferFromProgram() {
        Filler filler = new Filler();
        DataBase dataBase = filler.fillDataBase(30);
        CandidateService candidateService = new CandidateService(dataBase);
        assertEquals(9, dataBase.getOfferMap().size());
        assertEquals(24, dataBase.getCandidateProgramMap().size());
        int i = 1;
        for (String token : dataBase.getCandidates().values()) {
            RemoveOfferFromMyProgramDtoRequest removeOfferFromMyProgramDtoRequest = new RemoveOfferFromMyProgramDtoRequest("Build more shcools", token);
            String json = gson.toJson(removeOfferFromMyProgramDtoRequest);
            candidateService.removeOfferFromMyProgram(json);
            i++;
        }
        assertEquals(9, dataBase.getOfferMap().size());
        assertEquals(21, dataBase.getCandidateProgramMap().size());
    }
}