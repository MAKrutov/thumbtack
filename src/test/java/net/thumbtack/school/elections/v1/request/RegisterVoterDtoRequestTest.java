package net.thumbtack.school.elections.v1.request;

import com.google.gson.Gson;
import net.thumbtack.school.elections.v1.database.DataBase;
import net.thumbtack.school.elections.v1.request.election.RegisterVoterDtoRequest;
import net.thumbtack.school.elections.v1.response.ErrorResponse;
import net.thumbtack.school.elections.v1.service.ServiceErrorCode;
import net.thumbtack.school.elections.v1.service.ServiceException;
import net.thumbtack.school.elections.v1.service.VoterService;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class RegisterVoterDtoRequestTest {

    @Test
    public void registerVoterWithErrors() {
        //исключения брошены во время проверки экземпляра DTO на корректность введенных данных
        Gson gson = new Gson();
        VoterService voterService = new VoterService(new DataBase());
        RegisterVoterDtoRequest request = new RegisterVoterDtoRequest("Margo", "Liza", "Simpson", "12", "Q1w324ax", "Basila street", "19", "14", false);
        String Json = gson.toJson(request);
        assertEquals(gson.toJson(new ErrorResponse(new ServiceException(ServiceErrorCode.WRONG_LOGIN))), voterService.registerVoter(Json));

        request = new RegisterVoterDtoRequest("Mary", "Liza", "Simpson", "qwer2222ty", "123", "Basila street", "19", "14", false);
        Json = gson.toJson(request);
        assertEquals(gson.toJson(new ErrorResponse(new ServiceException(ServiceErrorCode.WEEK_PASSWORD))), voterService.registerVoter(Json));

        request = new RegisterVoterDtoRequest("", "Liza", "Simpson", "qwer1414ty", "Q1w324ax", "Basila street", "19", "14", false);
        Json = gson.toJson(request);
        assertEquals(gson.toJson(new ErrorResponse(new ServiceException(ServiceErrorCode.WRONG_FIRST_NAME))), voterService.registerVoter(Json));

        request = new RegisterVoterDtoRequest("Margo", "", "Simpson", "qwer444ty", "Q1w324ax", "Basila street", "19", "14", false);
        Json = gson.toJson(request);
        assertEquals(gson.toJson(new ErrorResponse(new ServiceException(ServiceErrorCode.WRONG_LAST_NAME))), voterService.registerVoter(Json));
    }

    @Test
    public void registerVoterClean() {
        DataBase dataBase = new DataBase();
        VoterService voterService = new VoterService(dataBase);
        Gson gson = new Gson();
        RegisterVoterDtoRequest request = new RegisterVoterDtoRequest("Margo", "Liza", "Simpson", "qwe111515rty", "Q1w3222411ax", "Basila street", "19", "14", false);
        String Json = gson.toJson(request);
        RegisterVoterDtoRequest request2 = new RegisterVoterDtoRequest("Antony", "Liza", "Simpson", "qwe111r22ty", "Q1w12512332411ax", "Basila street", "19", "14", false);
        String Json2 = gson.toJson(request2);
        RegisterVoterDtoRequest request3 = new RegisterVoterDtoRequest("Franc", "Liza", "Simpson", "qwe111151rty", "Q1w32411ax", "Basila street", "19", "14", false);
        String Json3 = gson.toJson(request3);
        voterService.registerVoter(Json);
        voterService.registerVoter(Json2);
        voterService.registerVoter(Json3);
        assertEquals(3, dataBase.getActiveTokens().size());
    }

}