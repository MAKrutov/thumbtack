package net.thumbtack.school.elections.v1.service;

import com.google.gson.Gson;
import net.thumbtack.school.elections.v1.Filler;
import net.thumbtack.school.elections.v1.database.DataBase;
import net.thumbtack.school.elections.v1.database.DataBaseErrorCode;
import net.thumbtack.school.elections.v1.database.DataBaseException;
import net.thumbtack.school.elections.v1.request.election.RegisterVoterDtoRequest;
import net.thumbtack.school.elections.v1.request.voter.LogOutDtoRequest;
import net.thumbtack.school.elections.v1.request.voter.LoginDtoRequest;
import net.thumbtack.school.elections.v1.response.ErrorResponse;
import net.thumbtack.school.elections.v1.response.election.RegisterVoterDtoResponse;
import net.thumbtack.school.elections.v1.response.voter.LoginDtoResponse;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

public class VoterServiceTest {
    private static final double DOUBLE_EPS = 1E-6;
    Gson gson = new Gson();

    @Test
    public void testRegisterVoter() {
        DataBase dataBase = new DataBase();
        VoterService voterService = new VoterService(dataBase);
        Gson gson = new Gson();
        Filler filler = new Filler();
        dataBase = filler.fillDataBase(18);
        assertEquals(18, dataBase.getActiveTokens().size());
    }


    @Test
    public void registerVoterWithRepeatedLoginAndPassword() {
        //Регистрация с повторяющимся логином и паролем
        VoterService voterService = new VoterService(new DataBase());
        Gson gson = new Gson();
        String Json;
        RegisterVoterDtoRequest request;
        request = new RegisterVoterDtoRequest("Margo", "Viktorian", "Simpson", "qwe111151rty", "Q1w32411ax", "Basila street", "19", "14", false);
        Json = gson.toJson(request);
        voterService.registerVoter(Json);
        RegisterVoterDtoRequest request2 = new RegisterVoterDtoRequest("Josh", "Parting", "Simpson", "qwe111151rty", "17171771fbc", "Basila street", "19", "14", false);
        String Json2 = gson.toJson(request2);
        RegisterVoterDtoRequest request3 = new RegisterVoterDtoRequest("Franc", "Liza", "Simpson", "ttttttttttttt", "Q1w32411ax", "Basila street", "19", "14", false);
        String Json3 = gson.toJson(request3);
        assertEquals(gson.toJson(new ErrorResponse(new DataBaseException(DataBaseErrorCode.REPEATED_LOGIN))), voterService.registerVoter(Json2));
        assertEquals(gson.toJson(new ErrorResponse(new DataBaseException(DataBaseErrorCode.REPEATED_PASSWORD))), voterService.registerVoter(Json3));
    }


    @Test
    public void registerVoterWithErrors3() {
        //Повторная регистрация пользователя с другим логином и паролем
        VoterService voterService = new VoterService(new DataBase());
        Gson gson = new Gson();
        RegisterVoterDtoRequest request = new RegisterVoterDtoRequest("Margo", "Liza", "Ortyz", "1111111111", "2222222222", "Green street", "11", "14", false);
        String Json = gson.toJson(request);
        RegisterVoterDtoRequest request2 = new RegisterVoterDtoRequest("Antony", "Josh", "Simpson", "2222222222", "3333333333", "Basila street", "19", "14", false);
        String Json2 = gson.toJson(request2);
        RegisterVoterDtoRequest request3 = new RegisterVoterDtoRequest("Franc", "Liza", "Simpson", "33333333333", "1111111111", "Summer street", "22", "", false);
        String Json3 = gson.toJson(request3);
        RegisterVoterDtoRequest request4 = new RegisterVoterDtoRequest("Franc", "Liza", "Simpson", "777777777777", "888888888888", "Summer street", "22", "", true);
        String Json4 = gson.toJson(request4);
        voterService.registerVoter(Json);
        voterService.registerVoter(Json2);
        voterService.registerVoter(Json3);
        assertEquals(gson.toJson(new ErrorResponse(new DataBaseException(DataBaseErrorCode.USER_EXSISTS))), voterService.registerVoter(Json4));
    }


    @Test
    public void testLoginLogout() {
        DataBase dataBase = new DataBase();
        VoterService voterService = new VoterService(dataBase);
        Gson gson = new Gson();
        RegisterVoterDtoRequest request = new RegisterVoterDtoRequest("Margo", "Liza", "Ortyz", "1111111111", "2222222222", "Green street", "11", "14", false);
        String Json = gson.toJson(request);
        RegisterVoterDtoRequest request2 = new RegisterVoterDtoRequest("Antony", "Josh", "Simpson", "2222222222", "3333333333", "Basila street", "19", "14", false);
        String Json2 = gson.toJson(request2);
        RegisterVoterDtoRequest request3 = new RegisterVoterDtoRequest("Franc", "Liza", "Simpson", "33333333333", "1111111111", "Summer street", "22", "", false);
        String Json3 = gson.toJson(request3);
        RegisterVoterDtoResponse registerVoterDtoResponse = gson.fromJson(voterService.registerVoter(Json), RegisterVoterDtoResponse.class);
        voterService.registerVoter(Json2);
        voterService.registerVoter(Json3);
        assertEquals(3, dataBase.getActiveTokens().size());
        LogOutDtoRequest logOutDtoRequest = new LogOutDtoRequest(registerVoterDtoResponse.getToken());
        voterService.logOut(gson.toJson(logOutDtoRequest));
        assertEquals(2, dataBase.getActiveTokens().size());
        LoginDtoRequest login = new LoginDtoRequest("1111111111", "2222222222");
        LoginDtoResponse loginDtoResponse = gson.fromJson(voterService.logIn(gson.toJson(login)), LoginDtoResponse.class);
        assertEquals(3, dataBase.getActiveTokens().size());
        assertNotEquals(loginDtoResponse.getToken(), registerVoterDtoResponse.getToken());
    }

}