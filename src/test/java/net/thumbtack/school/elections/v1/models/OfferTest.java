package net.thumbtack.school.elections.v1.models;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class OfferTest {
    private static final double DOUBLE_EPS = 1E-6;

    @Test
    public void testGetMiddleValue() {
        Offer offer = new Offer("Cut woods", "Obama");
        offer.addRaitingWithRaiter("Nickolas", 5);
        offer.addRaitingWithRaiter("Nickol", 5);
        offer.addRaitingWithRaiter("Nolan", 5);
        offer.addRaitingWithRaiter("Ivan", 5);
        offer.addRaitingWithRaiter("Eva", 5);
        assertEquals("Obama", offer.getAuthor());
        assertEquals("Cut woods", offer.getOfferString());
        assertEquals(5, offer.getRaiting(), DOUBLE_EPS);
        assertEquals(6, offer.getSetOfRaiting().size());

    }

    @Test
    public void addRaitingWithRaiter() {
        Offer offer = new Offer("Cut woods", "Obama");
        offer.addRaitingWithRaiter("Nickolas", 5);
        offer.addRaitingWithRaiter("Nickolas", 1);
        assertEquals("Obama", offer.getAuthor());
        assertEquals("Cut woods", offer.getOfferString());
        assertEquals(3, offer.getRaiting(), DOUBLE_EPS);
        assertEquals(2, offer.getSetOfRaiting().size());
    }

    @Test
    public void getMiddleValue() {
        Offer offer = new Offer("Cut woods", "Obama");
        offer.addRaitingWithRaiter("Nickolas", 4);
        offer.addRaitingWithRaiter("Nickol", 3);
        offer.addRaitingWithRaiter("Nolan", 2);
        offer.addRaitingWithRaiter("Ivan", 1);
        assertEquals("Obama", offer.getAuthor());
        assertEquals("Cut woods", offer.getOfferString());
        assertEquals(3, offer.getRaiting(), DOUBLE_EPS);
    }

    @Test
    public void deleteRaitWithRaiter() {
        Offer offer = new Offer("Cut woods", "Obama");
        offer.addRaitingWithRaiter("Nickolas", 4);
        offer.addRaitingWithRaiter("Nickol", 3);
        offer.addRaitingWithRaiter("Nolan", 2);
        offer.addRaitingWithRaiter("Ivan", 1);
        offer.deleteRateWithRaiter("Nickolas");
        offer.deleteRateWithRaiter("Ivan");
        assertEquals(3, offer.getSetOfRaiting().size());
        assertEquals(10.0 / 3.0, offer.getRaiting(), DOUBLE_EPS);

    }
}