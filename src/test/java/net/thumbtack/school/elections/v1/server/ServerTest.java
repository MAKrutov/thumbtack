package net.thumbtack.school.elections.v1.server;

import com.google.gson.Gson;
import net.thumbtack.school.elections.v1.Filler;
import net.thumbtack.school.elections.v1.database.DataBaseErrorCode;
import net.thumbtack.school.elections.v1.request.election.OfferDtoRequest;
import net.thumbtack.school.elections.v1.request.election.RegisterVoterDtoRequest;
import net.thumbtack.school.elections.v1.request.voter.LogOutDtoRequest;
import net.thumbtack.school.elections.v1.request.voter.LoginDtoRequest;
import net.thumbtack.school.elections.v1.response.ErrorResponse;
import net.thumbtack.school.elections.v1.response.election.OfferDtoResponse;
import net.thumbtack.school.elections.v1.response.election.RegisterVoterDtoResponse;
import net.thumbtack.school.elections.v1.response.voter.LoginDtoResponse;
import net.thumbtack.school.elections.v1.service.ServiceErrorCode;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

import java.io.File;
import java.io.IOException;
import java.util.Set;

import static junit.framework.TestCase.*;

public class ServerTest {
    Gson gson = new Gson();
    Filler filler = new Filler();
    String jsonRequest = null;
    String jsonResponse = null;

    @Rule
    public TemporaryFolder folder = new TemporaryFolder();

    @Test
    public void testServer() throws IOException, ClassNotFoundException {
        Server server = new Server();
        File file = folder.newFile("Data.dat");
        server.startServer("");
        server.dataBase = filler.fillDataBase(10);
        Gson gson = new Gson();
        Set a = server.dataBase.getActiveTokens();
        server.stopServer(file);
        server.startServer(file);
        assertEquals(server.dataBase.getActiveTokens(), a);
        server.stopServer((String) null);
    }

    @Test
    public void testLoginLogout() throws IOException, ClassNotFoundException {
        Server server = new Server();
        server.startServer("");
        RegisterVoterDtoRequest requestRV;
        RegisterVoterDtoResponse responseRV;
        requestRV = new RegisterVoterDtoRequest("Margo", "Viktorian", "Simpson", "1111111111", "1111111111", "Basila street", "19", "14", false);
        jsonRequest = gson.toJson(requestRV);
        jsonResponse = server.voterService.registerVoter(jsonRequest);
        responseRV = gson.fromJson(jsonResponse, RegisterVoterDtoResponse.class);
        LogOutDtoRequest logOutDtoRequest = new LogOutDtoRequest(responseRV.getToken());
        jsonRequest = gson.toJson(logOutDtoRequest);
        jsonResponse = server.voterService.logOut(jsonRequest);
        assertFalse(server.dataBase.getActiveTokens().contains(logOutDtoRequest.getToken()));
        assertEquals(0, server.dataBase.getActiveTokens().size());

        LoginDtoRequest requestLI = new LoginDtoRequest("1111111111", "11111111112");
        jsonRequest = gson.toJson(requestLI);
        jsonResponse = server.voterService.logIn(jsonRequest);
        ErrorResponse responseEr = gson.fromJson(jsonResponse, ErrorResponse.class);
        assertEquals(0, server.dataBase.getActiveTokens().size());
        assertEquals(ServiceErrorCode.WRONG_ACTIVATION.getErrorString(), responseEr.getError());

        requestLI = new LoginDtoRequest("1111111111", "1111111111");
        jsonRequest = gson.toJson(requestLI);
        jsonResponse = server.voterService.logIn(jsonRequest);
        LoginDtoResponse responseLI = gson.fromJson(jsonResponse, LoginDtoResponse.class);
        assertEquals(1, server.dataBase.getActiveTokens().size());
    }

    @Test
    public void testRegisterVoter() throws IOException, ClassNotFoundException {
        Server server = new Server();
        server.startServer("");
        RegisterVoterDtoRequest request;
        RegisterVoterDtoResponse response;
        request = new RegisterVoterDtoRequest("Margo", "Viktorian", "Simpson", "1111111111", "1111111111", "Basila street", "19", "14", false);
        jsonRequest = gson.toJson(request);
        jsonResponse = server.voterService.registerVoter(jsonRequest);
        response = gson.fromJson(jsonResponse, RegisterVoterDtoResponse.class);
        assertTrue(server.dataBase.getActiveTokens().contains(response.getToken()));
        request = new RegisterVoterDtoRequest("Margo", "Viktorian", "Simpson", "qwe111151rtyzzz", "Q1w32411axzzz", "Basila street", "19", "14", true);
        jsonRequest = gson.toJson(request);
        jsonResponse = server.voterService.registerVoter(jsonRequest);
        ErrorResponse errorResponse = gson.fromJson(jsonResponse, ErrorResponse.class);
        assertEquals(DataBaseErrorCode.USER_EXSISTS.getErrorString(), errorResponse.getError());
        server.stopServer("");
    }

    @Test
    public void testMakeOffer() throws IOException, ClassNotFoundException {
        Server server = new Server();
        server.startServer("");
        RegisterVoterDtoRequest requestRv;
        RegisterVoterDtoResponse responseRv;
        requestRv = new RegisterVoterDtoRequest("Margo", "Viktorian", "Simpson", "1111111111", "1111111111", "Basila street", "19", "14", false);
        jsonRequest = gson.toJson(requestRv);
        jsonResponse = server.voterService.registerVoter(jsonRequest);
        responseRv = gson.fromJson(jsonResponse, RegisterVoterDtoResponse.class);
        OfferDtoRequest requestOffer = new OfferDtoRequest(responseRv.getToken(), "Make woods green again");
        jsonRequest = gson.toJson(requestOffer);
        jsonResponse = server.voterService.makeOffer(jsonRequest);
        OfferDtoResponse responseOffer = gson.fromJson(jsonResponse, OfferDtoResponse.class);
        assertEquals(responseOffer.getOffer().getOfferString(), server.dataBase.getOfferMap().getKey("Make woods green again").getOfferString());
        requestOffer = new OfferDtoRequest(responseRv.getToken(), "MORE JOB");
        jsonRequest = gson.toJson(requestOffer);
        jsonResponse = server.voterService.makeOffer(jsonRequest);
        ErrorResponse errorResponse = gson.fromJson(jsonResponse, ErrorResponse.class);
        assertEquals(ServiceErrorCode.WRONG_OFFER.getErrorString(), errorResponse.getError());
    }

}