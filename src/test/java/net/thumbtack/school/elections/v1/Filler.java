package net.thumbtack.school.elections.v1;

import com.google.gson.Gson;
import net.thumbtack.school.elections.v1.database.DataBase;
import net.thumbtack.school.elections.v1.models.Person;
import net.thumbtack.school.elections.v1.request.candidate.AddOfferToProgramDtoRequest;
import net.thumbtack.school.elections.v1.request.election.OfferDtoRequest;
import net.thumbtack.school.elections.v1.request.election.RegisterVoterDtoRequest;
import net.thumbtack.school.elections.v1.response.election.RegisterVoterDtoResponse;
import net.thumbtack.school.elections.v1.service.CandidateService;
import net.thumbtack.school.elections.v1.service.ElectionService;
import net.thumbtack.school.elections.v1.service.VoterService;

import java.util.HashSet;
import java.util.Set;

public class Filler {

    Gson gson = new Gson();
    /******************************************************************************************/
    private String[] firstName = {"Bill", "BobbY", "Liza", "Elena", "Valera", "Ivan", "Anna", "Josh", "Robin", "Colin"};
    private String[] middleName = {"Bobb", "Ron", "Zick", "Zack", "Tron", "Robinson", "Bah", ""};
    private String[] lastName = {"Putin", "Obama", "Melnikov", "Kozlov", "Frederik", "Zera", "Zara", "Poll", "Qenny"};
    private String[] adress = {"GreenStreet", "GrandmaStreet", "Amur", "Mendeleeva", "MiddleStreet"};
    private String[] home = {"1", "2", "3", "4", "5", "6", "7"};
    private String[] apartment = {"1", "2", "3", "4", "5", "6", "7", "8", "9"};
    /******************************************************************************************/
    private String[] offers = {
            "Peace to peace",
            "Discount all prices",
            "Dust to dust",
            "Build more factories",
            "Build more shcools",
            "Free education for all",
            "Give home to homeless",
            "We need more trees"
    };

    public DataBase fillDataBase(int a) {
        // заполняет базу данных, каждый 10-й человек - кандидат, токены перенурмеруются с 1 до n, кандидаты имеют самый высокий номер;
        DataBase dataBase = new DataBase();
        VoterService voterService = new VoterService(dataBase);
        ElectionService electionService = new ElectionService(dataBase);
        int j = 1000000000;
        while (a > 0) {
            RegisterVoterDtoResponse one;
            RegisterVoterDtoRequest request = new RegisterVoterDtoRequest
                    (firstName[a % 10], lastName[a % 9], middleName[a % 8], Integer.toString(j), Integer.toString(j),
                            adress[a % 5], home[a % 7], apartment[a % 9], a % 10 == 0);
            String Json = gson.toJson(request);
            one = gson.fromJson(voterService.registerVoter(Json), RegisterVoterDtoResponse.class);
            a--;
            j--;
        }
        Set<Person> personSet = new HashSet<>(dataBase.getVoters().keySet());
        dataBase.getActiveTokens().clear();
        int i = 1;
        for (Person person : personSet) {
            dataBase.getVoters().put(person, Integer.toString(i));
            i++;
        }
        personSet = new HashSet<>(dataBase.getCandidates().keySet());
        for (Person person : personSet) {
            dataBase.getCandidates().put(person, Integer.toString(i));
            i++;
        }
        i--;
        while (i > 0) {
            dataBase.getActiveTokens().add(Integer.toString(i));
            i--;
        }
        OfferDtoRequest offerDtoRequest = new OfferDtoRequest("1", "Cut the coord");
        String offerJson = gson.toJson(offerDtoRequest);
        electionService.makeOffer(offerJson);

        for (String string : offers) {
            electionService.makeOffer(gson.toJson(new OfferDtoRequest("1", string)));
        }

        dataBase = fillOffers(dataBase);

        return dataBase;
    }

    public DataBase fillOffers(DataBase dataBase) {
        CandidateService candidateService = new CandidateService(dataBase);
        AddOfferToProgramDtoRequest offerToProgramDtoRequest;
        int i = offers.length;
        while (i > 0) {
            for (String strin : dataBase.getCandidates().values()) {
                offerToProgramDtoRequest = new AddOfferToProgramDtoRequest(offers[offers.length - i], strin);
                candidateService.addOfferToMyProgram(gson.toJson(offerToProgramDtoRequest));
            }
            i--;
        }
        return dataBase;
    }

}

