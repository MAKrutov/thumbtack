package net.thumbtack.school.misc.v3;

import org.junit.Test;

import static org.junit.Assert.*;

public class SegmentTest {
    private static final double DOUBLE_EPS = 1E-6;

    @Test
    public void setStart() {
        Segment a = new Segment(0, 0, 10, 10, 1);
        a.setStart(-5, 0);
        assertEquals(-5, a.getStart().getX());
        assertEquals(0, a.getStart().getY());
        a.setStart(10, 20);
        assertEquals(10, a.getStart().getX());
        assertEquals(20, a.getStart().getY());

    }

    @Test
    public void setEnd() {
        Segment segment = new Segment(10, 20, 30, 40, 1);
        assertEquals(30, segment.getEnd().getX());
        assertEquals(40, segment.getEnd().getY());
        segment.setEnd(-10, -20);
        assertEquals(-10, segment.getEnd().getX());
        assertEquals(-20, segment.getEnd().getY());

    }


    @Test
    public void getSetWidth() {
        Segment segment = new Segment(10, 20, 30, 40, 1);
        assertEquals(1, segment.getWidth());
        segment.setWidth(20);
        assertEquals(20, segment.getWidth());
        segment.setWidth(30);
        assertEquals(30, segment.getWidth());
    }

    @Test
    public void getLenght() {
        Segment segment = new Segment(10, 10, 10, -10, 1);
        assertEquals(20, segment.getLenght());
        segment = new Segment(10, 0, 10, 10, 1);
        assertEquals(10, segment.getLenght());
        segment = new Segment(10, 20, 30, 40, 1);
        assertEquals(28, segment.getLenght());
    }

    @Test
    public void moveRel() {
        Segment segment = new Segment(10, 20, 30, 40, 1);
        segment.moveRel(10, 10);
        assertEquals(20, segment.getStart().getX());
        assertEquals(30, segment.getStart().getY());
        assertEquals(40, segment.getEnd().getX());
        assertEquals(50, segment.getEnd().getY());
    }

    @Test
    public void moveTo() {
        Segment segment = new Segment(10, 20, 30, 40, 1);
        segment.moveTo(0, 0);
        assertEquals(0, segment.getStart().getX());
        assertEquals(0, segment.getStart().getY());
        assertEquals(20, segment.getEnd().getX());
        assertEquals(20, segment.getEnd().getY());
    }


    @Test
    public void isInside() { //falls when devide by zero
        Segment segment = new Segment(-1, -1, 1, 1, 1);
        assertTrue(segment.isInside(0, 0));
        segment.moveRel(1, 1);
        assertTrue(segment.isInside(1, 1));
        segment.moveRel(0, 1);
        assertFalse(segment.isInside(33, 1));

    }

    @Test
    public void getArea() {
        Segment segment = new Segment(0, -5, 0, 5, 1);
        assertEquals(10.0 * 1.0, segment.getArea(), DOUBLE_EPS);
        segment = new Segment(55, 0, 0, 0, 2);
        assertEquals(55.0 * 2.0, segment.getArea(), DOUBLE_EPS);
        segment = new Segment(0, -5, 0, 5, 3);
        assertEquals(10.0 * 3.0, segment.getArea(), DOUBLE_EPS);
    }

    @Test
    public void getPerimeter() {
        Segment segment = new Segment(0, -5, 0, 5, 1);
        assertEquals(22, segment.getPerimeter(), DOUBLE_EPS);
        segment = new Segment(55, 0, 0, 0, 2);
        assertEquals(114, segment.getPerimeter(), DOUBLE_EPS);
        segment = new Segment(0, -5, 0, 5, 3);
        assertEquals(20 + 3 + 3, segment.getPerimeter(), DOUBLE_EPS);

    }

    @Test
    public void resize() {
        Segment segment = new Segment(0, 20, 0, 40, 1);
        assertEquals(20, segment.getLenght());
        segment.resize(10);
        assertEquals(200, segment.getLenght());
        segment = new Segment(1, 15, 1, 1, 1);
        assertEquals(14, segment.getLenght());
        segment.resize(10);
        assertEquals(140, segment.getLenght());

    }

    @Test
    public void equals1() {
        Segment segmentFirst = new Segment(10, 20, 30, 40, 2);
        Segment segmentSecond = new Segment(0, 20, 30, 40, 1);
        Segment segmentThird = new Segment(10, 20, 30, 40, 1);
        Segment segmentFourth = new Segment(10, 20, 30, 40, 2);
        assertFalse(segmentFirst.equals(segmentSecond));
        assertFalse(segmentThird.equals(segmentFourth));
        assertTrue(segmentFirst.equals(segmentFourth));
    }

}