package net.thumbtack.school.misc.v3;

import net.thumbtack.school.figures.v3.Point;
import org.junit.Test;

import static org.junit.Assert.*;

public class CurveTest {
    private static final double DOUBLE_EPS = 1E-6;

    @Test
    public void setGetCenter() {
        Curve curve = new Curve();
        curve.setCenter(4, 4);
        assertEquals(4, curve.getCenter().getX());
        assertEquals(4, curve.getCenter().getY());
        curve.setCenter(1, 1);
        assertEquals(1, curve.getCenter().getX());
        assertEquals(1, curve.getCenter().getY());

        curve.setCenter(2, 1);
        assertEquals(2, curve.getCenter().getX());
        assertEquals(1, curve.getCenter().getY());
    }


    @Test
    public void setGetRadius() {
        Curve curve = new Curve(0, 0, 5);
        curve.setRadius(10);
        assertEquals(10, curve.getRadius());
        curve.setRadius(5);
        assertEquals(5, curve.getRadius());
    }

    @Test
    public void moveRel() {
        Curve curve = new Curve(0, 0, 5);
        curve.moveRel(10, 10);
        assertEquals(10, curve.getCenter().getX());
        assertEquals(10, curve.getCenter().getY());
        curve.moveRel(-5, -6);
        assertEquals(5, curve.getCenter().getX());
        assertEquals(4, curve.getCenter().getY());

    }

    @Test
    public void moveTo() {
        Curve curve = new Curve(0, 0, 5);
        curve.moveTo(new Point(10, 20));
        assertEquals(10, curve.getCenter().getX());
        assertEquals(20, curve.getCenter().getY());
        curve.moveTo(-10, 30);
        assertEquals(-10, curve.getCenter().getX());
        assertEquals(30, curve.getCenter().getY());

    }


    @Test
    public void isInside() {
        Curve curve = new Curve(0, 0, 5);
        assertTrue(curve.isInside(2, 2));
        assertFalse(curve.isInside(-1, -1));
        assertTrue(curve.isInside(new Point(5, 0)));
        assertFalse(curve.isInside(new Point(6, 0)));
    }

    @Test
    public void getArea() {
        Curve curve = new Curve(10);
        assertEquals(Math.PI * 50, curve.getArea(), DOUBLE_EPS);
        curve.setRadius(1);
        assertEquals(Math.PI * 0.5, curve.getArea(), DOUBLE_EPS);
        curve.setRadius(15);
        assertEquals(Math.PI * 225 / 2.0, curve.getArea(), DOUBLE_EPS);
    }

    @Test
    public void getPerimeter() {
        Curve curve = new Curve(10);
        assertEquals(Math.PI * 10 + 20, curve.getPerimeter(), DOUBLE_EPS);
        curve.setRadius(1);
        assertEquals(Math.PI * 1 + 2, curve.getPerimeter(), DOUBLE_EPS);
        curve.setRadius(15);
        assertEquals(Math.PI * 15 + 30, curve.getPerimeter(), DOUBLE_EPS);
    }

    @Test
    public void resize() {
        Curve curve = new Curve(10);
        curve.resize(0.5);
        assertEquals(5, curve.getRadius());
        curve.resize(3);
        assertEquals(15, curve.getRadius());
        curve.resize(2);
        assertEquals(30, curve.getRadius());
    }

    @Test
    public void equals1() {
        Curve curveFirst = new Curve(0, 0, 5);
        Curve curveSecond = new Curve(0, 0, 5);
        assertTrue(curveFirst.equals(curveSecond));
        curveFirst.moveTo(10, 10);
        assertFalse(curveFirst.equals(curveSecond));
    }
}