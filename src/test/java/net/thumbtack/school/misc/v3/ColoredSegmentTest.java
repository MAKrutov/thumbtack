package net.thumbtack.school.misc.v3;

import net.thumbtack.school.colors.v3.Color;
import net.thumbtack.school.colors.v3.ColorException;
import net.thumbtack.school.figures.v3.Point;
import org.junit.Test;

import static net.thumbtack.school.colors.v3.Color.*;
import static org.junit.Assert.*;

public class ColoredSegmentTest {

    @Test
    public void equals1() throws ColorException {
        ColoredSegment coloredSegment1 = new ColoredSegment(0, 0, 1, 1, 1, RED);
        ColoredSegment coloredSegment2 = new ColoredSegment(0, 0, 1, 1, 1, BLUE);
        assertFalse(coloredSegment1.equals(coloredSegment2));
        coloredSegment2 = new ColoredSegment(new Point(0, 0), new Point(1, 1), 1, RED);
        assertTrue(coloredSegment1.equals(coloredSegment2));
    }

    @Test
    public void getColor() throws ColorException {
        ColoredSegment coloredSegment = new ColoredSegment(10, RED);
        assertEquals(RED, coloredSegment.getColor());
        coloredSegment = new ColoredSegment(10, "Green");
        assertEquals(GREEN, coloredSegment.getColor());
    }

    @Test(expected = ColorException.class)
    public void setColor() throws ColorException {
        ColoredSegment coloredSegment = new ColoredSegment(10, 20, 30, 40, 1, RED);
        assertEquals(RED, coloredSegment.getColor());
        coloredSegment.setColor(BLUE);
        assertEquals(BLUE, coloredSegment.getColor());
        coloredSegment.setColor(GREEN);
        assertEquals(GREEN, coloredSegment.getColor());
        coloredSegment.setColor("BLACK");
    }

    @Test(expected = ColorException.class)
    public void setColor1() throws ColorException {
        ColoredSegment coloredSegment = new ColoredSegment(new Point(10, 20), new Point(30, 40), 1, "RED");
        assertEquals(RED, coloredSegment.getColor());
        coloredSegment.setColor("BLUE");
        assertEquals(BLUE, coloredSegment.getColor());
        coloredSegment.setColor("GREEN");
        assertEquals(GREEN, coloredSegment.getColor());
        coloredSegment.setColor((Color) null);
    }
}