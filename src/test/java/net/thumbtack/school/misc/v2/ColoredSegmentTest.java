package net.thumbtack.school.misc.v2;

import org.junit.Test;

import static org.junit.Assert.*;

public class ColoredSegmentTest {

    @Test
    public void setGetColor() {
        ColoredSegment segment = new ColoredSegment();
        segment.setColor(1);
        assertEquals(1,segment.getColor());
        segment.setColor(2);
        assertEquals(2,segment.getColor());
        segment.setColor(3);
        assertEquals(3,segment.getColor());
    }


    @Test
    public void equals1() {
        ColoredSegment first = new ColoredSegment();
        ColoredSegment second = new ColoredSegment();
        assertTrue(first.equals(second));
        first.setColor(5);
        assertFalse(first.equals(second));
    }
}