package net.thumbtack.school.boxes.v3;

import net.thumbtack.school.figures.v3.Figure;
import net.thumbtack.school.iface.v3.HasArea;

public class PairBox<T extends Figure, I extends Figure> implements HasArea {
    private I obj2;
    private T obj1;

    public PairBox(T obj1, I obj2) {
        this.obj1 = obj1;
        this.obj2 = obj2;
    }

    static boolean isAreaEqual(PairBox<? extends Figure,? extends Figure> pairBox1, PairBox<? extends Figure,? extends Figure> pairBox2) {
        return (pairBox1.getArea() == pairBox2.getArea());
    }

    public T getContentFirst() {
        return obj1;
    }

    public void setContentFirst(T obj1) {
        this.obj1 = obj1;
    }

    public I getContentSecond() {
        return obj2;
    }

    public void setContentSecond(I obj2) {
        this.obj2 = obj2;
    }

    @Override
    public double getArea() {
        return obj1.getArea() + obj2.getArea();
    }

    boolean isAreaEqual(PairBox<? extends Figure,? extends Figure> pairBox) {
        return (this.getArea() == pairBox.getArea());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        PairBox<?, ?> pairBox = (PairBox<?, ?>) o;

        if (obj1 != null ? !obj1.equals(pairBox.obj1) : pairBox.obj1 != null) return false;
        return obj2 != null ? obj2.equals(pairBox.obj2) : pairBox.obj2 == null;

    }

    @Override
    public int hashCode() {
        int result = obj1 != null ? obj1.hashCode() : 0;
        result = 31 * result + (obj2 != null ? obj2.hashCode() : 0);
        return result;
    }
}
