package net.thumbtack.school.boxes.v3;

import net.thumbtack.school.figures.v3.Figure;

public class NamedBox<T extends Figure> extends Box {

    private String name;

    public NamedBox(T obj) {
        super(obj);
    }

    public NamedBox(T obj, String name) {
        this(obj);
        setName(name);
    }

    @Override
    public T getContent() {
        return (T) obj;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
