package net.thumbtack.school.boxes.v3;

import net.thumbtack.school.figures.v3.Figure;
import net.thumbtack.school.iface.v3.HasArea;

public class Box<T extends Figure> implements HasArea {

    protected T obj;

    public Box(T obj) {
        this.obj = obj;
    }

    static boolean isAreaEqual(Box<? extends Figure> aBox, Box<? extends Figure> bBox) {
        return (aBox.getArea() == bBox.getArea());
    }

    public T getContent() {
        return obj;
    }

    public void setContent(T obj) {
        this.obj = obj;
    }


    boolean isAreaEqual(Box<? extends Figure> tBox) {
        return (getArea() == tBox.getArea());
    }


    @Override
    public int hashCode() {
        return obj != null ? obj.hashCode() : 0;
    }

    @Override
    public double getArea() {
        return obj.getArea();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Box<?> box = (Box<?>) o;

        return obj != null ? obj.equals(box.obj) : box.obj == null;

    }
}
