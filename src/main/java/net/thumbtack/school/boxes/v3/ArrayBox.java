package net.thumbtack.school.boxes.v3;

import net.thumbtack.school.figures.v3.Figure;

public class ArrayBox<T extends Figure> {
    private T[] obj;

    public ArrayBox(T[] obj) {
        super();
        this.obj = obj;
    }

    public T[] getContent() {
        return obj;
    }

    public void setContent(T[] obj) {
        this.obj = obj;
    }

    public T getElement(int i) {
        return obj[i];
    }

    public void setElement(T obj, int i) {
        this.obj[i] = obj;
    }

    boolean isSameSize(ArrayBox<? extends Figure> arrayBox) {
        return (obj.length == arrayBox.obj.length);
    }

}
