package net.thumbtack.school.database.mybatis.daoimpl;

import net.thumbtack.school.database.model.Group;
import net.thumbtack.school.database.model.School;
import net.thumbtack.school.database.model.Subject;
import net.thumbtack.school.database.model.Trainee;
import net.thumbtack.school.database.mybatis.dao.SchoolDao;
import net.thumbtack.school.database.mybatis.utils.MyBatisUtils;
import org.apache.ibatis.session.SqlSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

public class SchoolDaoImpl extends DaoImplBase implements SchoolDao {
    private static final Logger LOGGER = LoggerFactory.getLogger(SchoolDaoImpl.class);

    @Override
    public School insert(School school) {
        LOGGER.debug("DAO insert  school {}", school);
        try (SqlSession sqlSession = MyBatisUtils.getSession()) {
            try {
                getSchoolMapper(sqlSession).insert(school);
            } catch (RuntimeException ex) {
                LOGGER.info("Can't insert school {}, {}", school, ex);
                sqlSession.rollback();
                throw ex;
            }
            sqlSession.commit();
        }
        return school;
    }

    @Override
    public School getById(int id) {
        School school;
        LOGGER.debug("DAO get school by id{}", id);
        try (SqlSession sqlSession = MyBatisUtils.getSession()) {
            try {
                school = getSchoolMapper(sqlSession).getById(id);
            } catch (RuntimeException ex) {
                LOGGER.info("Can't get school{}, {}", ex);
                throw ex;
            }
        }
        return school;
    }

    @Override
    public List<School> getAllLazy() {
        LOGGER.debug("DAO get all LAZY join");
        List<School> schools = null;
        try (SqlSession sqlSession = getSession()) {
            schools = getSchoolMapper(sqlSession).getAllLazy();
        } catch (RuntimeException ex) {
            LOGGER.debug("Can't get all LAZY{}", ex);
            throw ex;
        }
        return schools;
    }

    @Override
    public List<School> getAllUsingJoin() {
        LOGGER.debug("DAO get all using join");
        List<School> schools = null;
        try (SqlSession sqlSession = getSession()) {
            schools = sqlSession.selectList("net.thumbtack.school.database.mybatis.mappers.SchoolMapper.getAllUsingJoin");
        } catch (RuntimeException ex) {
            LOGGER.debug("Can't get all Using Join{}", ex);
            throw ex;
        }
        return schools;
    }

    @Override
    public void update(School school) {
        LOGGER.debug("DAO update school {}", school);
        try (SqlSession sqlSession = MyBatisUtils.getSession()) {
            try {
                getSchoolMapper(sqlSession).update(school);
            } catch (RuntimeException ex) {
                LOGGER.info("Can't update school{}, {}", ex);
                sqlSession.rollback();
                throw ex;
            }
            sqlSession.commit();
        }
    }

    @Override
    public void delete(School school) {
        LOGGER.debug("DAO delete school {}", school);
        try (SqlSession sqlSession = MyBatisUtils.getSession()) {
            try {
                getSchoolMapper(sqlSession).delete(school);
            } catch (RuntimeException ex) {
                LOGGER.info("Can't delete school{}, {}", ex);
                throw ex;
            }
            sqlSession.commit();
        }
    }

    @Override
    public void deleteAll() {
        LOGGER.debug("DAO delete all schools {}");
        try (SqlSession sqlSession = MyBatisUtils.getSession()) {
            try {
                getSchoolMapper(sqlSession).deleteAll();
            } catch (RuntimeException ex) {
                LOGGER.info("Can't delete school{}", ex);
                throw ex;
            }
            sqlSession.commit();
        }
    }

    @Override
    public School insertSchoolTransactional(School school2018) {
        LOGGER.debug("transactional DAO insert school", school2018);
        try (SqlSession sqlSession = MyBatisUtils.getSession()) {
            try {
                getSchoolMapper(sqlSession).insert(school2018);
                for (Group group : school2018.getGroups()) {

                    getGroupMapper(sqlSession).insert(school2018, group);

                    for (Subject subject : group.getSubjects()) {
                        getGroupMapper(sqlSession).addSubjectToGroup(group, subject);
                    }

                    for (Trainee trainee : group.getTrainees()) {
                        getTraineeMapper(sqlSession).insert(group, trainee);
                    }

                }
            } catch (RuntimeException ex) {
                LOGGER.debug("Can't insert school transactional {} {}", school2018, ex);
                sqlSession.rollback();
                throw ex;
            }
            sqlSession.commit();
        }
        return school2018;
    }
}

