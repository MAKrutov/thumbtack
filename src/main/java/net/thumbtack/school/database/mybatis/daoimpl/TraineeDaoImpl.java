package net.thumbtack.school.database.mybatis.daoimpl;

import net.thumbtack.school.database.model.Group;
import net.thumbtack.school.database.model.Trainee;
import net.thumbtack.school.database.mybatis.dao.TraineeDao;
import net.thumbtack.school.database.mybatis.utils.MyBatisUtils;
import org.apache.ibatis.session.SqlSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

public class TraineeDaoImpl extends DaoImplBase implements TraineeDao {
    private static final Logger LOGGER = LoggerFactory.getLogger(TraineeDaoImpl.class);

    @Override
    public Trainee insert(Group group, Trainee trainee) {
        LOGGER.debug("DAO insert trainee with/no group {},{}", group, trainee);
        try (SqlSession sqlSession = MyBatisUtils.getSession()) {
            try {
                getTraineeMapper(sqlSession).insert(group, trainee);
            } catch (RuntimeException ex) {
                LOGGER.info("Can't insert trainee {}, {}", group, trainee, ex);
                sqlSession.rollback();
                throw ex;
            }
            sqlSession.commit();
        }
        return trainee;
    }

    @Override
    public Trainee getById(int id) {
        LOGGER.debug("DAO get Trainee by Id {}", id);
        try (SqlSession sqlSession = MyBatisUtils.getSession()) {
            try {
                return getTraineeMapper(sqlSession).getById(id);
            } catch (RuntimeException ex) {
                LOGGER.info("Can't get Trainee {}", ex);
                throw ex;
            }
        }
    }


    @Override
    public List<Trainee> getAll() {
        List<Trainee> trainees = null;
        LOGGER.debug("DAO get all Trainee {}");
        try (SqlSession sqlSession = MyBatisUtils.getSession()) {
            try {
                trainees = getTraineeMapper(sqlSession).getAll();
            } catch (RuntimeException ex) {
                LOGGER.info("Can't get Trainee {}", ex);
                throw ex;
            }
        }
        return trainees;
    }

    @Override
    public Trainee update(Trainee trainee) {
        LOGGER.debug("DAO update Trainee {}", trainee);

        try (SqlSession sqlSession = MyBatisUtils.getSession()) {
            try {
                getTraineeMapper(sqlSession).update(trainee);
            } catch (RuntimeException ex) {
                LOGGER.debug("DAO can't update Trainee {}", ex);
                sqlSession.rollback();
                throw ex;
            }
            sqlSession.commit();
        }
        return trainee;
    }

    @Override
    public List<Trainee> getAllWithParams(String firstName, String lastName, Integer rating) {
        List<Trainee> trainees = null;
        LOGGER.debug("DAO get trainees by params {}", firstName, lastName, rating);

        try (SqlSession sqlSession = MyBatisUtils.getSession()) {
            try {
                trainees = getTraineeMapper(sqlSession).getAllWithParams(firstName, lastName, rating);
            } catch (RuntimeException e) {
                LOGGER.debug("Can't find trainee with this params");
                throw e;
            }
        }
        return trainees;
    }

    @Override
    public void batchInsert(List<Trainee> trainees) {
        LOGGER.debug("DAO insert list of trainee {}");
        try (SqlSession sqlSession = MyBatisUtils.getSession()) {
            try {
                getTraineeMapper(sqlSession).batchInsert(trainees);
            } catch (RuntimeException ex) {
                LOGGER.info("Can't insert trainees {}, {}", trainees, ex);
                sqlSession.rollback();
                throw ex;
            }
            sqlSession.commit();
        }
    }

    @Override
    public void delete(Trainee trainee) {
        LOGGER.debug("DAO delete current Trainee");
        try (SqlSession sqlSession = MyBatisUtils.getSession()) {
            try {
                getTraineeMapper(sqlSession).delete(trainee);
            } catch (RuntimeException ex) {
                LOGGER.debug("DAO can't delete current trainee", ex);
                sqlSession.rollback();
                throw ex;
            }
            sqlSession.commit();
        }
    }

    @Override
    public void deleteAll() {
        LOGGER.debug("DAO delete all Trainees");
        try (SqlSession sqlSession = MyBatisUtils.getSession()) {
            try {
                getTraineeMapper(sqlSession).deleteAll();
            } catch (RuntimeException ex) {
                LOGGER.debug("DAO can't delete Trainees", ex);
                sqlSession.rollback();
                throw ex;
            }
            sqlSession.commit();
        }
    }

}
