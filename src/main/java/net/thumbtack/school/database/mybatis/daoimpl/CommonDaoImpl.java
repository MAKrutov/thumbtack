package net.thumbtack.school.database.mybatis.daoimpl;

import net.thumbtack.school.database.mybatis.dao.CommonDao;

public class CommonDaoImpl implements CommonDao {
    @Override
    public void clear() {
        GroupDaoImpl groupDao = new GroupDaoImpl();
        TraineeDaoImpl traineeDao = new TraineeDaoImpl();
        SubjectDaoImpl subjectDao = new SubjectDaoImpl();
        SchoolDaoImpl schoolDao = new SchoolDaoImpl();
        schoolDao.deleteAll();
        groupDao.getAll();
        traineeDao.deleteAll();
        subjectDao.deleteAll();
    }
}
