package net.thumbtack.school.database.mybatis.mappers;

import net.thumbtack.school.database.model.Group;
import net.thumbtack.school.database.model.Trainee;
import org.apache.ibatis.annotations.*;

import java.util.List;

public interface TraineeMapper {
    @Insert({"<script>",
            "insert into trainee(firstName,lastName,rating, groupID) values " +
                    "(#{trainee.firstName}, #{trainee.lastName}, #{trainee.rating},",
            "<if test='`group`!=null'> #{group.id} " +
                    " </if> )",

            "</script>"})
    @Options(useGeneratedKeys = true, keyProperty = "trainee.id")
    Integer insert(@Param("group") Group group, @Param("trainee") Trainee trainee);



    @Select("SELECT * FROM trainee where id = #{id}")
    Trainee getById(int id);

    @Select("SELECT * FROM trainee")
    List<Trainee> getAll();

    @Update("update trainee set firstName = #{firstName},lastName =#{lastName}, rating = #{rating} where id = #{id}")
    void update(Trainee trainee);


    @Select({"<script>",
            "SELECT * FROM trainee" +
                    " <where>" +

                    "<if test='rating!= null'> rating = #{rating}",
            "</if>",

            "<if test='firstName != null'> AND firstName like #{firstName}",
            "</if>",
            "<if test='lastName != null'> AND lastName like #{lastName}",
            "</if>",
            "</where>" +
                    "</script>"})
    List<Trainee> getAllWithParams(@Param("firstName") String firstName, @Param("lastName") String lastName, @Param("rating") Integer rating);


    @Insert({"<script>",
            "insert into trainee (firstName,lastName,rating) values",
            "<foreach item='item' collection='list' separator=','>",
            "(#{item.firstName},#{item.lastName},#{item.rating})",
            "</foreach>",
            "</script>"
    })
    @Options(useGeneratedKeys = true)
    void batchInsert(@Param("list") List<Trainee> trainees);

    @Delete("delete from trainee where id = #{id}")
    void delete(Trainee trainee);

    @Delete("delete from trainee")
    void deleteAll();


    @Select("Select * from trainee where groupID = #{group.id}")
    List<Trainee> getByGroup(@Param("group") int groupID);
}
