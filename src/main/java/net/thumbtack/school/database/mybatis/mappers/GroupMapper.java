package net.thumbtack.school.database.mybatis.mappers;

import net.thumbtack.school.database.model.Group;
import net.thumbtack.school.database.model.School;
import net.thumbtack.school.database.model.Subject;
import net.thumbtack.school.database.model.Trainee;
import org.apache.ibatis.annotations.*;
import org.apache.ibatis.mapping.FetchType;

import java.util.List;

public interface GroupMapper {

    // вставляет Group в базу данных, привязывая ее к School.
    @Insert("insert into `group` (name, room, schoolid) values (#{group.name},#{group.room},#{school.id})")
    @Options(useGeneratedKeys = true, keyProperty = "group.id")
    void insert(@Param("school") School school, @Param("group") Group group);

    // изменяет Group  в базе данных, принадлежность к School не меняется
    @Update(" update `group` set name = #{name}, room = #{room} where id = #{id}")
    void update(Group group);

    // получает список всех Group, независимо от School
    @Select("select * from `group`")
    @Results({
            @Result(property = "id", column = "id"),
            @Result(property = "name", column = "name"),
            @Result(property = "trainees", column = "id", javaType = List.class,
                    many = @Many(select = "net.thumbtack.school.database.mybatis.mappers.TraineeMapper.getByGroup", fetchType = FetchType.LAZY)),
            @Result(property = "subjects", column = "id", javaType = List.class,
                    many = @Many(select = "net.thumbtack.school.database.mybatis.mappers.SubjectMapper.getByGroup", fetchType = FetchType.LAZY))
    })
    List<Group> getAll();

    // удаляет Group  в базе данных, при этом все Trainee оказываеются не принадлежащими никакой Group
    @Delete("delete from `group` where id =#{id}")
    void delete(Group group);

    // переводит Trainee в Group. Если Trainee не принадлежал никакой Group, добавляет его в Group
    @Update("update trainee set groupID = #{group.id} where trainee.id =#{trainee.id} ")
    void moveTraineeToGroup(@Param("group") Group group, @Param("trainee") Trainee trainee);

    // удаляет Trainee из Group, после этого Trainee не принадлежит никакой Group
    @Update("update trainee set groupID = null  where id = #{id}")
    void deleteTraineeFromGroup(Trainee trainee);

    // добавляет Subject к Group
    @Insert("insert into subject_group (subjectID, groupID,name) values (#{subject.id}, #{group.id},#{subject.name})")
    void addSubjectToGroup(@Param("group") Group group, @Param("subject") Subject subject);

    @Select("select * from `group` where schoolid = #{id}")
    @Results({
            @Result(property = "id", column = "id"),
            @Result(property = "name", column = "name"),
            @Result(property = "year", column = "year"),
            @Result(property = "trainees", column = "id", javaType = List.class,
                    many = @Many(select = "net.thumbtack.school.database.mybatis.mappers.TraineeMapper.getByGroup", fetchType = FetchType.LAZY)),
            @Result(property = "subjects", column = "id", javaType = List.class,
                    many = @Many(select = "net.thumbtack.school.database.mybatis.mappers.SubjectMapper.getByGroup", fetchType = FetchType.LAZY)),

    })
    List<Group> getBySchool(int id);
}
