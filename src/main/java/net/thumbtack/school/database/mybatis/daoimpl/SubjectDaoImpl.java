package net.thumbtack.school.database.mybatis.daoimpl;

import net.thumbtack.school.database.model.Subject;
import net.thumbtack.school.database.mybatis.dao.SubjectDao;
import net.thumbtack.school.database.mybatis.utils.MyBatisUtils;
import org.apache.ibatis.session.SqlSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

public class SubjectDaoImpl extends DaoImplBase implements SubjectDao {
    private static final Logger LOGGER = LoggerFactory.getLogger(SubjectDaoImpl.class);

    @Override
    public Subject insert(Subject subject) {
        LOGGER.debug("DAO insert subject {}", subject);
        try (SqlSession sqlSession = MyBatisUtils.getSession()) {
            try {
                getSubjectMapper(sqlSession).insert(subject);
            } catch (RuntimeException ex) {
                LOGGER.info("Can't insert Subject {}", subject, ex);
                sqlSession.rollback();
                throw ex;
            }
            sqlSession.commit();
        }
        return subject;
    }

    @Override
    public Subject getById(int id) {
        LOGGER.debug("DAO get Subject by Id {}", id);
        try (SqlSession sqlSession = MyBatisUtils.getSession()) {
            try {
                return getSubjectMapper(sqlSession).getById(id);
            } catch (RuntimeException ex) {
                LOGGER.info("Can't get Subject {}", ex);
                throw ex;
            }
        }
    }

    @Override
    public List<Subject> getAll() {
        LOGGER.debug("DAO get ALL Subjects");
        try (SqlSession sqlSession = MyBatisUtils.getSession()) {
            try {
                return getSubjectMapper(sqlSession).getAll();
            } catch (RuntimeException ex) {
                LOGGER.info("Can't get Subjects {}", ex);
                throw ex;
            }
        }
    }

    @Override
    public Subject update(Subject subject) {
        LOGGER.debug("DAO update Subject");

        try (SqlSession sqlSession = MyBatisUtils.getSession()) {
            try {
                getSubjectMapper(sqlSession).update(subject);
            } catch (RuntimeException ex) {
                LOGGER.debug("DAO can't update Subject {}", ex);
                sqlSession.rollback();
                throw ex;
            }
            sqlSession.commit();
        }
        return subject;
    }

    @Override
    public void delete(Subject subject) {
        LOGGER.debug("DAO delete current Subject");
        try (SqlSession sqlSession = MyBatisUtils.getSession()) {
            try {
                getSubjectMapper(sqlSession).delete(subject);
            } catch (RuntimeException ex) {
                LOGGER.debug("DAO can't delete current Subject", ex);
                sqlSession.rollback();
                throw ex;
            }
            sqlSession.commit();
        }
    }

    @Override
    public void deleteAll() {
        LOGGER.debug("DAO delete all Subjects");
        try (SqlSession sqlSession = MyBatisUtils.getSession()) {
            try {
                getSubjectMapper(sqlSession).deleteAll();
            } catch (RuntimeException ex) {
                sqlSession.rollback();
                throw ex;
            }
            sqlSession.commit();
        }
    }

}

