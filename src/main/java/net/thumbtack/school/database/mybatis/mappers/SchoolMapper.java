package net.thumbtack.school.database.mybatis.mappers;

import net.thumbtack.school.database.model.School;
import org.apache.ibatis.annotations.*;
import org.apache.ibatis.mapping.FetchType;

import java.util.List;

public interface SchoolMapper {

    @Insert("insert into school(name,year)  values( #{name},#{year})")
    @Options(useGeneratedKeys = true)
    int insert(School school);

    @Select("select * from school where id = #{id}")
    @Results({
            @Result(property = "id", column = "id"),
            @Result(property = "name", column = "name"),
            @Result(property = "year", column = "year"),
            @Result(property = "groups", column = "id", javaType = List.class,
                    many = @Many(select = "net.thumbtack.school.database.mybatis.mappers.GroupMapper.getBySchool", fetchType = FetchType.LAZY)),
    })
    School getById(int id);

    @Select("select * from school")
    @Results({
            @Result(property = "id", column = "id"),
            @Result(property = "name", column = "name"),
            @Result(property = "year", column = "year"),
            @Result(property = "groups", column = "id", javaType = List.class,
                    many = @Many(select = "net.thumbtack.school.database.mybatis.mappers.GroupMapper.getBySchool", fetchType = FetchType.LAZY)),
    })
    List<School> getAllLazy();

    // изменяет School  в базе данных
    @Update("update school set name = #{name},year=#{year}")
    void update(School school);

    @Delete("delete from school where id = #{id}")
    void delete(School school);

    @Delete("delete from school")
    void deleteAll();

}
