package net.thumbtack.school.database.mybatis.mappers;

import net.thumbtack.school.database.model.Subject;
import org.apache.ibatis.annotations.*;

import java.util.List;

public interface SubjectMapper {

    @Insert("insert into `subject`(name) values( #{name})")
    @Options(useGeneratedKeys = true)
    Integer insert(Subject subject);

    @Select("Select * from `subject` where id = #{id}")
    @Results({
            @Result(property = "id", column = "id"),
            @Result(property = "name", column = "name")
    })
    Subject getById(int id);

    @Select("Select  *from `subject`")
    @Results({
            @Result(property = "id", column = "id"),
            @Result(property = "name", column = "name")
    })
    List<Subject> getAll();

    @Update("update `subject` set name = #{name} where id = #{id}")
    void update(Subject subject);

    @Delete("delete from `subject` where id =#{id}")
    void delete(Subject subject);

    @Delete("delete from `subject`")
    void deleteAll();


    @Select("Select * from `subject` WHERE id in (select subjectID from subject_group where groupID = #{id})")
    @Results({
            @Result(property = "id", column = "id"),
            @Result(property = "name", column = "name")
    })
    List<Subject> getByGroup(int id);
}
