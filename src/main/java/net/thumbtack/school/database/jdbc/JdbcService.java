package net.thumbtack.school.database.jdbc;

import com.mysql.jdbc.Statement;
import net.thumbtack.school.database.model.Group;
import net.thumbtack.school.database.model.School;
import net.thumbtack.school.database.model.Subject;
import net.thumbtack.school.database.model.Trainee;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class JdbcService {

    //    Вместо многоточия вставьте требуемый набор полей и ключей.
//    При определении набора полей исходите из набора полей соответствующего класса Java.
//    Например, для таблицы  trainee такими полями, очевидно, будут id, firstname, lastname и rating.
//    При необходимости Вы можете добавлять и иные поля и/или таблицы, например, для обеспечения соотношений 1 : many или many : many.
//
//    Для получения Connection в каждом методе воспользуйтесь методом JdbcUtils.getConnection.
//
//    Во всех методах должен использоваться PreparedStatement, а не Statement!
//    В каждом из методов разрешается выполнить только один SQL запрос (executeQuery или executeUpdate) !
//
//    При реализации этих методов и SQL создания БД нужно исходить из следующих положений
//
//    Trainee может как принадлежать, так и не принадлежать к Group.
//    Поэтому при вставке  Trainee можно либо указать Group, к которой он будет принадлежать, либо не указывать,
//    в этом случае Trainee не будет принадлежать ни к какой группе.
//    Для вставленного Trainee может быть в дальнейшем произведена привязка к какой-то Group (в этом задании упражнений с такой привязкой нет).
//
//    Subject всегда вставляется независимо от чего бы то ни было.
//    В дальнейшем Subject может быть привязан к одной или нескольким Group (в этом задании упражнений с такой привязкой нет)
//
//    School в этом Задании вставляется без своих Group, добавление в нее Group должно быть сделано позже.
//    Group не может быть вставлена без привязки к School.
    public static void insertTrainee(Trainee trainee) throws SQLException {
        String insertQuery = "INSERT INTO trainee (id,firstname,lastname,rating)values(?,?,?,?)";
        Connection con = JdbcUtils.getConnection();
        try (PreparedStatement stmt = con.prepareStatement(insertQuery, Statement.RETURN_GENERATED_KEYS)) {
            stmt.setNull(1, Types.INTEGER);
            stmt.setString(2, trainee.getFirstName());
            stmt.setString(3, trainee.getLastName());
            stmt.setInt(4, trainee.getRating());
            stmt.executeUpdate();
            try (ResultSet generatedKeys = stmt.getGeneratedKeys()) {
                while (generatedKeys.next()) {
                    trainee.setId(generatedKeys.getInt(1));
                }
            }
        }
    }


    public static void updateTrainee(Trainee trainee) throws SQLException {
        String updateQuery = "update trainee set rating = ?, lastname = ? ,firstname = ? where id = ? ";
        Connection con = JdbcUtils.getConnection();
        try (PreparedStatement stmt = con.prepareStatement(updateQuery)) {
            stmt.setInt(1, trainee.getRating());
            stmt.setString(2, trainee.getLastName());
            stmt.setString(3, trainee.getFirstName());
            stmt.setInt(4, trainee.getId());
            stmt.executeUpdate();
        }
    }


    public static Trainee getTraineeByIdUsingColNames(int traineeId) throws SQLException {
        Trainee trainee = null;
        String selectQuery = "select * from trainee where id = ?";
        Connection con = JdbcUtils.getConnection();
        try (PreparedStatement stmt = con.prepareStatement(selectQuery)) {
            stmt.setInt(1, traineeId);
            try (ResultSet rs = stmt.executeQuery()) {
                while (rs.next()) {
                    int id = traineeId;
                    String firstName = rs.getString("firstName");
                    String lastName = rs.getString("lastName");
                    int rating = rs.getInt("rating");
                    trainee = new Trainee(id, firstName, lastName, rating);
                }
            }
        }
        return trainee;
    }

    //    Получает Trainee  из базы данных по его ID, используя метод получения “по именам полей”. Если Trainee с таким ID нет, возвращает null.
//
    public static Trainee getTraineeByIdUsingColNumbers(int traineeId) throws SQLException {
        Trainee trainee = null;
        String getQuery = "select * from trainee where id = ?";
        Connection con = JdbcUtils.getConnection();
        try (PreparedStatement stmt = con.prepareStatement(getQuery)) {

            stmt.setInt(1, traineeId);
            try (ResultSet rs = stmt.executeQuery()) {
                while (rs.next()) {
                    int id = rs.getInt(1);
                    String firstName = rs.getString(2);
                    String lastName = rs.getString(3);
                    int rating = rs.getInt(4);
                    trainee = new Trainee(id, firstName, lastName, rating);
                }
            }
            return trainee;
        }
    }


    public static List<Trainee> getTraineesUsingColNames() throws SQLException {
        List<Trainee> trainees = new ArrayList<>();
        String selectQuery = "select * from trainee";
        Connection con = JdbcUtils.getConnection();
        try (PreparedStatement stmt = con.prepareStatement(selectQuery);
             ResultSet rs = stmt.executeQuery()) {
            while (rs.next()) {
                int id = rs.getInt("id");
                String firstName = rs.getString("firstName");
                String lastName = rs.getString("lastName");
                int rating = rs.getInt("rating");
                trainees.add(new Trainee(id, firstName, lastName, rating));
            }
        }
        return trainees;
    }


    public static List<Trainee> getTraineesUsingColNumbers() throws SQLException {
        List<Trainee> trainees = new ArrayList<>();
        String selectQuery = "select * from trainee";
        Connection con = JdbcUtils.getConnection();
        try (PreparedStatement stmt = con.prepareStatement(selectQuery);
             ResultSet rs = stmt.executeQuery()) {
            while (rs.next()) {
                int id = rs.getInt(1);
                String firstName = rs.getString(2);
                String lastName = rs.getString(3);
                int rating = rs.getInt(4);
                trainees.add(new Trainee(id, firstName, lastName, rating));
            }
        }
        return trainees;
    }


    public static void deleteTrainee(Trainee trainee) throws SQLException {
        String deleteQuery = "delete from trainee where id = ?";
        Connection con = JdbcUtils.getConnection();
        try (PreparedStatement stmt = con.prepareStatement(deleteQuery)) {
            stmt.setInt(1, trainee.getId());
            stmt.executeUpdate();
        }
    }


    public static void deleteTrainees() throws SQLException {
        String deleteQuery = "delete from  trainee";
        Connection con = JdbcUtils.getConnection();
        try (PreparedStatement stmt = con.prepareStatement(deleteQuery)) {
            stmt.executeUpdate();
        }
    }


    public static void insertSubject(Subject subject) throws SQLException {
        String insertQuery = "insert into subject (id,name)values(?,?)";
        Connection con = JdbcUtils.getConnection();
        try (PreparedStatement stmt = con.prepareStatement(insertQuery, Statement.RETURN_GENERATED_KEYS)) {
            stmt.setNull(1, Types.INTEGER);
            stmt.setString(2, subject.getName());
            stmt.executeUpdate();
            try (ResultSet rs = stmt.getGeneratedKeys()) {
                while (rs.next()) {
                    subject.setId(rs.getInt(1));
                }
            }
        }
    }

    public static Subject getSubjectByIdUsingColNames(int subjectId) throws SQLException {
        Subject subject = null;
        String selectQuery = "select * from subject where id = ?";
        Connection con = JdbcUtils.getConnection();
        try (PreparedStatement stmt = con.prepareStatement(selectQuery)) {
            stmt.setInt(1, subjectId);
            try (ResultSet rs = stmt.executeQuery()) {
                while (rs.next()) {
                    int id = rs.getInt("id");
                    String name = rs.getString("name");
                    subject = new Subject(id, name);
                }
            }
        }
        return subject;
    }


    public static Subject getSubjectByIdUsingColNumbers(int subjectId) throws SQLException {
        Subject subject = null;
        String selectQuery = "select * from subject where id = ?";
        Connection con = JdbcUtils.getConnection();
        try (PreparedStatement stmt = con.prepareStatement(selectQuery)) {
            stmt.setInt(1, subjectId);
            try (ResultSet rs = stmt.executeQuery()) {
                while (rs.next()) {
                    int id = rs.getInt(1);
                    String name = rs.getString(2);
                    subject = new Subject(id, name);
                }
            }
        }
        return subject;
    }


    public static void deleteSubjects() throws SQLException {
        String deleteQuery = "delete from subject";
        Connection con = JdbcUtils.getConnection();
        try (PreparedStatement stmt = con.prepareStatement(deleteQuery)) {
            stmt.executeUpdate();
        }
    }


    public static void insertSchool(School school) throws SQLException {
        String insertQuery;
        insertQuery = "insert into school values(?,?,?)";
        Connection con = JdbcUtils.getConnection();
        try (PreparedStatement stmt = con.prepareStatement(insertQuery, Statement.RETURN_GENERATED_KEYS)) {
            stmt.setNull(1, Types.INTEGER);
            stmt.setString(2, String.valueOf(school.getYear()));
            stmt.setString(3, school.getName());
            stmt.executeUpdate();
            try (ResultSet rs = stmt.getGeneratedKeys()) {
                while (rs.next()) {
                    school.setId(rs.getInt(1));
                }
            }
        }
    }


    public static School getSchoolByIdUsingColNames(int schoolId) throws SQLException {
        School school = null;
        String selectQuery = "select * from school where id = ?";
        Connection con = JdbcUtils.getConnection();
        try (PreparedStatement stmt = con.prepareStatement(selectQuery)) {
            stmt.setInt(1, schoolId);
            try (ResultSet rs = stmt.executeQuery()) {
                while (rs.next()) {
                    int id = rs.getInt("id");
                    String name = rs.getString("name");
                    int year = rs.getInt("year");
                    school = new School(id, name, year);
                }
            }
        }
        return school;
    }


    public static School getSchoolByIdUsingColNumbers(int schoolId) throws SQLException {
        School school = null;
        String selectQuery = "select * from school where id = ?";
        Connection con = JdbcUtils.getConnection();
        try (PreparedStatement stmt = con.prepareStatement(selectQuery)) {
            stmt.setInt(1, schoolId);
            try (ResultSet rs = stmt.executeQuery()) {
                while (rs.next()) {
                    int id = rs.getInt(1);
                    int year = rs.getInt(2);
                    String name = rs.getString(3);
                    school = new School(id, name, year);
                }
            }
        }
        return school;
    }


    public static void deleteSchools() throws SQLException {
        String deleteQuery = "delete from  school";
        Connection con = JdbcUtils.getConnection();
        try (PreparedStatement stmt = con.prepareStatement(deleteQuery, Statement.RETURN_GENERATED_KEYS)) {
            stmt.executeUpdate();
        }
    }


    public static void insertGroup(School school, Group group) throws SQLException {
        String insertQuery = "insert into `group` values(?,?,?,?)";
        Connection con = JdbcUtils.getConnection();
        try (PreparedStatement stmt = con.prepareStatement(insertQuery, PreparedStatement.RETURN_GENERATED_KEYS)) {
            stmt.setNull(1, Types.INTEGER);
            stmt.setString(2, group.getName());
            stmt.setString(3, group.getRoom());
            stmt.setInt(4, school.getId());
            stmt.executeUpdate();
            try (ResultSet rs = stmt.getGeneratedKeys()) {
                while (rs.next()) {
                    group.setId(rs.getInt(1));
                }
            }
        }

    }


    public static School getSchoolByIdWithGroups(int id) throws SQLException {
        String seleqtQuery = "SELECT * FROM school join" + "`group`" + "on schoolid=school.id AND schoolid=?";
        School school = null;
        List<Group> groupList = new ArrayList<>();
        Connection con = JdbcUtils.getConnection();
        try (PreparedStatement stmt = con.prepareStatement(seleqtQuery)) {
            stmt.setInt(1, id);
            try (ResultSet rs = stmt.executeQuery()) {
                while (rs.next()) {
                    int intId = rs.getInt("group.id");
                    String groupString = rs.getString("group.name");
                    String room = rs.getString("room");
                    groupList.add(new Group(intId, groupString, room));
                    if (!rs.next()) {
                        rs.previous();
                        school = new School(rs.getInt("id"), rs.getString("school.name"), rs.getInt("year"), groupList);
                    } else rs.previous();
                }
            }
        }
        return school;
    }



    public static List<School> getSchoolsWithGroups() throws SQLException {
        List<School> schoolList = new ArrayList<>();
        List<Group> groupList = new ArrayList<>();

        String seleqtQuery = "SELECT  * FROM school join `group` on schoolid=school.id";
        Connection con = JdbcUtils.getConnection();
        try (PreparedStatement stmt = con.prepareStatement(seleqtQuery)) {
            try (ResultSet rs = stmt.executeQuery()) {
                int previousSchoolId = 0;
                while (rs.next()) {
                    if (previousSchoolId != rs.getInt("schoolid") && previousSchoolId != 0) {
                        rs.previous();
                        schoolList.add(new School(rs.getInt("id"), rs.getString("name"), rs.getInt("year"), groupList));
                        groupList = new ArrayList<>();
                        rs.next();
                    }
                    int intId = rs.getInt("group.id");
                    String groupString = rs.getString("group.name");
                    String room = rs.getString("room");
                    groupList.add(new Group(intId, groupString, room));
                    if (rs.isLast()) {
                        schoolList.add(new School(rs.getInt("id"), rs.getString("name"), rs.getInt("year"), groupList));
                    }
                    previousSchoolId = rs.getInt("schoolid");
                }
            }
        }
        return schoolList;
    }

}