package net.thumbtack.school.elections.v1.server;

import net.thumbtack.school.elections.v1.database.DataBase;
import net.thumbtack.school.elections.v1.service.CandidateService;
import net.thumbtack.school.elections.v1.service.VoterService;

import java.io.*;

public class Server {

    DataBase dataBase = null;
    VoterService voterService = null;
    CandidateService candidateService = null;

    public void startServer(String savedDataFileName) throws IOException, ClassNotFoundException {
        if (savedDataFileName != null && !savedDataFileName.equals("")) {
            try (ObjectInputStream dis = new ObjectInputStream(new FileInputStream(new File(savedDataFileName)))) {
                dataBase = (DataBase) dis.readObject();
                voterService = new VoterService(dataBase);
                candidateService = new CandidateService(dataBase);
            }
        } else {
            dataBase = new DataBase();
            voterService = new VoterService(dataBase);
            candidateService = new CandidateService(dataBase);
        }
    }

    public void startServer(File file) throws IOException, ClassNotFoundException {
        startServer(file.getAbsolutePath());
    }

    public void stopServer(String savedDataFileName) throws IOException {
        if (savedDataFileName != null && !savedDataFileName.equals("")) {
            try (ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(new File(savedDataFileName)))) {
                oos.writeObject(dataBase);
            }
        }
    }

    public void stopServer(File file) throws IOException {
        stopServer(file.getAbsolutePath());
    }

}
