package net.thumbtack.school.elections.v1.service;

import com.google.gson.Gson;
import net.thumbtack.school.elections.v1.daoimpl.ServicesDaoImpl;
import net.thumbtack.school.elections.v1.database.DataBase;
import net.thumbtack.school.elections.v1.database.DataBaseException;

public class Service {
    protected DataBase dataBase;
    protected boolean electionStarted;
    private ServicesDaoImpl servicesDao;
    protected Gson gson = new Gson();
    public Service() {
        dataBase = new DataBase();
        electionStarted = false;
    }

    public Service(DataBase dataBase) {
        this.dataBase = dataBase;
        servicesDao = new ServicesDaoImpl(dataBase);
    }

    public void startElection() {
        servicesDao.startElections();
        electionStarted = true;
    }

    public void stopElection() {
        electionStarted = false;
    }

    protected void validateToken(String token) throws DataBaseException {
        servicesDao.validateToken(token);
    }

    protected void electionsStarted() throws ServiceException {
        if (electionStarted) throw new ServiceException(ServiceErrorCode.ELECTIONS_STARTED);
    }

    protected void electionsNotStarted() throws ServiceException {
        if (!electionStarted) throw new ServiceException(ServiceErrorCode.ELECTIONS_NOT_STARTED);
    }
}
