package net.thumbtack.school.elections.v1.daoimpl;

import net.thumbtack.school.elections.v1.dao.ElectionDao;
import net.thumbtack.school.elections.v1.database.DataBase;
import net.thumbtack.school.elections.v1.database.DataBaseException;
import net.thumbtack.school.elections.v1.models.Offer;
import net.thumbtack.school.elections.v1.models.Person;
import net.thumbtack.school.elections.v1.request.election.RaitOfferDtoRequest;

import java.util.Map;
import java.util.Set;

public class ElectionDaoImpl implements ElectionDao {

    private DataBase dataBase;

    public ElectionDaoImpl(DataBase dataBase) {
        this.dataBase = dataBase;
    }

    @Override
    public String vote(String myToken, String hisToken) throws DataBaseException {
        return dataBase.vote(myToken, hisToken);
    }

    @Override
    public Set<Person> getActiveVoters() {
        return dataBase.getActiveVoters();
    }

    @Override
    public Map<String, Set<Offer>> getOfferByAuthors(Set<String> authorSet) {
        return dataBase.getOfferByAuthors(authorSet);
    }

    @Override
    public String moveToCandidates(String json) {
        return dataBase.moveToCandidate(json);
    }

    @Override
    public String moveToCandidates(String myToken, String hisToken) throws DataBaseException {
        return dataBase.moveToCandidate(myToken, hisToken);
    }

    @Override
    public Offer insertOffer(Offer offer) {
        return dataBase.insertOffer(offer);
    }

    @Override
    public Offer raitOffer(RaitOfferDtoRequest raitOfferDtoRequest) throws DataBaseException {
        return dataBase.raitOffer(raitOfferDtoRequest.getOfferString(), raitOfferDtoRequest.getToken(), raitOfferDtoRequest.getRait());
    }

    @Override
    public Offer removeRaiting(String offerString, String token) throws DataBaseException {
        return dataBase.removeRaitingFromOffer(offerString, token);
    }

    @Override
    public Set<Offer> getAllOffers() {
        return dataBase.getAllOffers();
    }

    @Override
    public Map<String, Set<Offer>> getCandidatesWithProgram(Set<String> request) {
        return dataBase.getCandidateProgramMap(request);
    }


    @Override
    public String getWinner() {
        return dataBase.getWinner();
    }


}
