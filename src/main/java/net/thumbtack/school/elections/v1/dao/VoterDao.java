package net.thumbtack.school.elections.v1.dao;

import net.thumbtack.school.elections.v1.database.DataBaseException;
import net.thumbtack.school.elections.v1.models.Person;

public interface VoterDao {
    String insert(Person person, String token) throws DataBaseException;

    boolean logIn(String login, String password);

    String logOut(String token);

    String loginPerson(String login, String password, String token);


    String iWillTheCandidate(String json);

}
