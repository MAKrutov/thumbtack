package net.thumbtack.school.elections.v1.response.candidate;

import net.thumbtack.school.elections.v1.models.Offer;

import java.util.Set;

public class RemoveOfferFromMyProgramDtoResponse {
    private Set<Offer> response;

    public RemoveOfferFromMyProgramDtoResponse(Set<Offer> response) {
        this.response = response;
    }
}
