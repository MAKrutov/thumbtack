package net.thumbtack.school.elections.v1.response.candidate;

import net.thumbtack.school.elections.v1.models.Offer;

public class AddOfferToProgramDtoResponse {
    private Offer offer;

    public AddOfferToProgramDtoResponse(Offer responseOffer) {
        offer = responseOffer;
    }

}
