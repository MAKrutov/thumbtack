package net.thumbtack.school.elections.v1.database;

public class DataBaseException extends Exception {

    private DataBaseErrorCode dataBaseErrorCode;

    public DataBaseException(DataBaseErrorCode errorCode, Throwable cause) {
        super(errorCode.getErrorString(), cause);
        dataBaseErrorCode = errorCode;
    }

    public DataBaseException(DataBaseErrorCode errorCode) {
        super(errorCode.getErrorString());
        dataBaseErrorCode = errorCode;
    }

    public DataBaseErrorCode getErrorCode() {
        return dataBaseErrorCode;
    }
}
