package net.thumbtack.school.elections.v1.daoimpl;

import net.thumbtack.school.elections.v1.dao.ServicesDao;
import net.thumbtack.school.elections.v1.database.DataBase;
import net.thumbtack.school.elections.v1.database.DataBaseException;

public class ServicesDaoImpl implements ServicesDao {
    private DataBase dataBase;

    public ServicesDaoImpl(DataBase dataBase) {
        this.dataBase = dataBase;
    }

    public void validateToken(String token) throws DataBaseException {
        dataBase.validateToken(token);
    }

    @Override
    public void startElections() {
        dataBase.startElections();
    }
}
