package net.thumbtack.school.elections.v1.dao;

import net.thumbtack.school.elections.v1.database.DataBaseException;
import net.thumbtack.school.elections.v1.models.Offer;

import java.util.Set;

public interface CandidateDao {

    Offer includeOfferToProgram(String offer, String token);

    String goToVoters(String token);

    Set<Offer> removeOfferFromProgram(String offerString, String token) throws DataBaseException;
}
