package net.thumbtack.school.elections.v1.response.election;

import net.thumbtack.school.elections.v1.models.Offer;

import java.util.Map;
import java.util.Set;

public class GetCandidatesWithProgramDtoResponse {
    private Map<String, Set<Offer>> response;


    public GetCandidatesWithProgramDtoResponse(Map<String, Set<Offer>> response) {
        this.response = response;
    }

    public Map<String, Set<Offer>> getResponse() {
        return response;
    }

}
