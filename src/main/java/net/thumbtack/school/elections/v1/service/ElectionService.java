package net.thumbtack.school.elections.v1.service;

import com.google.gson.Gson;
import net.thumbtack.school.elections.v1.daoimpl.ElectionDaoImpl;
import net.thumbtack.school.elections.v1.database.DataBase;
import net.thumbtack.school.elections.v1.database.DataBaseException;
import net.thumbtack.school.elections.v1.models.Offer;
import net.thumbtack.school.elections.v1.models.Person;
import net.thumbtack.school.elections.v1.request.election.*;
import net.thumbtack.school.elections.v1.response.ErrorResponse;
import net.thumbtack.school.elections.v1.response.election.*;

import java.util.Map;
import java.util.Set;

public class ElectionService extends Service {

    private ElectionDaoImpl electionDao;

    public ElectionService(DataBase dataBase) {
        super(dataBase);
        electionDao = new ElectionDaoImpl(dataBase);
    }

    public String addCandidate(String json) {
        AddCandidateDtoRequest addCandidateDtoRequest = gson.fromJson(json, AddCandidateDtoRequest.class);
        try {
            electionsStarted();
            validateToken(addCandidateDtoRequest.getToken());
            validateToken(addCandidateDtoRequest.getCandidateToken());
            if (addCandidateDtoRequest.getCandidateToken().equals(addCandidateDtoRequest.getToken())) {
                String responseStr = electionDao.moveToCandidates(addCandidateDtoRequest.getToken());
                AddCandidateDtoResponse response = new AddCandidateDtoResponse();
                return gson.toJson(response);
            } else {
                String responseStr = electionDao.moveToCandidates(addCandidateDtoRequest.getToken(), addCandidateDtoRequest.getCandidateToken());
                AddCandidateDtoResponse response = new AddCandidateDtoResponse();
                return gson.toJson(response);
            }
        } catch (ServiceException | DataBaseException e) {
            return gson.toJson(new ErrorResponse(e));
        }

    }


    public String makeOffer(String json) {
        OfferDtoRequest offerDtoRequest = gson.fromJson(json, OfferDtoRequest.class);
        try {
            validateToken(offerDtoRequest.getToken());
            electionsStarted();
            if (offerDtoRequest.getOfferString().length() < 10)
                throw new ServiceException(ServiceErrorCode.WRONG_OFFER);
        } catch (ServiceException | DataBaseException e) {
            return gson.toJson(new ErrorResponse(e));
        }
        Offer offer = new Offer(offerDtoRequest.getOfferString(), offerDtoRequest.getToken());
        OfferDtoResponse response = new OfferDtoResponse(electionDao.insertOffer(offer));

        return gson.toJson(response);
    }

    public String removeRaiting(String json) {
        RemoveRaitingDtoRequest removeRaitingDtoRequest = gson.fromJson(json, RemoveRaitingDtoRequest.class);
        try {
            validateToken(removeRaitingDtoRequest.getToken());
            electionsStarted();
            Offer responseOffer = electionDao.removeRaiting(removeRaitingDtoRequest.getOfferString(), removeRaitingDtoRequest.getToken());
            RemoveRaitingDtoResponse response = new RemoveRaitingDtoResponse(responseOffer);
            return gson.toJson(response);
        } catch (ServiceException | DataBaseException e) {
            return gson.toJson(new ErrorResponse(e));
        }
    }

    public String raitOffer(String json) {
        Gson gson = new Gson();
        RaitOfferDtoRequest raitOfferDtoRequest = gson.fromJson(json, RaitOfferDtoRequest.class);
        try {
            if (raitOfferDtoRequest.getRait() > 5 || raitOfferDtoRequest.getRait() < 1)
                throw new ServiceException(ServiceErrorCode.WRONG_RAITING);
            validateToken(raitOfferDtoRequest.getToken());
            electionsStarted();
            Offer responseOffer = electionDao.raitOffer(raitOfferDtoRequest);
            RaitOfferDtoResponse response = new RaitOfferDtoResponse(responseOffer);
            return gson.toJson(response);
        } catch (ServiceException | DataBaseException e) {
            return gson.toJson(new ErrorResponse(e));
        }
    }


    public String getOffersByAuthors(String json) {
        GetOfferWithAuthorDtoRequest getOfferWithAuthorDtoRequest = gson.fromJson(json, GetOfferWithAuthorDtoRequest.class);

        try {
            validateToken(getOfferWithAuthorDtoRequest.getToken());
        } catch (DataBaseException e) {
            return gson.toJson(new ErrorResponse(e));
        }
        Map<String, Set<Offer>> responseMap = electionDao.getOfferByAuthors(getOfferWithAuthorDtoRequest.getAuthorSet());
        GetOfferWithAuthorDtoResponse response = new GetOfferWithAuthorDtoResponse(responseMap);
        return gson.toJson(response);
    }

    public String vote(String json) {
        VoteDtoRequest voteDtoRequest = gson.fromJson(json, VoteDtoRequest.class);
        String responseStr;
        try {
            electionsNotStarted();
            validateToken(voteDtoRequest.getToken());
            if (voteDtoRequest.getHisToken().equals(voteDtoRequest.getToken()))
                throw new ServiceException(ServiceErrorCode.VOTE_FOR_U);
            responseStr = electionDao.vote(voteDtoRequest.getToken(), voteDtoRequest.getHisToken());
        } catch (ServiceException | DataBaseException e) {
            return gson.toJson(new ErrorResponse(e));
        }
        VoteDtoResponse response = new VoteDtoResponse(responseStr);

        return gson.toJson(response);
    }

    public String getAllOffersDescendant() {
        Set<Offer> responseSet = electionDao.getAllOffers();
        GetOfferSortedDtoResponse response = new GetOfferSortedDtoResponse(responseSet);
        return gson.toJson(response);
    }

    public String getCandidatesWithProgram(String json) {
        GetCandidatesWithProgramDtoRequest gcwp = gson.fromJson(json, GetCandidatesWithProgramDtoRequest.class);
        try {
            validateToken(gcwp.getToken());
        } catch (DataBaseException e) {
            return gson.toJson(new ErrorResponse(e));
        }
        Map<String, Set<Offer>> responseMap = electionDao.getCandidatesWithProgram(gcwp.getStringSet());
        GetCandidatesWithProgramDtoResponse response = new GetCandidatesWithProgramDtoResponse(responseMap);
        return gson.toJson(response);
    }

    public String getActiveVoters() {
        Set<Person> responseSet = electionDao.getActiveVoters();
        GetActiveVotersDtoResponse response = new GetActiveVotersDtoResponse(responseSet);
        return gson.toJson(response);
    }


    public String getWinner() {
        String responseStr = electionDao.getWinner();
        WinnerDtoResponse response = new WinnerDtoResponse(responseStr);
        return gson.toJson(response);
    }
}
