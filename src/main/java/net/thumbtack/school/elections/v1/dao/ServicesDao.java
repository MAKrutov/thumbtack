package net.thumbtack.school.elections.v1.dao;

import net.thumbtack.school.elections.v1.database.DataBaseException;

public interface ServicesDao {
    void validateToken(String token) throws DataBaseException;

    void startElections();
}
