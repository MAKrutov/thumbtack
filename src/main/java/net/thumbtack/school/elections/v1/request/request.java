package net.thumbtack.school.elections.v1.request;

public class request {
    protected String token;

    public request(String token) {
        this.token = token;
    }

    public String getToken() {
        return token;
    }
}
