package net.thumbtack.school.elections.v1.service;

import net.thumbtack.school.elections.v1.daoimpl.CandidateDaoImpl;
import net.thumbtack.school.elections.v1.database.DataBase;
import net.thumbtack.school.elections.v1.database.DataBaseException;
import net.thumbtack.school.elections.v1.models.Offer;
import net.thumbtack.school.elections.v1.request.candidate.AddOfferToProgramDtoRequest;
import net.thumbtack.school.elections.v1.request.candidate.MakeMeAVoterDtoRequest;
import net.thumbtack.school.elections.v1.request.candidate.RemoveOfferFromMyProgramDtoRequest;
import net.thumbtack.school.elections.v1.response.ErrorResponse;
import net.thumbtack.school.elections.v1.response.candidate.AddOfferToProgramDtoResponse;
import net.thumbtack.school.elections.v1.response.candidate.MakeMeAVoterDtoResponse;
import net.thumbtack.school.elections.v1.response.candidate.RemoveOfferFromMyProgramDtoResponse;

import java.util.Set;

public class CandidateService extends ElectionService {
    private CandidateDaoImpl candidateDao;

    public CandidateService(DataBase dataBase) {
        super(dataBase);
        candidateDao = new CandidateDaoImpl(dataBase);
    }

    public String addOfferToMyProgram(String json) {
        try {
            electionsStarted();
        } catch (ServiceException e) {
            return gson.toJson(new ErrorResponse(e));
        }
        AddOfferToProgramDtoRequest addOfferToProgramDtoRequest = gson.fromJson(json, AddOfferToProgramDtoRequest.class);
        Offer responseOffer = candidateDao.includeOfferToProgram(addOfferToProgramDtoRequest.getOfferString(), addOfferToProgramDtoRequest.getToken());
        AddOfferToProgramDtoResponse response = new AddOfferToProgramDtoResponse(responseOffer);
        return gson.toJson(response);
    }

    public String removeOfferFromMyProgram(String json) {
        try {
            electionsStarted();
        } catch (ServiceException e) {
            return gson.toJson(new ErrorResponse(e));
        }
        RemoveOfferFromMyProgramDtoRequest rofmp = gson.fromJson(json, RemoveOfferFromMyProgramDtoRequest.class);
        RemoveOfferFromMyProgramDtoResponse response;
        try {
            Set<Offer> responseSet = candidateDao.removeOfferFromProgram(rofmp.getOfferString(), rofmp.getToken());
            response = new RemoveOfferFromMyProgramDtoResponse(responseSet);
            return gson.toJson(response);
        } catch (DataBaseException e) {
            return gson.toJson(new ErrorResponse(e));
        }


    }

    public String makeMeAVoter(String json) {
        try {
            electionsStarted();
        } catch (ServiceException e) {
            return gson.toJson(new ErrorResponse(e));
        }
        MakeMeAVoterDtoRequest makeMeAVoterDtoRequest = gson.fromJson(json, MakeMeAVoterDtoRequest.class);
        String response = candidateDao.goToVoters(makeMeAVoterDtoRequest.getToken());
        MakeMeAVoterDtoResponse makeMeAVoterDtoResponse = new MakeMeAVoterDtoResponse(response);
        return gson.toJson(makeMeAVoterDtoResponse);
    }
}
