package net.thumbtack.school.elections.v1.models;

import java.io.Serializable;

public class Person implements Comparable<Person>, Serializable {
    private String firstName, lastName, middleName, login, password, adress, home, apartment;
    private boolean isCandidate;


    public Person(String firstName, String lastName, String middleName, String login, String password, String adress, String home, String apartment, boolean isCandidate) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.middleName = middleName;
        this.login = login;
        this.password = password;
        this.adress = adress;
        this.home = home;
        this.apartment = apartment;
        this.isCandidate = isCandidate;
    }

    public void setIsCandidate(boolean isCandidate) {
        this.isCandidate = isCandidate;
    }

    public boolean isCandidate() {
        return isCandidate;
    }


    public String getLogin() {
        return login;
    }


    public String getPassword() {
        return password;
    }


    @Override
    public String toString() {
        return (firstName + middleName + lastName + adress + home + apartment);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Person person = (Person) o;

        if (firstName != null ? !firstName.equals(person.firstName) : person.firstName != null) return false;
        if (lastName != null ? !lastName.equals(person.lastName) : person.lastName != null) return false;
        if (middleName != null ? !middleName.equals(person.middleName) : person.middleName != null) return false;
        if (adress != null ? !adress.equals(person.adress) : person.adress != null) return false;
        if (home != null ? !home.equals(person.home) : person.home != null) return false;
        return apartment != null ? apartment.equals(person.apartment) : person.apartment == null;

    }


    @Override
    public int hashCode() {
        int result = firstName != null ? firstName.hashCode() : 0;
        result = 31 * result + (lastName != null ? lastName.hashCode() : 0);
        result = 31 * result + (middleName != null ? middleName.hashCode() : 0);
        result = 31 * result + (adress != null ? adress.hashCode() : 0);
        result = 31 * result + (home != null ? home.hashCode() : 0);
        result = 31 * result + (apartment != null ? apartment.hashCode() : 0);
        return result;
    }

    @Override
    public int compareTo(Person o) {
        return this.hashCode() - o.hashCode();
    }
}
