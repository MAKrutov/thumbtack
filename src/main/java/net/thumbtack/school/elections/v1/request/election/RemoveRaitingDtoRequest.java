package net.thumbtack.school.elections.v1.request.election;

import net.thumbtack.school.elections.v1.request.request;

public class RemoveRaitingDtoRequest extends request {
    private String offerString;

    public RemoveRaitingDtoRequest(String offerString, String token) {
        super(token);
        this.offerString = offerString;
    }

    public String getOfferString() {
        return offerString;
    }

}
