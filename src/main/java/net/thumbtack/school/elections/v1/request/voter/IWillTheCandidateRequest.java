package net.thumbtack.school.elections.v1.request.voter;

import net.thumbtack.school.elections.v1.request.request;

public class IWillTheCandidateRequest extends request {

    public IWillTheCandidateRequest(String token) {
        super(token);
    }

    public String getToken() {
        return token;
    }

}
