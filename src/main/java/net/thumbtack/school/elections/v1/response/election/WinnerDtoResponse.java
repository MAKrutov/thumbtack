package net.thumbtack.school.elections.v1.response.election;

public class WinnerDtoResponse {
    private String result;

    public WinnerDtoResponse(String result) {
        this.result = result;
    }

    public String getResult() {
        return result;
    }
}
