package net.thumbtack.school.elections.v1.daoimpl;

import net.thumbtack.school.elections.v1.dao.VoterDao;
import net.thumbtack.school.elections.v1.database.DataBase;
import net.thumbtack.school.elections.v1.database.DataBaseException;
import net.thumbtack.school.elections.v1.models.Person;


public class VoterDaoImpl implements VoterDao {
    private DataBase dataBase;

    public VoterDaoImpl(DataBase dataBase) {
        this.dataBase = dataBase;
    }

    @Override
    public String insert(Person person, String token) throws DataBaseException {
        return dataBase.putPerson(person, token);
    }

    @Override
    public boolean logIn(String login, String password) {
        return dataBase.validateLoginAndPassword(login, password);
    }

    public String loginPerson(String login, String password, String token) {
        return dataBase.loginPerson(login, password, token);
    }

    @Override
    public String logOut(String token) {
        return dataBase.logOutPerson(token);
    }


    @Override
    public String iWillTheCandidate(String json) {
        return dataBase.iWillTheCandidate(json);
    }

}
