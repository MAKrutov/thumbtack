package net.thumbtack.school.elections.v1.response.election;

import net.thumbtack.school.elections.v1.models.Offer;

public class OfferDtoResponse {
    private Offer offer;

    public OfferDtoResponse(Offer offer) {
        this.offer = offer;
    }

    public Offer getOffer() {
        return offer;
    }

}
