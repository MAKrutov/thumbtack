package net.thumbtack.school.elections.v1.response.election;

public class VoteDtoResponse {
    private String hisToken;

    public VoteDtoResponse(String hisToken) {
        this.hisToken = hisToken;
    }

    public String getHisToken() {
        return hisToken;
    }

}
