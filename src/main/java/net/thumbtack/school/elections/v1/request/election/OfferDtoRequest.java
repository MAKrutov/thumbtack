package net.thumbtack.school.elections.v1.request.election;

import net.thumbtack.school.elections.v1.request.request;

public class OfferDtoRequest extends request {
    private String offerString;


    public OfferDtoRequest(String author, String offerString) {
        super(author);
        setOfferString(offerString);
    }

    public String getOfferString() {
        return offerString;
    }

    public void setOfferString(String offerString) {
        this.offerString = offerString;
    }


}
