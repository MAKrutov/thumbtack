package net.thumbtack.school.elections.v1.database;

import net.thumbtack.school.elections.v1.models.Offer;
import net.thumbtack.school.elections.v1.models.Person;
import org.apache.commons.collections4.Bag;
import org.apache.commons.collections4.BidiMap;
import org.apache.commons.collections4.SetValuedMap;
import org.apache.commons.collections4.bag.HashBag;
import org.apache.commons.collections4.bidimap.DualHashBidiMap;
import org.apache.commons.collections4.multimap.HashSetValuedHashMap;

import java.io.Serializable;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class DataBase implements Serializable {
    private final String voteAgainstAll = "Against all";

    private BidiMap<Person, String> voters = new DualHashBidiMap<>();
    private BidiMap<Person, String> candidates = new DualHashBidiMap<>();
    private Map<String, String> logins = new HashMap<>(),
            passwords = new HashMap<>();
    private Set<String> activeTokens = new HashSet<>();
    private Set<String> makeChoice = new HashSet<>();
    private BidiMap<Offer, String> offerMap = new DualHashBidiMap<>();
    private SetValuedMap<String, String> authorOfferMap = new HashSetValuedHashMap<>();
    private SetValuedMap<String, Offer> candidateProgramMap = new HashSetValuedHashMap<>();
    private Bag<String> elections = new HashBag<>();

    public String putPerson(Person person, String token) throws DataBaseException {
        if (logins.keySet().contains(person.getLogin()))
            throw new DataBaseException(DataBaseErrorCode.REPEATED_LOGIN);
        if (passwords.keySet().contains(person.getPassword()))
            throw new DataBaseException(DataBaseErrorCode.REPEATED_PASSWORD);
        if (!voters.keySet().contains(person) && !candidates.keySet().contains(person))
            if (!person.isCandidate()) {
                voters.put(person, token);
                activeTokens.add(token);
                logins.put(person.getLogin(), token);
                passwords.put(person.getPassword(), token);
                return token;
            } else {
                candidates.put(person, token);
                activeTokens.add(token);
                logins.put(person.getLogin(), token);
                passwords.put(person.getPassword(), token);
                return token;
            }
        else throw new DataBaseException(DataBaseErrorCode.USER_EXSISTS);
    }


    public boolean validateLoginAndPassword(String login, String password) {
        return (getLogins().get(login).equals(getPasswords().get(password)));
    }


    public String loginPerson(String login, String password, String token) {
        String buffToken = logins.get(login);
        logins.replace(login, buffToken, buffToken);
        passwords.replace(password, buffToken, token);
        activeTokens.add(token);
        Person person = voters.getKey(buffToken);
        voters.replace(person, buffToken, token);
        return token;
    }

    public String logOutPerson(String token) {
        activeTokens.remove(token);
        while (authorOfferMap.keySet().contains(token)) {
            for (String offerString : authorOfferMap.get(token)) {
                makeOfferToEveryone(offerString);
            }
            authorOfferMap.remove(token);
        }
        for (Offer offer : offerMap.keySet())
            offer.deleteRateWithRaiter(token);
        return token;
    }


    public String iWillTheCandidate(String token) {
        voters.getKey(token).setIsCandidate(true);
        return token;
    }

    public String moveToCandidate(String token) {
        Person person = voters.getKey(token);
        voters.remove(person, token);
        person.setIsCandidate(true);
        candidates.put(person, token);
        candidateProgramMap.put(candidates.get(person), null);
        if (authorOfferMap.keySet().contains(candidates.get(person))) {
            for (Offer offer : offerMap.keySet())
                if (offer.getAuthor().equals(token))
                    candidateProgramMap.put(token, offer);
        }
        return token;
    }

    public String moveToCandidate(String myToken, String hisToken) throws DataBaseException {
        if (voters.getKey(hisToken).isCandidate())
            moveToCandidate(hisToken);
        else
            throw new DataBaseException(DataBaseErrorCode.HE_DONT_WANT_TO_BE_THE_CANDIDATE);
        return hisToken;
    }

    public Offer raitOffer(String offerString, String token, int raiting) throws DataBaseException {
        if (!offerMap.containsValue(offerString))
            throw new DataBaseException(DataBaseErrorCode.OFFER_IS_NOT_EXISTS);
        if (token.equals(offerMap.getKey(offerString).getAuthor()))
            throw new DataBaseException(DataBaseErrorCode.WRONG_OPERATION);
        offerMap.getKey(offerString).addRaitingWithRaiter(token, raiting);
        return offerMap.getKey(offerString);
    }

    public Offer removeRaitingFromOffer(String offerString, String token) throws DataBaseException {
        if (token.equals(offerMap.getKey(offerString).getAuthor()))
            throw new DataBaseException(DataBaseErrorCode.WRONG_OPERATION);
        if (!offerMap.containsValue(offerString))
            throw new DataBaseException(DataBaseErrorCode.OFFER_IS_NOT_EXISTS);

        offerMap.getKey(offerString).deleteRateWithRaiter(token);
        return offerMap.getKey(offerString);
    }

    public Offer insertOffer(Offer offer) {
        if (!offerMap.containsKey(offer)) {
            offerMap.put(offer, offer.getOfferString());
            authorOfferMap.put(offer.getAuthor(), offer.getOfferString());
            return offer;
        } else {
            offerMap.getKey(offer).addRaitingWithRaiter(offer.getAuthor(), 5);
            return offer;
        }
    }

    public Offer includeOfferToProgram(String offerString, String token) {
        if (offerMap.containsValue(offerString)) {
            offerMap.getKey(offerString).addRaitingWithRaiter(token, 5);
            candidateProgramMap.put(token, offerMap.getKey(offerString));
        } else {
            insertOffer(new Offer(offerString, token));
            candidateProgramMap.put(token, offerMap.getKey(offerString));
        }
        return offerMap.getKey(offerString);
    }

    public void validateToken(String token) throws DataBaseException {
        if (!activeTokens.contains(token)) throw new DataBaseException(DataBaseErrorCode.TOKEN_IS_NOT_VALID);
    }

    private void makeOfferToEveryone(String offer) {
        offerMap.getKey(offer).setAuthor("Everyone");
    }

    public String goToVoters(String token) {
        candidateProgramMap.remove(token);
        Person person = candidates.removeValue(token);
        person.setIsCandidate(false);
        voters.put(person, token);
        return token;
    }

    public String vote(String myToken, String hisToken) throws DataBaseException {

        if (makeChoice.contains(myToken)) throw new DataBaseException(DataBaseErrorCode.WRONG_VOTE);
        if (hisToken.equals(voteAgainstAll)) {
            elections.add(hisToken);
            makeChoice.add(myToken);
        } else {
            elections.add(hisToken);
            makeChoice.add(myToken);
        }
        return hisToken;
    }

    public Set<Person> getActiveVoters() {
        Set<Person> buff = new HashSet<>();
        for (String string : getActiveTokens())
            if (voters.getKey(string) != null)
                buff.add(voters.getKey(string));
        return buff;
    }

    public Map<String, Set<Offer>> getOfferByAuthors(Set<String> authorSet) {

        Set<Offer> buffOffers = new HashSet<>();
        Map<String, Set<Offer>> response = new HashMap<>();

        for (String string : authorSet) {
            if (authorOfferMap.keySet().contains(string)) {
                Set<String> offerStrings = authorOfferMap.get(string);
                for (String offerString : offerStrings) {
                    buffOffers.add(offerMap.getKey(offerString));
                }
                response.put(string, buffOffers);
            }
        }
        return response;
    }

    public void startElections() {
        for (String string : candidates.values()) {
            if (candidateProgramMap.get(string).isEmpty()) {
                voters.put(candidates.getKey(string), string);
                candidates.removeValue(string);
            }
        }
    }

    public Set<Offer> getAllOffers() {
        return offerMap.keySet();
    }

    public Map<String, Set<Offer>> getCandidateProgramMap(Set<String> request) {
        Set<Offer> buffOffer;
        Map<String, Set<Offer>> buffMap = new HashMap<>();
        for (String person : request) {
            buffOffer = candidateProgramMap.get(person);
            buffMap.put(person, buffOffer);
        }
        return buffMap;
    }

    public Set<Offer> removeOfferFromProgram(String token, String offerString) throws DataBaseException {
        if (!authorOfferMap.get(token).contains(offerString))
        candidateProgramMap.get(token).remove(offerMap.getKey(offerString));
        else throw new DataBaseException(DataBaseErrorCode.WRONG_OPERATION_WITH_OFFER_BY_CANDIDATE);
        return candidateProgramMap.get(token);
    }

    public String getWinner() {
        Map<String, Double> stringDoubleMap = new HashMap<>();
        String winner = null;
        for (String string : candidates.values()) {
            stringDoubleMap.put(string, (double) elections.getCount(string) / elections.size());
        }
        stringDoubleMap.put(voteAgainstAll, (double) elections.getCount(voteAgainstAll) / elections.size());

        double max = Double.MIN_VALUE;
        for (Double temp : stringDoubleMap.values()) {
            max = Double.max(temp, max);
        }
        for (String string : stringDoubleMap.keySet()) {
            if (stringDoubleMap.get(string).equals(max))
                winner = string;
        }
        assert winner != null;
        if (winner.equals(voteAgainstAll))
            winner = "No winner";
        return winner;
    }

    public BidiMap<Offer, String> getOfferMap() {
        return offerMap;
    }


    public Set<String> getActiveTokens() {
        return activeTokens;
    }


    public Map<Person, String> getVoters() {
        return voters;
    }


    public Map<Person, String> getCandidates() {
        return candidates;
    }


    public Map<String, String> getLogins() {
        return logins;
    }


    public Map<String, String> getPasswords() {
        return passwords;
    }


    public SetValuedMap<String, String> getAuthorOfferMap() {
        return authorOfferMap;
    }


    public SetValuedMap<String, Offer> getCandidateProgramMap() {
        return candidateProgramMap;
    }


    public Bag<String> getElections() {
        return elections;
    }
}
