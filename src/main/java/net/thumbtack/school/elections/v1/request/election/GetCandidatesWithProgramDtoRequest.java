package net.thumbtack.school.elections.v1.request.election;

import net.thumbtack.school.elections.v1.request.request;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

public class GetCandidatesWithProgramDtoRequest extends request {
    private Set<String> stringSet;

    public GetCandidatesWithProgramDtoRequest(Set<String> stringSet, String token) {
        super(token);
        this.stringSet = stringSet;
    }

    public GetCandidatesWithProgramDtoRequest(String[] stringSet, String token) {
        this(new HashSet<>(Arrays.asList(stringSet)), token);
    }

    public Set<String> getStringSet() {
        return stringSet;
    }
}
