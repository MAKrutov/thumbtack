package net.thumbtack.school.elections.v1.response.election;

import net.thumbtack.school.elections.v1.models.Offer;

import java.util.Map;
import java.util.Set;

public class GetOfferWithAuthorDtoResponse {
    private Map<String, Set<Offer>> offerAuthorMap;

    public GetOfferWithAuthorDtoResponse(Map<String, Set<Offer>> offerAuthorMap) {
        this.offerAuthorMap = (offerAuthorMap);
    }

    public Map<String, Set<Offer>> getOfferAuthorMap() {
        return offerAuthorMap;
    }
}
