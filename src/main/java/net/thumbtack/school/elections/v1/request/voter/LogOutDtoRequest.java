package net.thumbtack.school.elections.v1.request.voter;

import net.thumbtack.school.elections.v1.request.request;

public class LogOutDtoRequest extends request {

    public LogOutDtoRequest(String token) {
        super(token);
    }

    public String getToken() {
        return token;
    }

}
