package net.thumbtack.school.elections.v1.response.election;

public class RegisterVoterDtoResponse {
    String token;

    public RegisterVoterDtoResponse(String token) {
        this.token = token;
    }

    public String getToken() {
        return token;
    }

}
