package net.thumbtack.school.elections.v1.response.election;

import net.thumbtack.school.elections.v1.models.Offer;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public class GetOfferSortedDtoResponse {
    private List<Offer> offerList;

    public GetOfferSortedDtoResponse(Set<Offer> offerList) {
        this.offerList = new ArrayList<>(offerList);
        this.offerList.sort((o1, o2) -> (int) -(o1.getRaiting() - o2.getRaiting()));
    }

    public List<Offer> getOfferList() {
        return offerList;
    }
}
