package net.thumbtack.school.elections.v1.database;

public enum DataBaseErrorCode {

    WRONG_VOTE("U CANT VOTE AGAIN"),
    REPEATED_LOGIN("LOGIN EXISTS"),
    REPEATED_PASSWORD("PASSWORD EXISTS"),
    USER_EXSISTS("U CANNOT REGISTER AGAIN"),
    TOKEN_IS_NOT_VALID("TOKEN IS NOT VALID"),
    HE_DONT_WANT_TO_BE_THE_CANDIDATE("HE DONT WANNA BE THE CANDIDATE"),
    OFFER_IS_NOT_EXISTS("OFFER IS NOT EXISTS"),
    WRONG_OPERATION("U CANT CHANGE RAITING YOUR OFFER, OR DELETE ITS"),
    WRONG_OPERATION_WITH_OFFER_BY_CANDIDATE("U CANT DELETE YOUR OFFER FROM PROGRAM, SORRY");

    private String message;

    DataBaseErrorCode(String message) {
        this.message = message;
    }

    public String getErrorString() {
        return message;
    }
}
