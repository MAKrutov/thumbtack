package net.thumbtack.school.elections.v1.request.candidate;

import net.thumbtack.school.elections.v1.request.request;

public class AddOfferToProgramDtoRequest extends request {
    private String offerString;

    public AddOfferToProgramDtoRequest(String offerString, String myToken) {
        super(myToken);
        this.offerString = offerString;
    }

    public String getOfferString() {
        return offerString;
    }

}
