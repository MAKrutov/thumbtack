package net.thumbtack.school.elections.v1.service;

import com.google.gson.Gson;
import net.thumbtack.school.elections.v1.daoimpl.VoterDaoImpl;
import net.thumbtack.school.elections.v1.database.DataBase;
import net.thumbtack.school.elections.v1.database.DataBaseException;
import net.thumbtack.school.elections.v1.models.Person;
import net.thumbtack.school.elections.v1.request.election.RegisterVoterDtoRequest;
import net.thumbtack.school.elections.v1.request.voter.IWillTheCandidateRequest;
import net.thumbtack.school.elections.v1.request.voter.LogOutDtoRequest;
import net.thumbtack.school.elections.v1.request.voter.LoginDtoRequest;
import net.thumbtack.school.elections.v1.response.ErrorResponse;
import net.thumbtack.school.elections.v1.response.election.RegisterVoterDtoResponse;
import net.thumbtack.school.elections.v1.response.voter.IWillTheCandidateResponse;
import net.thumbtack.school.elections.v1.response.voter.LogOutDtoResponse;
import net.thumbtack.school.elections.v1.response.voter.LoginDtoResponse;

import java.util.UUID;

public class VoterService extends ElectionService {
    private VoterDaoImpl voterDao;


    public VoterService(DataBase dataBase) {
        super(dataBase);
        voterDao = new VoterDaoImpl(dataBase);
    }

    public String registerVoter(String json) {
        Gson gson = new Gson();
        RegisterVoterDtoRequest registerVoterDtoRequest = gson.fromJson(json, RegisterVoterDtoRequest.class);
        RegisterVoterDtoResponse response;
        Person person;

        try {
            validateRegisterVoter(registerVoterDtoRequest);

            person = gson.fromJson(gson.toJson(registerVoterDtoRequest), Person.class);
            String responseStr = voterDao.insert(person, UUID.randomUUID().toString());
            response = new RegisterVoterDtoResponse(responseStr);
            return gson.toJson(response);
        } catch (ServiceException | DataBaseException e) {
            return gson.toJson(new ErrorResponse(e));
        }
    }

    public String logIn(String json) {
        Gson gson = new Gson();
        LoginDtoRequest loginDtoRequest = gson.fromJson(json, LoginDtoRequest.class);
        LoginDtoResponse response;
        try {
            electionsStarted();
            if (voterDao.logIn(loginDtoRequest.getLogin(), loginDtoRequest.getPassword())) {
                String token = UUID.randomUUID().toString();
                String responseStr = voterDao.loginPerson(loginDtoRequest.getLogin(), loginDtoRequest.getPassword(), token);
                response = new LoginDtoResponse(responseStr);

            } else throw new ServiceException(ServiceErrorCode.WRONG_ACTIVATION);
            return gson.toJson(response);
        } catch (ServiceException e) {
            return gson.toJson(new ErrorResponse(e));
        }
    }

    public String logOut(String json) {
        try {
            electionsStarted();
        } catch (ServiceException e) {
            return gson.toJson(new ErrorResponse(e));
        }
        LogOutDtoRequest logOutDtoRequest = gson.fromJson(json, LogOutDtoRequest.class);
        voterDao.logOut(logOutDtoRequest.getToken());
        LogOutDtoResponse response = new LogOutDtoResponse();
        return gson.toJson(response);
    }


    public String iWillTheCandidateIfTheyWant(String json) {

        IWillTheCandidateRequest iWillTheCandidateRequest = gson.fromJson(json, IWillTheCandidateRequest.class);
        try {
            validateToken(iWillTheCandidateRequest.getToken());
            electionsStarted();
        } catch (ServiceException | DataBaseException e) {
            return gson.toJson(new ErrorResponse(e));
        }
        IWillTheCandidateResponse response;
        voterDao.iWillTheCandidate(iWillTheCandidateRequest.getToken());
        response = new IWillTheCandidateResponse();
        return gson.toJson(response);
    }

    private void validateRegisterVoter(RegisterVoterDtoRequest registerVoterDtoRequest) throws ServiceException {
        if (registerVoterDtoRequest.getFirstName().length() < 2)
            throw new ServiceException(ServiceErrorCode.WRONG_FIRST_NAME);
        if (registerVoterDtoRequest.getLastName().length() < 2)
            throw new ServiceException(ServiceErrorCode.WRONG_LAST_NAME);
        if (registerVoterDtoRequest.getLogin().length() < 8)
            throw new ServiceException(ServiceErrorCode.WRONG_LOGIN);
        if (registerVoterDtoRequest.getPassword().length() < 8)
            throw new ServiceException(ServiceErrorCode.WEEK_PASSWORD);
    }
}
