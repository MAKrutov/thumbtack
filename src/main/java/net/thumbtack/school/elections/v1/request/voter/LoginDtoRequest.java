package net.thumbtack.school.elections.v1.request.voter;

public class LoginDtoRequest {
    private String login, password;

    public LoginDtoRequest(String login, String password) {
        this.password = password;
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public String getLogin() {
        return login;
    }

}
