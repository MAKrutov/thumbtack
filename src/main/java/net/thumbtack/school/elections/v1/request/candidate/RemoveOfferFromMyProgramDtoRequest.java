package net.thumbtack.school.elections.v1.request.candidate;

import net.thumbtack.school.elections.v1.request.request;

public class RemoveOfferFromMyProgramDtoRequest extends request {
    private String offerString;

    public RemoveOfferFromMyProgramDtoRequest(String token, String offerString) {
        super(token);
        this.offerString = offerString;
    }

    public String getToken() {
        return token;
    }

    public String getOfferString() {
        return offerString;
    }
}
