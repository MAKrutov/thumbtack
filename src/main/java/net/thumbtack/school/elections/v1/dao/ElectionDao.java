package net.thumbtack.school.elections.v1.dao;

import net.thumbtack.school.elections.v1.database.DataBaseException;
import net.thumbtack.school.elections.v1.models.Offer;
import net.thumbtack.school.elections.v1.models.Person;
import net.thumbtack.school.elections.v1.request.election.RaitOfferDtoRequest;

import java.util.Map;
import java.util.Set;

public interface ElectionDao {
    String vote(String myToken, String hisToken) throws DataBaseException;

    Set<Person> getActiveVoters();

    Map<String, Set<Offer>> getOfferByAuthors(Set<String> authorSet);

    Set<Offer> getAllOffers();

    Map<String, Set<Offer>> getCandidatesWithProgram(Set<String> request);

    String moveToCandidates(String json);

    String moveToCandidates(String myToken, String hisToken) throws DataBaseException;

    Offer insertOffer(Offer offer);

    Offer raitOffer(RaitOfferDtoRequest raitOfferDtoRequest) throws DataBaseException;


    Offer removeRaiting(String offerString, String token) throws DataBaseException;

    String getWinner();
}
