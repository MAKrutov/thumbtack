package net.thumbtack.school.elections.v1.daoimpl;

import net.thumbtack.school.elections.v1.dao.CandidateDao;
import net.thumbtack.school.elections.v1.database.DataBase;
import net.thumbtack.school.elections.v1.database.DataBaseException;
import net.thumbtack.school.elections.v1.models.Offer;

import java.util.Set;

public class CandidateDaoImpl implements CandidateDao {
    private DataBase database;

    public CandidateDaoImpl(DataBase dataBase) {
        this.database = dataBase;
    }

    @Override
    public Offer includeOfferToProgram(String offerString, String token) {
        return database.includeOfferToProgram(offerString, token);
    }

    @Override
    public String goToVoters(String token) {
        return database.goToVoters(token);
    }

    @Override
    public Set<Offer> removeOfferFromProgram(String offerString, String token) throws DataBaseException {
        return database.removeOfferFromProgram(offerString, token);
    }

}
