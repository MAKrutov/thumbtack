package net.thumbtack.school.elections.v1.models;


import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;


public class Offer implements Comparable, Serializable {
    private String offerString;
    private String author;
    private double raiting;
    private Map<String, Integer> setOfRaiting = new HashMap<>();


    public Offer(String offerString, String author) {
        setOfferString(offerString);
        setAuthor(author);
        addRaitingWithRaiter(author, 5);
    }

    public void addRaitingWithRaiter(String token, Integer raiting) {
        setOfRaiting.put(token, raiting);
        setRaiting();
    }

    public void deleteRateWithRaiter(String token) {
        setOfRaiting.remove(token);
        setRaiting();
    }

    public String getOfferString() {
        return offerString;
    }

    public void setOfferString(String offerString) {
        this.offerString = offerString;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public double getRaiting() {
        return raiting;
    }

    public void setRaiting() {
        this.raiting = getMiddleValue();
    }

    private double getMiddleValue() {
        Integer buff = 0;
        for (Integer integer : setOfRaiting.values())
            buff += integer;

        return ((double) buff / (double) setOfRaiting.size());
    }

    public Map<String, Integer> getSetOfRaiting() {
        return setOfRaiting;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Offer offer = (Offer) o;

        if (offerString != null ? !offerString.equals(offer.offerString) : offer.offerString != null) return false;
        if (author != null ? !author.equals(offer.author) : offer.author != null) return false;
        return setOfRaiting != null ? setOfRaiting.equals(offer.setOfRaiting) : offer.setOfRaiting == null;

    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        result = offerString != null ? offerString.hashCode() : 0;
        temp = Double.doubleToLongBits(raiting);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        return result;
    }

    @Override
    public int compareTo(Object o) {
        return hashCode() - o.hashCode();
    }
}
