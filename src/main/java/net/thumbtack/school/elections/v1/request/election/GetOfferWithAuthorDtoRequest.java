package net.thumbtack.school.elections.v1.request.election;

import net.thumbtack.school.elections.v1.request.request;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

public class GetOfferWithAuthorDtoRequest extends request {
    private Set<String> authorSet;

    public GetOfferWithAuthorDtoRequest(Set<String> authorSet, String token) {
        super(token);
        this.authorSet = authorSet;
    }

    public GetOfferWithAuthorDtoRequest(String[] authorStrings, String token) {
        this(new HashSet<>(Arrays.asList(authorStrings)), token);
    }

    public Set<String> getAuthorSet() {
        return authorSet;
    }
}
