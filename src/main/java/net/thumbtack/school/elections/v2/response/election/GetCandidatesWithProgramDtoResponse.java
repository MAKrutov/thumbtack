package net.thumbtack.school.elections.v2.response.election;

import net.thumbtack.school.elections.v2.models.Offer;
import net.thumbtack.school.elections.v2.models.Person;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class GetCandidatesWithProgramDtoResponse {
    private Map<String, Set<String>> response;


    public GetCandidatesWithProgramDtoResponse(Map<Person, Set<Offer>> response) {
        this.response = new HashMap<>();
        for (Person person : response.keySet()) {
            Set<String> offerStrings = new HashSet<>();
            for (Offer offer : response.get(person))
                offerStrings.add(offer.getOfferString());
            this.response.put(person.getFirstName() + " " + person.getMiddleName() + " " + person.getLastName(), offerStrings);
        }
    }

    public Map<String, Set<String>> getResponse() {
        return response;
    }

}
