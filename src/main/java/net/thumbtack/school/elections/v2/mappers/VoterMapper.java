package net.thumbtack.school.elections.v2.mappers;

import net.thumbtack.school.elections.v2.models.Person;
import org.apache.ibatis.annotations.*;

public interface VoterMapper {

    @Insert("INSERT into person (firstName, middleName,lastName, login," +
            " password, adress, home, apartment, isCandidate) " +
            "VALUES (#{person.firstName}, #{person.middleName},#{person.lastName}," +
            " #{person.login}, #{person.password}, #{person.adress}, #{person.home}," +
            " #{person.apartment}, #{person.isCandidate})")
    @Options(useGeneratedKeys = true, keyProperty = "person.id")
    int insert(@Param("person") Person person);

    @Update("INSERT into personUUID(UUID, personID) VALUES( #{token}, (SELECT id from person where login =#{login} AND password = #{password}))")
    void logIn(@Param("login") String login, @Param("password") String password, @Param("token") String token);

    @Update("DELETE from personUUID WHERE UUID = #{token}")
    void logOut(String token);

    @Update("UPDATE person set isCandidate = (1) where id in (select personID from personUUID where UUID = #{UUID}) ")
    void iWillTheCandidate(String json);

    @Insert("INSERT into personUUID (personID, UUID) values (#{personID}, #{UUID})")
    @Options(useGeneratedKeys = true, keyProperty = "id")
    void insertUuidToPerson(@Param("personID") int id, @Param("UUID") String token);

    @Delete("DELETE from person")
    void deleteAll();

    @Select("SELECT * from person WHERE id in (select personID from personUUID where UUID =#{UUID})")
    Person getPersonByUUID(String UUID);

    @Select("SELECT * from person WHERE id = #{id}")
    Person getPersonByID(int id);
}
