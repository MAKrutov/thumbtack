package net.thumbtack.school.elections.v2.daoimpl;

import net.thumbtack.school.elections.v2.dao.CommonDao;

public class CommonDaoImpl extends DaoImplBase implements CommonDao {
    @Override
    public void clear() {
        VoterDaoImpl voterDao = new VoterDaoImpl();
        ElectionDaoImpl electionService = new ElectionDaoImpl();
        electionService.clear();
        voterDao.deleteAll();
    }
}
