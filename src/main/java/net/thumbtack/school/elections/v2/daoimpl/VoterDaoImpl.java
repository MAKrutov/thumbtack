package net.thumbtack.school.elections.v2.daoimpl;

import net.thumbtack.school.elections.v2.dao.VoterDao;
import net.thumbtack.school.elections.v2.models.Person;
import net.thumbtack.school.elections.v2.utils.MyBatisUtils;
import org.apache.ibatis.session.SqlSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class VoterDaoImpl extends DaoImplBase implements VoterDao {

    private static final Logger LOGGER = LoggerFactory.getLogger(VoterDaoImpl.class);

    //Транзакционно вставляет персону, если вставка удачная, то вставляет ТОКЕН
    //если персона указала, что хочет быть кандидатом, вставляет запись в таблицу-связку
    @Override
    public String insert(Person person, String token) throws RuntimeException {
        LOGGER.debug("DAO insert person with UUID transaction {},{}", person, token);
        try (SqlSession sqlSession = MyBatisUtils.getSession()) {
            try {
                getVoterMapper(sqlSession).insert(person);
                getVoterMapper(sqlSession).insertUuidToPerson(person.getId(), token);
            } catch (RuntimeException ex) {
                LOGGER.info("Can't insert THIS person {}, because of {}", person, ex);
                sqlSession.rollback();
                throw new RuntimeException("Can't register");
            }
            sqlSession.commit();
        }

        if (person.isCandidate())
            try (SqlSession sqlSession = MyBatisUtils.getSession()) {
                try {
                    getElectionMapper(sqlSession).moveToCandidates(token);
                } catch (RuntimeException ex) {
                    LOGGER.info("Can't make him a candidate {}, because of {}", person, ex);
                    sqlSession.rollback();
                    throw new RuntimeException("Can't register");
                }
                sqlSession.commit();
            }
        return token;
    }

    //Вставляет новый токен для персоны  с данными login, password в базу данных
    @Override
    public String logIn(String login, String password, String token) throws RuntimeException {
        LOGGER.debug("DAO logIn person {}", token);
        try (SqlSession sqlSession = MyBatisUtils.getSession()) {
            try {
                getVoterMapper(sqlSession).logIn(login, password, token);
            } catch (RuntimeException ex) {
                LOGGER.info("DOA Can't logIn  person {}, because of {}", token, ex);
                sqlSession.rollback();
                throw new RuntimeException("Login or password is not correct");
            }
            sqlSession.commit();
        }
        return (token);
    }


    //Удаляет токен из базы данных
    @Override
    public String logOut(String token) throws RuntimeException {
        LOGGER.debug("DAO logOut person {}", token);
        try (SqlSession sqlSession = MyBatisUtils.getSession()) {
            try {
                getElectionMapper(sqlSession).removeRaitingFromAllOffers(token);
                getElectionMapper(sqlSession).removeAuthorFromOffers(token);
                getVoterMapper(sqlSession).logOut(token);
            } catch (RuntimeException ex) {
                LOGGER.info("Can't logOut THIS person {} because of, {}", token, ex);
                sqlSession.rollback();
                throw new RuntimeException("Can't Logout");
            }
            sqlSession.commit();
        }
        return ("Succses");
    }


    //Персона дает согласие на выдвижение в кандидаты
    @Override
    public void iWillTheCandidate(String token) throws RuntimeException {
        LOGGER.debug("DAO i will the candidate {}", token);
        try (SqlSession sqlSession = MyBatisUtils.getSession()) {
            try {
                getVoterMapper(sqlSession).iWillTheCandidate(token);
            } catch (RuntimeException ex) {
                LOGGER.info("DAO Can't i will the candidate {}", ex);
                sqlSession.rollback();
                throw new RuntimeException("Can't move to candidate");
            }
            sqlSession.commit();
        }
    }


    public void deleteAll() throws RuntimeException {
        LOGGER.debug("DAO delete all persons ");
        try (SqlSession sqlSession = MyBatisUtils.getSession()) {
            try {
                getVoterMapper(sqlSession).deleteAll();
            } catch (RuntimeException ex) {
                LOGGER.info("DAO Can't delete persons ");
                sqlSession.rollback();
                throw ex;
            }
            sqlSession.commit();
        }
    }
}
