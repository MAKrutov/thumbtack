package net.thumbtack.school.elections.v2.response;

public class ErrorResponse {
    private String error;

    public ErrorResponse(Exception e) {
        error = e.getMessage();
    }

    public String getError() {
        return error;
    }
}
