package net.thumbtack.school.elections.v2.dao;

public interface ServicesDao {
    //Проверка токена на действительност
    boolean validateToken(String token);

    boolean validateAuthorOffer(String token, String offerString);

    void startElections();
}
