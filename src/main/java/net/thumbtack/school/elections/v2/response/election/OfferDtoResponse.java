package net.thumbtack.school.elections.v2.response.election;

import net.thumbtack.school.elections.v2.models.Offer;

public class OfferDtoResponse {
    private Offer offer;

    public OfferDtoResponse(Offer offer) {
        this.offer = offer;
    }

    public Offer getOffer() {
        return offer;
    }

}
