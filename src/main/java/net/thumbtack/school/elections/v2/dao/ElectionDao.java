package net.thumbtack.school.elections.v2.dao;

import net.thumbtack.school.elections.v2.models.Offer;
import net.thumbtack.school.elections.v2.models.Person;

import java.util.List;
import java.util.Map;
import java.util.Set;

public interface ElectionDao {
    //Кладет голос в корзину
    void vote(String myToken, String hisToken);

    // Получает список активных пользователей сервера
    Set<Person> getActiveVoters();

    //Список Пользователей и их предложений
    Map<Person, List<Offer>> getOffersByAuthor(Set<Person> authorSet);

    //Получает список всех предложений
    Set<Offer> getAllOffers();

    //Получает список Всех кандидатов с их программами
    Map<Person, Set<Offer>> getCandidatesWithProgram();

    //Переводит текущего пользователя в кандидаты
    void moveToCandidates(String token);

    //Переводит другого пользователя в кандидаты, если тот согласен быть кандидатом
    void moveToCandidates(String myToken, String hisToken);

    //Добавляет Offer в базу данных
    Offer insertOffer(Offer offer, String token);

    //Оценка Offer из базы данных
    void rateOffer(String offerString, int rate, String token);

    //Удаляет оценку Offer'а из базы данных для данного пользователя
    void removeRating(String offerString, String token);

    //Определить  Победителя
    Person getWinner();

    //удалить все offers
    void clear();
}
