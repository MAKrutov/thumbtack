package net.thumbtack.school.elections.v2.request.election;

import net.thumbtack.school.elections.v2.request.request;

public class RaitOfferDtoRequest extends request {
    private String offerString;
    private int rait;

    public RaitOfferDtoRequest(String offerString, String token, int rait) {
        super(token);
        this.offerString = offerString;
        this.rait = rait;
    }

    public String getOfferString() {
        return offerString;
    }

    public void setOfferString(String offerString) {
        this.offerString = offerString;
    }

    public int getRait() {
        return rait;
    }

    public void setRait(int rait) {
        this.rait = rait;
    }
}
