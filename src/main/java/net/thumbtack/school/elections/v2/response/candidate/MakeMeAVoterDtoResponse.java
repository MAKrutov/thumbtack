package net.thumbtack.school.elections.v2.response.candidate;

public class MakeMeAVoterDtoResponse {
    private String myToken;

    public MakeMeAVoterDtoResponse(String myToken) {
        this.myToken = myToken;
    }

    public String getMyToken() {
        return myToken;
    }

}
