package net.thumbtack.school.elections.v2.request.election;

import net.thumbtack.school.elections.v2.models.Person;
import net.thumbtack.school.elections.v2.request.request;

import java.util.Set;

public class GetOfferWithAuthorDtoRequest extends request {
    private Set<Person> authorSet;

    public GetOfferWithAuthorDtoRequest(Set<Person> authorSet, String token) {
        super(token);
        this.authorSet = authorSet;
    }

    public Set<Person> getAuthorSet() {
        return authorSet;
    }
}
