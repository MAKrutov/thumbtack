package net.thumbtack.school.elections.v2.response.candidate;

import net.thumbtack.school.elections.v2.models.Offer;

import java.util.Set;

public class RemoveOfferFromMyProgramDtoResponse {
    private Set<Offer> response;

    public RemoveOfferFromMyProgramDtoResponse(Set<Offer> response) {
        this.response = response;
    }
}
