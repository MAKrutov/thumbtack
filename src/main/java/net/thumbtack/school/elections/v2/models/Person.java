package net.thumbtack.school.elections.v2.models;

import java.io.Serializable;

public class Person implements Serializable {
    private int id;
    private String firstName, lastName, middleName, login, password, adress, home, apartment;
    private boolean isCandidate;


    public Person(int id, String firstName, String lastName, String middleName, String login, String password, String adress, String home, String apartment, boolean isCandidate) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.middleName = middleName;
        this.login = login;
        this.password = password;
        this.adress = adress;
        this.home = home;
        this.apartment = apartment;
        this.isCandidate = isCandidate;
    }

    public String getFIO() {
        return firstName + " " + middleName + " " + lastName;
    }

    public Person(String firstName, String lastName, String middleName, String login, String password, String adress, String home, String apartment, boolean isCandidate) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.middleName = middleName;
        this.login = login;
        this.password = password;
        this.adress = adress;
        this.home = home;
        this.apartment = apartment;
        this.isCandidate = isCandidate;
    }

    public Person() {
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Person)) return false;

        Person person = (Person) o;

        if (getId() != person.getId()) return false;
        if (isCandidate() != person.isCandidate()) return false;
        if (getFirstName() != null ? !getFirstName().equals(person.getFirstName()) : person.getFirstName() != null)
            return false;
        if (getLastName() != null ? !getLastName().equals(person.getLastName()) : person.getLastName() != null)
            return false;
        if (getMiddleName() != null ? !getMiddleName().equals(person.getMiddleName()) : person.getMiddleName() != null)
            return false;
        if (getLogin() != null ? !getLogin().equals(person.getLogin()) : person.getLogin() != null) return false;
        if (getPassword() != null ? !getPassword().equals(person.getPassword()) : person.getPassword() != null)
            return false;
        if (getAdress() != null ? !getAdress().equals(person.getAdress()) : person.getAdress() != null) return false;
        if (getHome() != null ? !getHome().equals(person.getHome()) : person.getHome() != null) return false;
        return getApartment() != null ? getApartment().equals(person.getApartment()) : person.getApartment() == null;

    }

    @Override
    public int hashCode() {
        int result = getId();
        result = 31 * result + (getFirstName() != null ? getFirstName().hashCode() : 0);
        result = 31 * result + (getLastName() != null ? getLastName().hashCode() : 0);
        result = 31 * result + (getMiddleName() != null ? getMiddleName().hashCode() : 0);
        result = 31 * result + (getLogin() != null ? getLogin().hashCode() : 0);
        result = 31 * result + (getPassword() != null ? getPassword().hashCode() : 0);
        result = 31 * result + (getAdress() != null ? getAdress().hashCode() : 0);
        result = 31 * result + (getHome() != null ? getHome().hashCode() : 0);
        result = 31 * result + (getApartment() != null ? getApartment().hashCode() : 0);
        result = 31 * result + (isCandidate() ? 1 : 0);
        return result;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getAdress() {
        return adress;
    }

    public void setAdress(String adress) {
        this.adress = adress;
    }

    public String getHome() {
        return home;
    }

    public void setHome(String home) {
        this.home = home;
    }

    public String getApartment() {
        return apartment;
    }

    public void setApartment(String apartment) {
        this.apartment = apartment;
    }

    public boolean isCandidate() {
        return isCandidate;
    }

    public void setCandidate(boolean candidate) {
        isCandidate = candidate;
    }

}
