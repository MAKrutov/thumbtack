package net.thumbtack.school.elections.v2.response.election;

import net.thumbtack.school.elections.v2.models.Offer;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public class GetOfferSortedDtoResponse {
    List<String> offerStrings = new ArrayList<>();

    public GetOfferSortedDtoResponse(Set<Offer> offerSet) {
        List<Offer> offerList = new ArrayList<>();
        offerList = new ArrayList<>(offerSet);
        offerList.sort((o1, o2) -> (int) -(o1.getRating() - o2.getRating()));

        for (Offer offer : offerList) {
            this.offerStrings.add(offer.getOfferString());
        }
    }

    public List<String> getOfferList() {
        return offerStrings;
    }
}
