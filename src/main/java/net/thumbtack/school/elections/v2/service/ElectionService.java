package net.thumbtack.school.elections.v2.service;

import com.google.gson.Gson;
import net.thumbtack.school.elections.v2.daoimpl.ElectionDaoImpl;
import net.thumbtack.school.elections.v2.models.Offer;
import net.thumbtack.school.elections.v2.models.Person;
import net.thumbtack.school.elections.v2.request.election.*;
import net.thumbtack.school.elections.v2.response.ErrorResponse;
import net.thumbtack.school.elections.v2.response.election.*;

import java.util.List;
import java.util.Map;
import java.util.Set;

// TODO: 22.08.2019 fix it 

public class ElectionService extends Service {

    private ElectionDaoImpl electionDao;

    public ElectionService() {
        super();
        electionDao = new ElectionDaoImpl();
    }

    public String addCandidate(String json) {
        AddCandidateDtoRequest addCandidateDtoRequest = gson.fromJson(json, AddCandidateDtoRequest.class);
        try {
            electionsStarted();
            validateToken(addCandidateDtoRequest.getToken());
            validateToken(addCandidateDtoRequest.getCandidateToken());
            if (addCandidateDtoRequest.getCandidateToken().equals(addCandidateDtoRequest.getToken())) {
                electionDao.moveToCandidates(addCandidateDtoRequest.getToken());
                AddCandidateDtoResponse response = new AddCandidateDtoResponse();
                return gson.toJson(response);
            } else {
                //todo validatePersonIsCandidate
                electionDao.moveToCandidates(addCandidateDtoRequest.getToken(), addCandidateDtoRequest.getCandidateToken());
                AddCandidateDtoResponse response = new AddCandidateDtoResponse();
                return gson.toJson(response);
            }
        } catch (ServiceException e) {
            return gson.toJson(new ErrorResponse(e));
        }

    }


    public String makeOffer(String json) {
        OfferDtoRequest offerDtoRequest = gson.fromJson(json, OfferDtoRequest.class);
        try {
            validateToken(offerDtoRequest.getToken());
            electionsStarted();
            if (offerDtoRequest.getOfferString().length() < 10)
                throw new ServiceException(ServiceErrorCode.WRONG_OFFER);
        } catch (ServiceException e) {
            return gson.toJson(new ErrorResponse(e));
        }
        Offer offer = new Offer(offerDtoRequest.getOfferString());
        OfferDtoResponse response = new OfferDtoResponse(electionDao.insertOffer(offer, offerDtoRequest.getToken()));

        return gson.toJson(response);
    }

    public String removeRaiting(String json) {
        RemoveRaitingDtoRequest removeRaitingDtoRequest = gson.fromJson(json, RemoveRaitingDtoRequest.class);
        try {
            validateAuthorOffer(removeRaitingDtoRequest.getToken(), removeRaitingDtoRequest.getOfferString());
            validateToken(removeRaitingDtoRequest.getToken());
            electionsStarted();
            electionDao.removeRating(removeRaitingDtoRequest.getOfferString(), removeRaitingDtoRequest.getToken());
            return null;
        } catch (ServiceException e) {
            return gson.toJson(new ErrorResponse(e));
        }
    }

    public String raitOffer(String json) {
        Gson gson = new Gson();
        RaitOfferDtoRequest raitOfferDtoRequest = gson.fromJson(json, RaitOfferDtoRequest.class);
        try {
            validateAuthorOffer(raitOfferDtoRequest.getToken(), raitOfferDtoRequest.getOfferString());

            if (raitOfferDtoRequest.getRait() > 5 || raitOfferDtoRequest.getRait() < 1)
                throw new ServiceException(ServiceErrorCode.WRONG_RAITING);
            validateToken(raitOfferDtoRequest.getToken());
            electionsStarted();
            electionDao.rateOffer(raitOfferDtoRequest.getOfferString(), raitOfferDtoRequest.getRait(), raitOfferDtoRequest.getToken());
            return null;
        } catch (ServiceException e) {
            return gson.toJson(new ErrorResponse(e));
        }
    }


    public String getOffersByAuthors(String json) {
        GetOfferWithAuthorDtoRequest getOfferWithAuthorDtoRequest = gson.fromJson(json, GetOfferWithAuthorDtoRequest.class);
        try {
            validateToken(getOfferWithAuthorDtoRequest.getToken());
        } catch (ServiceException e) {
            return gson.toJson(new ErrorResponse(e));
        }
        Map<Person, List<Offer>> responseMap = electionDao.getOffersByAuthor(getOfferWithAuthorDtoRequest.getAuthorSet());
        GetOfferWithAuthorDtoResponse getOfferWithAuthorDtoResponse = new GetOfferWithAuthorDtoResponse(responseMap);
        return gson.toJson(getOfferWithAuthorDtoResponse);
    }

    public String vote(String json) {
        VoteDtoRequest voteDtoRequest = gson.fromJson(json, VoteDtoRequest.class);
        String responseStr;
        try {
            electionsNotStarted();
            validateToken(voteDtoRequest.getToken());
            if (voteDtoRequest.getHisToken().equals(voteDtoRequest.getToken()))
                throw new ServiceException(ServiceErrorCode.VOTE_FOR_U);
            electionDao.vote(voteDtoRequest.getToken(), voteDtoRequest.getHisToken());
        } catch (ServiceException e) {
            return gson.toJson(new ErrorResponse(e));
        }


        return null;
    }

    public String getAllOffersDescendant() {
        Set<Offer> responseSet = electionDao.getAllOffers();
        GetOfferSortedDtoResponse response = new GetOfferSortedDtoResponse(responseSet);
        String respStr = gson.toJson(response);
        return respStr;
    }

    public String getCandidatesWithProgram(String json) {
        Map<Person, Set<Offer>> responseMap = electionDao.getCandidatesWithProgram();
        GetCandidatesWithProgramDtoResponse response = new GetCandidatesWithProgramDtoResponse(responseMap);
        return gson.toJson(response);
    }

    public String getActiveVoters() {
        Set<Person> responseSet = electionDao.getActiveVoters();
        GetActiveVotersDtoResponse response = new GetActiveVotersDtoResponse(responseSet);
        return gson.toJson(response);
    }


    public String getWinner() {
        Person responseStr = electionDao.getWinner();
        WinnerDtoResponse response = new WinnerDtoResponse(responseStr);
        return gson.toJson(response);
    }
}
