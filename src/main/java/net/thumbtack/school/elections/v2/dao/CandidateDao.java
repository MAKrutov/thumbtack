package net.thumbtack.school.elections.v2.dao;

public interface CandidateDao {
    // Включает Offer в программу кандидата
    void includeOfferToProgram(String offer, String token);

    //Переводит кандидата в обычные пользователи и удаляет программу кандидата
    void goToVoters(String token);

    //Удаляет  Offer из программы кандидата
    void removeOfferFromProgram(String offerString, String token);
}
