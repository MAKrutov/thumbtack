package net.thumbtack.school.elections.v2.dao;

import net.thumbtack.school.elections.v2.models.Person;

public interface VoterDao {

    //Добавляет персону в базу данных и дает ему токен
    String insert(Person person, String token) throws Exception;

    //Даёт разлогинившемуся пользователю новый токен, обнавляет токен в базе данных
    String logIn(String login, String password, String token);

    //Удаляет токен пользователя
    String logOut(String token);

    //Пользователь соглашается стат кандидатом, если кто-то предложит его кандидадатуру
    void iWillTheCandidate(String token);

    //Удаляет всех пользователей сервера из базы данных
    void deleteAll();
}
