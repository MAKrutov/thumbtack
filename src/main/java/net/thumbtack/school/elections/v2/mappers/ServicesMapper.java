package net.thumbtack.school.elections.v2.mappers;

import org.apache.ibatis.annotations.Select;

public interface ServicesMapper {

    @Select("SELECT UUID from personUUID WHERE UUID = #{token}")
    String validateToken(String token);

    void startElections();
}
