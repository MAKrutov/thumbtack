package net.thumbtack.school.elections.v2.service;

public class ServiceException extends Exception {

    private ServiceErrorCode serviceErrorCode;

    public ServiceException(ServiceErrorCode errorCode, Throwable cause) {
        super(errorCode.getErrorString(), cause);
        serviceErrorCode = errorCode;
    }

    public ServiceException(ServiceErrorCode errorCode) {
        super(errorCode.getErrorString());
        serviceErrorCode = errorCode;
    }

    public ServiceErrorCode getErrorCode() {
        return serviceErrorCode;
    }
}
