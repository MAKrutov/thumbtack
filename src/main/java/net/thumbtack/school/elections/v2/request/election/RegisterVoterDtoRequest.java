package net.thumbtack.school.elections.v2.request.election;

public class RegisterVoterDtoRequest {
    private String firstName, lastName, middleName, login, password, adress, home, apartment;
    private boolean isCandidate;

    public RegisterVoterDtoRequest(String firstName, String lastName, String middleName, String login, String password, String adress, String home, String apartment, boolean isCandidate) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.middleName = middleName;
        this.login = login;
        this.password = password;
        this.adress = adress;
        this.home = home;
        this.apartment = apartment;
        this.isCandidate = isCandidate;
    }

    public void setIsCandidate(boolean isCandidate) {
        this.isCandidate = isCandidate;
    }

    public boolean isCandidate() {
        return isCandidate;
    }

    public String getFirstName() {
        return firstName;
    }


    public String getLastName() {
        return lastName;
    }


    public String getMiddleName() {
        return middleName;
    }


    public String getLogin() {
        return login;
    }


    public String getPassword() {
        return password;
    }


    public String getAdress() {
        return adress;
    }


    public String getHome() {
        return home;
    }


    public String getApartment() {
        return apartment;
    }

}
