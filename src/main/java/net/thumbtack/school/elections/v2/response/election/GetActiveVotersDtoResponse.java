package net.thumbtack.school.elections.v2.response.election;

import net.thumbtack.school.elections.v2.models.Person;

import java.util.Set;

public class GetActiveVotersDtoResponse {
    private Set<Person> response;

    public GetActiveVotersDtoResponse(Set<Person> response) {
        this.response = response;
    }

    public Set<Person> getResponse() {
        return response;
    }
}
