package net.thumbtack.school.elections.v2.response.election;

import net.thumbtack.school.elections.v2.models.Person;

public class WinnerDtoResponse {
    private Person result = null;

    public WinnerDtoResponse(Person result) {
        this.result = result;
    }

    public Person getResult() {
        return result;
    }
}
