package net.thumbtack.school.elections.v2.request.candidate;

import net.thumbtack.school.elections.v2.request.request;

public class RemoveOfferFromMyProgramDtoRequest extends request {
    private String offerString;

    public RemoveOfferFromMyProgramDtoRequest(String offerString, String token) {
        super(token);
        this.offerString = offerString;
    }

    public String getToken() {
        return token;
    }

    public String getOfferString() {
        return offerString;
    }
}
