package net.thumbtack.school.elections.v2.service;

import com.google.gson.Gson;
import net.thumbtack.school.elections.v2.daoimpl.ServicesDaoImpl;


public class Service {
    protected boolean electionStarted;
    protected Gson gson = new Gson();
    private ServicesDaoImpl servicesDao;

    public Service() {
        electionStarted = false;
        servicesDao = new ServicesDaoImpl();
    }


    public void startElection() {
        servicesDao.startElections();
        electionStarted = true;
    }

    public void stopElection() {
        electionStarted = false;
    }

    protected void validateToken(String token) throws ServiceException {
        if (!servicesDao.validateToken(token))
            throw new ServiceException(ServiceErrorCode.TOKEN_IS_NOT_VALID);
    }

    //    если предложение принадлежит текущей персоне - брсает исключения, запрещая операции удаления из программы и изменение оценки для персоны
    protected void validateAuthorOffer(String token, String offerString) throws ServiceException {
        if (servicesDao.validateAuthorOffer(token, offerString))
            throw new ServiceException(ServiceErrorCode.AUTHORS_BANS);

    }

    protected void electionsStarted() throws ServiceException {
        if (electionStarted) throw new ServiceException(ServiceErrorCode.ELECTIONS_STARTED);
    }

    protected void electionsNotStarted() throws ServiceException {  //TODO CHEK delete or fix IT !!!!!!!!!!
        if (!electionStarted) throw new ServiceException(ServiceErrorCode.ELECTIONS_NOT_STARTED);
    }
}
