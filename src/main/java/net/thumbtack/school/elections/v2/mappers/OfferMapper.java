package net.thumbtack.school.elections.v2.mappers;

import net.thumbtack.school.elections.v2.models.Offer;
import net.thumbtack.school.elections.v2.models.Person;
import org.apache.ibatis.annotations.*;
import org.apache.ibatis.mapping.FetchType;

import java.util.List;

public interface OfferMapper {


    @Select("SELECT * from offer ")
    @Results({
            @Result(property = "id", column = "id"),
            @Result(property = "offerString", column = "offerString"),
            @Result(property = "arrayOfRaitings", column = "id", javaType = List.class,
                    many = @Many(select = "net.thumbtack.school.elections.v2.mappers.OfferMapper.getRatingsForOfferByID", fetchType = FetchType.LAZY)),
            @Result(property = "author", column = "id", javaType = Person.class,
                    one = @One(select = "net.thumbtack.school.elections.v2.mappers.VoterMapper.getPersonByID", fetchType = FetchType.LAZY))
    })
    Offer getOfferByID(int id);

    @Select("SELECT (rate) from offerRate WHERE offerID = #{id}")
    List<Integer> getRatingsForOfferByID(int id);

    @Select("SELECT * from offer WHERE authorID = #{id}")
    @Results({
            @Result(property = "id", column = "id"),
            @Result(property = "offerString", column = "offerString"),
            @Result(property = "rateList", column = "id", javaType = List.class,
                    many = @Many(select = "net.thumbtack.school.elections.v2.mappers.OfferMapper.getRatingsForOfferByID", fetchType = FetchType.LAZY)),
            @Result(property = "author", column = "authorID", javaType = Person.class,
                    one = @One(select = "net.thumbtack.school.elections.v2.mappers.VoterMapper.getPersonByID", fetchType = FetchType.LAZY))
    })
    List<Offer> getOffersByAuthorID(int id);

    @Select("SELECT * from offer WHERE offerString = #{offerString}")
    @Results({
            @Result(property = "id", column = "id"),
            @Result(property = "offerString", column = "offerString"),
            @Result(property = "rateList", column = "id", javaType = List.class,
                    many = @Many(select = "net.thumbtack.school.elections.v2.mappers.OfferMapper.getRatingsForOfferByID", fetchType = FetchType.LAZY)),
            @Result(property = "author", column = "authorID", javaType = Person.class,
                    one = @One(select = "net.thumbtack.school.elections.v2.mappers.VoterMapper.getPersonByID", fetchType = FetchType.LAZY))
    })
    Offer getOfferByOfferString(String token);
}
