package net.thumbtack.school.elections.v2.daoimpl;


import net.thumbtack.school.elections.v2.mappers.*;
import net.thumbtack.school.elections.v2.utils.MyBatisUtils;
import org.apache.ibatis.session.SqlSession;

public class DaoImplBase {

    protected SqlSession getSession() {
        return MyBatisUtils.getSqlSessionFactory().openSession();
    }

    protected CandidateMapper getCandidateMapper(SqlSession sqlSession) {
        return sqlSession.getMapper(CandidateMapper.class);
    }

    protected ElectionMapper getElectionMapper(SqlSession sqlSession) {
        return sqlSession.getMapper(ElectionMapper.class);
    }

    protected VoterMapper getVoterMapper(SqlSession sqlSession) {
        return sqlSession.getMapper(VoterMapper.class);
    }

    protected ServicesMapper getServicesMapper(SqlSession sqlSession) {
        return sqlSession.getMapper(ServicesMapper.class);
    }


    protected OfferMapper getOfferMapper(SqlSession sqlSession) {
        return sqlSession.getMapper(OfferMapper.class);
    }

}
