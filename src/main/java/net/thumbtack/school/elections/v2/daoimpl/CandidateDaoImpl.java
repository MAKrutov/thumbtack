package net.thumbtack.school.elections.v2.daoimpl;

import net.thumbtack.school.elections.v2.dao.CandidateDao;
import net.thumbtack.school.elections.v2.utils.MyBatisUtils;
import org.apache.ibatis.session.SqlSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CandidateDaoImpl extends DaoImplBase implements CandidateDao {

    private static final Logger LOGGER = LoggerFactory.getLogger(CandidateDaoImpl.class);

    @Override
    public void includeOfferToProgram(String offer, String token) {
        LOGGER.debug("DAO Include offer to program");
        try (SqlSession sqlSession = MyBatisUtils.getSession()) {
            try {
                getCandidateMapper(sqlSession).includeOfferToProgram(offer, token);
            } catch (RuntimeException ex) {
                LOGGER.info("CAN'T INSERT OFFER TO PROGRAM {}", ex);
                sqlSession.rollback();
                throw new RuntimeException("U trying include offer again or this offer already exists");
            }
            sqlSession.commit();
        }
    }

    @Override
    public void goToVoters(String token) {
        LOGGER.debug("DAO move person from candidate to voters");
        try (SqlSession sqlSession = MyBatisUtils.getSession()) {
            try {
                getCandidateMapper(sqlSession).removeProgram(token);
                getCandidateMapper(sqlSession).goToVoters(token);
            } catch (RuntimeException ex) {
                LOGGER.info("CAN'T MOVE TO VOTERS {}", ex);
                sqlSession.rollback();
                throw ex;
            }
            sqlSession.commit();
        }
    }

    @Override
    public void removeOfferFromProgram(String offerString, String token) {
        LOGGER.debug("DAO remove offer from program");
        try (SqlSession sqlSession = MyBatisUtils.getSession()) {
            try {
                getCandidateMapper(sqlSession).removeOfferFromProgram(offerString, token);
            } catch (RuntimeException ex) {
                LOGGER.info("DAO CAN'T REMOVE OFFER FROM PROGRAM{}", ex);
                sqlSession.rollback();
                throw ex;
            }
            sqlSession.commit();
        }
    }
}
