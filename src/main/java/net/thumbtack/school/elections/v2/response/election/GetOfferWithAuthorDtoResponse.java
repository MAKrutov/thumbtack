package net.thumbtack.school.elections.v2.response.election;

import net.thumbtack.school.elections.v2.models.Offer;
import net.thumbtack.school.elections.v2.models.Person;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class GetOfferWithAuthorDtoResponse {
    private Map<String, List<String>> offerAuthorMap;

    public GetOfferWithAuthorDtoResponse(Map<Person, List<Offer>> offerAuthorMap) {
        this.offerAuthorMap = new HashMap<>();
        for (Person person : offerAuthorMap.keySet()) {
            List<String> offerStrings = new ArrayList<>();
            for (Offer offer : offerAuthorMap.get(person))
                offerStrings.add(offer.getOfferString());
            this.offerAuthorMap.put(person.getFirstName() + " " + person.getMiddleName() + " " + person.getLastName(), offerStrings);
        }
    }

    public Map<String, List<String>> getOfferAuthorMap() {
        return offerAuthorMap;
    }
}
