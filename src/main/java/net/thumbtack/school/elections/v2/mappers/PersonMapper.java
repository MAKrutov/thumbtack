package net.thumbtack.school.elections.v2.mappers;

import net.thumbtack.school.elections.v2.models.Person;
import org.apache.ibatis.annotations.Select;

public interface PersonMapper {

    @Select("SELECT * from person where id in (select personID from personUUID WHERE UUID = #{token}")
    Person getPersonByUUID(String token);
}
