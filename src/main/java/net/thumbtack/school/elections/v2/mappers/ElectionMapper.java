package net.thumbtack.school.elections.v2.mappers;

import net.thumbtack.school.elections.v2.models.Offer;
import net.thumbtack.school.elections.v2.models.Person;
import org.apache.ibatis.annotations.*;
import org.apache.ibatis.mapping.FetchType;

import java.util.List;
import java.util.Set;

public interface ElectionMapper {
    //Добавляет Offer в базу данных
    @Insert("INSERT into offer (offerString,authorID) " +
            "VALUES ( #{offer.offerString}, (SELECT personID from personUUID where UUID = #{token} ) )")
    @Options(useGeneratedKeys = true, keyProperty = "offer.id")
    void insertOffer(@Param("offer") Offer offer, @Param("token") String token);

    //Оценка Offer из базы данных
    @Insert("INSERT into offerRate (offerID, rate,raterID) " +
            "VALUES(" +
            "(SELECT id from offer where offerString =#{offerString})," +
            "#{rate}," +
            "(SELECT personID from personUUID where UUID =#{token})" +
            ") " +
            "ON DUPLICATE KEY UPDATE rate = #{rate}")
    void raitOffer(@Param("offerString") String offerString, @Param("rate") int rate, @Param("token") String token);

    //Удаляет оценку Offer'а из базы данных для данного пользователя
    @Delete("DELETE from offerRate WHERE offerID in (SELECT id from offer WHERE offerString =#{offerString}) " +
            "AND raterID = (SELECT personID from personUUID where UUID = #{token})")
    void removeRaiting(@Param("offerString") String offerString, @Param("token") String token);

    @Delete("DELETE from offerRate WHERE raterID = (SELECT personID from personUUID where UUID = #{token})")
    void removeRaitingFromAllOffers(String token);

    @Update("UPDATE offer set authorID = NULL where authorID = (SELECT personID from personUUID where UUID = #{token})")
    void removeAuthorFromOffers(String token);

    //Кладет голос в корзину
    @Insert("INSERT into voterBag (candidateID, voterID) values ((select personID from personUUID WHERE UUID  = #{candidateToken}),( select personID from personUUID WHERE UUID  = #{personToken}))  ")
    void vote(@Param("personToken") String myToken, @Param("candidateToken") String hisToken);

    // Получает список активных пользователей сервера
    @Select("SELECT * from person where id in (SELECT personID from personUUID WHERE UUID IS NOT NULL)")
    Set<Person> getActiveVoters();

    //Список Пользователей и их предложений
//    Set<Offer> getOfferByAuthors(Set<Person> authorSet);

    //Получает список всех предложений
    @Select("SELECT * from offer")
    @Results({
            @Result(property = "id", column = "id"),
            @Result(property = "offerString", column = "offerString"),
            @Result(property = "rateList", column = "id", javaType = List.class,
                    many = @Many(select = "net.thumbtack.school.elections.v2.mappers.OfferMapper.getRatingsForOfferByID", fetchType = FetchType.LAZY)),
            @Result(property = "author", column = "authorID", javaType = Person.class,
                    one = @One(select = "net.thumbtack.school.elections.v2.mappers.VoterMapper.getPersonByID", fetchType = FetchType.LAZY))
    })
    Set<Offer> getAllOffers();

    //Получает список Всех кандидатов с их программами
//    Map<Person, Set<Offer>> getCandidatesWithProgram();

    //Переводит текущего пользователя в кандидаты
    @Insert("INSERT into candidateID(candidateID)  select (personID) from personUUID WHERE UUID =  #{candidateToken}")
    void moveToCandidates(@Param("candidateToken") String token);

    @Delete("DELETE from offer")
    void clear();


    //Определить  Победителя
    @Select("SELECT candidateID from voterBag")
    List<String> getVoterBag();


    //Получить программу кандидата
    @Select("SELECT * from offer where id in (SELECT offerID from candidateOffer WHERE candidateID = #{id})")
    @Results({
            @Result(property = "id", column = "id"),
            @Result(property = "offerString", column = "offerString"),
            @Result(property = "rateList", column = "id", javaType = List.class,
                    many = @Many(select = "net.thumbtack.school.elections.v2.mappers.OfferMapper.getRatingsForOfferByID", fetchType = FetchType.LAZY)),
            @Result(property = "author", column = "authorID", javaType = Person.class,
                    one = @One(select = "net.thumbtack.school.elections.v2.mappers.VoterMapper.getPersonByID", fetchType = FetchType.LAZY))
    })
    Set<Offer> getOffersByCandidateID(int id);

    @Select("Select * from person WHERE id in (SELECT candidateID from candidateID)")
    Set<Person> getAllCandidates();
}
