package net.thumbtack.school.elections.v2.request.election;

import net.thumbtack.school.elections.v2.request.request;

public class AddCandidateDtoRequest extends request {
    private String candidateToken;

    public AddCandidateDtoRequest(String myToken, String candidateToken) {
        super(myToken);
        this.candidateToken = candidateToken;
    }

    public String getCandidateToken() {
        return candidateToken;
    }

}
