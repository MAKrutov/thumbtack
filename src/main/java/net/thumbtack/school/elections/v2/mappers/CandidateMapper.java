package net.thumbtack.school.elections.v2.mappers;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;

public interface CandidateMapper {

    @Insert("INSERT into candidateOffer (candidateID,offerID) values" +
            "((SELECT personID FROM personUUID where UUID = #{token})," +
            "(SELECT id FROM offer where offerString = #{offerString}))")
    void includeOfferToProgram(@Param("offerString") String offer, @Param("token") String token);

    @Delete("DELETE from candidateID where candidateID in (SELECT personID FROM personUUID where UUID=#{token}  )")
    void goToVoters(String token);

    @Delete("DELETE from candidateOffer  where " +
            " candidateid = (SELECT personID FROM personUUID where UUID = #{token}) " +
            "AND offerID = (SELECT id FROM offer where offerString = #{offerString})")
    void removeOfferFromProgram(@Param("offerString") String offerString, @Param("token") String token);

    @Delete("DELETE * from candidateOffer WHERE candidateID in (SELECT personID FROM personUUID where UUID=#{token}  )")
    void removeProgram(String token);

    @Insert("INSERT into candidateOffer (candidateID,offerID) VALUES " +
            "((SELECT personID FROM personUUID where UUID = #{token})," +
            "(SELECT id FROM offer where authorID = (SELECT personID FROM personUUID where UUID = #{token})))")
    void includeOffersToProgram(@Param("token") String myToken);
}
