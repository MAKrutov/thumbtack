package net.thumbtack.school.elections.v2.request.candidate;

import net.thumbtack.school.elections.v2.request.request;

public class MakeMeAVoterDtoRequest extends request {
    public MakeMeAVoterDtoRequest(String myToken) {
        super(myToken);
    }
}
