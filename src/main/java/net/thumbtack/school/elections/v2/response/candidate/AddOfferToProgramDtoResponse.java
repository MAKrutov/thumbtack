package net.thumbtack.school.elections.v2.response.candidate;

import net.thumbtack.school.elections.v2.models.Offer;

public class AddOfferToProgramDtoResponse {
    private Offer offer;

    public AddOfferToProgramDtoResponse(Offer responseOffer) {
        offer = responseOffer;
    }

}
