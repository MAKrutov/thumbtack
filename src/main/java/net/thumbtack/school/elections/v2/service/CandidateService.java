package net.thumbtack.school.elections.v2.service;

import net.thumbtack.school.elections.v2.daoimpl.CandidateDaoImpl;
import net.thumbtack.school.elections.v2.request.candidate.AddOfferToProgramDtoRequest;
import net.thumbtack.school.elections.v2.request.candidate.MakeMeAVoterDtoRequest;
import net.thumbtack.school.elections.v2.request.candidate.RemoveOfferFromMyProgramDtoRequest;
import net.thumbtack.school.elections.v2.response.ErrorResponse;

public class CandidateService extends ElectionService {
    private CandidateDaoImpl candidateDao;

    public CandidateService() {
        super();
        candidateDao = new CandidateDaoImpl();
    }

    public String addOfferToMyProgram(String json) {
        AddOfferToProgramDtoRequest addOfferToProgramDtoRequest = gson.fromJson(json, AddOfferToProgramDtoRequest.class);
        try {
            electionsStarted();
            validateToken(addOfferToProgramDtoRequest.getToken());
        } catch (ServiceException e) {
            return gson.toJson(new ErrorResponse(e));
        }
        candidateDao.includeOfferToProgram(addOfferToProgramDtoRequest.getOfferString(), addOfferToProgramDtoRequest.getToken());
        return gson.toJson("Succses");
    }

    public String removeOfferFromMyProgram(String json) {
        RemoveOfferFromMyProgramDtoRequest rofmp = gson.fromJson(json, RemoveOfferFromMyProgramDtoRequest.class);
        try {
            electionsStarted();
            validateToken(rofmp.getToken());
            validateAuthorOffer(rofmp.getToken(), rofmp.getOfferString());
        } catch (ServiceException e) {
            return gson.toJson(new ErrorResponse(e));
        }
        candidateDao.removeOfferFromProgram(rofmp.getOfferString(), rofmp.getToken());
        return gson.toJson("Succses");
    }

    public String makeMeAVoter(String json) {
        MakeMeAVoterDtoRequest makeMeAVoterDtoRequest = gson.fromJson(json, MakeMeAVoterDtoRequest.class);

        try {
            validateToken(makeMeAVoterDtoRequest.getToken());
            electionsStarted();
        } catch (ServiceException e) {
            return gson.toJson(new ErrorResponse(e));
        }
        candidateDao.goToVoters(makeMeAVoterDtoRequest.getToken());
        return gson.toJson("Succses");
    }
}
