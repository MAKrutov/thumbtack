package net.thumbtack.school.elections.v2.request.election;

import net.thumbtack.school.elections.v2.request.request;

public class VoteDtoRequest extends request {
    private String hisToken;

    public VoteDtoRequest(String myToken, String hisToken) {
        super(myToken);
        this.hisToken = hisToken;
    }

    public void setMyToken(String myToken) {
        token = myToken;
    }

    public String getHisToken() {
        return hisToken;
    }

}
