package net.thumbtack.school.elections.v2.daoimpl;

import net.thumbtack.school.elections.v2.dao.ElectionDao;
import net.thumbtack.school.elections.v2.models.Offer;
import net.thumbtack.school.elections.v2.models.Person;
import net.thumbtack.school.elections.v2.utils.MyBatisUtils;
import org.apache.commons.collections4.Bag;
import org.apache.commons.collections4.bag.HashBag;
import org.apache.commons.collections4.bidimap.DualHashBidiMap;
import org.apache.ibatis.session.SqlSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;

public class ElectionDaoImpl extends DaoImplBase implements ElectionDao {

    private static final Logger LOGGER = LoggerFactory.getLogger(ElectionDaoImpl.class);


    @Override
    public void vote(String myToken, String hisToken) {
        LOGGER.debug("DAO vote {}", hisToken);
        try (SqlSession sqlSession = MyBatisUtils.getSession()) {
            try {
                getElectionMapper(sqlSession).vote(myToken, hisToken);
            } catch (RuntimeException ex) {
                LOGGER.info("CAN'T VOTE {}", ex);
                sqlSession.rollback();
                throw new RuntimeException("U trying to vote again or other token is not candidate's token");
            }
            sqlSession.commit();
        }
    }


    //    Получает список активных пользователей сервера
    @Override
    public Set<Person> getActiveVoters() {
        LOGGER.debug("DAO get active voters ");
        try (SqlSession sqlSession = MyBatisUtils.getSession()) {
            try {
                return getElectionMapper(sqlSession).getActiveVoters();
            } catch (RuntimeException ex) {
                LOGGER.info("DAO CANT GET ACTIVE VOTERS CAUSE OF {}", ex);
                throw ex;
            }
        }
    }

    @Override
    public Map<Person, List<Offer>> getOffersByAuthor(Set<Person> authorSet) {
        Map<Person, List<Offer>> personSetOfferMap = new HashMap<>();
        LOGGER.debug("DAO GET OFFERS BY AUTHORS");
        List<Offer> offers = null;
        try (SqlSession sqlSession = MyBatisUtils.getSession()) {
            try {
                for (Person person : authorSet) {
                    offers = getOfferMapper(sqlSession).getOffersByAuthorID(person.getId());

                    for (Offer offer : offers) {
                        offer.setRaiting();
                    }
                    personSetOfferMap.put(person, offers);
                }
            } catch (RuntimeException ex) {
                LOGGER.info("DAO CAN't GET OFFERS BY AUTHORS");
                throw ex;
            }
        }
        return personSetOfferMap;
    }

    @Override
    public Set<Offer> getAllOffers() {
        LOGGER.debug("DAO GET OFFERS SORTED");
        Set<Offer> offers = null;
        try (SqlSession sqlSession = MyBatisUtils.getSession()) {
            try {
                offers = getElectionMapper(sqlSession).getAllOffers();
                for (Offer offer : offers) {
                    offer.setRaiting();
                }
            } catch (RuntimeException ex) {
                LOGGER.info("DAO CAN't GET OFFERS BY AUTHORS");
                throw ex;
            }
        }
        return offers;
    }

    @Override
    public Map<Person, Set<Offer>> getCandidatesWithProgram() {
        Map<Person, Set<Offer>> canidateOffers = new HashMap<>();

        LOGGER.debug("DAO get Candidate - Program(offers) map ");

        try (SqlSession sqlSession = MyBatisUtils.getSession()) {
            try {
                Set<Person> candidates = getElectionMapper(sqlSession).getAllCandidates();
                for (Person candidate : candidates) {
                    Set<Offer> offers;
                    offers = getElectionMapper(sqlSession).getOffersByCandidateID(candidate.getId());
                    for (Offer offer : offers) {
                        offer.setRaiting();
                    }
                    canidateOffers.put(candidate, offers);
                }
            } catch (RuntimeException ex) {
                LOGGER.info("DAO CAN'T get Candidate - Program(offers) map {}", ex);
                throw ex;
            }
        }
        return canidateOffers;
    }

    @Override
    public void moveToCandidates(String myToken) {
        LOGGER.debug("DAO MOVE TO CANDIDATES {}", myToken);
        try (SqlSession sqlSession = MyBatisUtils.getSession()) {
            try {
                //todo include all person's offers to program
                getElectionMapper(sqlSession).moveToCandidates(myToken);
                //              getCandidateMapper(sqlSession).includeOffersToProgram(myToken);
            } catch (RuntimeException ex) {
                LOGGER.info("DAO CAN'T MOVE TO CANDIDATE {}", ex);
                sqlSession.rollback();
                throw ex;
            }
            sqlSession.commit();
        }
    }

    @Override
    public void moveToCandidates(String myToken, String hisToken) {
        LOGGER.debug("DAO MOVE TO CANDIDATES {}", hisToken);
        try (SqlSession sqlSession = MyBatisUtils.getSession()) {
            try {
                //todo if person don't want to be a candidate throw exception  WRITE IN SERVICE

                getElectionMapper(sqlSession).moveToCandidates(hisToken);
            } catch (RuntimeException ex) {
                LOGGER.info("DAO CAN'T MOVE TO CANDIDATE {}", ex);
                sqlSession.rollback();
                throw ex;
            }
            sqlSession.commit();
        }
    }

    @Override
    public Offer insertOffer(Offer offer, String token) {
        LOGGER.debug("DAO insert offer {}", offer);
        try (SqlSession sqlSession = MyBatisUtils.getSession()) {
            try {
                getElectionMapper(sqlSession).insertOffer(offer, token);
                getElectionMapper(sqlSession).raitOffer(offer.getOfferString(), 5, token);
            } catch (RuntimeException ex) {
                LOGGER.info("CAN't insert offer {}", ex);
                sqlSession.rollback();
                throw ex;
            }
            sqlSession.commit();
        }
        return offer;
    }

    @Override
    public void rateOffer(String offerString, int rate, String token) {
        LOGGER.debug("DAO rate offer - {}", offerString);
        try (SqlSession sqlSession = MyBatisUtils.getSession()) {
            try {
                getElectionMapper(sqlSession).raitOffer(offerString, rate, token);
            } catch (RuntimeException ex) {
                LOGGER.info("DAO CAN't rate offer {}", ex);
                sqlSession.rollback();
                throw ex;
            }
            sqlSession.commit();
        }
    }

    @Override
    public void removeRating(String offerString, String token) {
        LOGGER.debug("DAO remove rating from offer - {}", offerString);
        try (SqlSession sqlSession = MyBatisUtils.getSession()) {
            try {
                getElectionMapper(sqlSession).removeRaiting(offerString, token);
            } catch (RuntimeException ex) {
                LOGGER.info("DAO CAN't remove rating {}", ex);
                sqlSession.rollback();
                throw ex;
            }
            sqlSession.commit();
        }
    }

    @Override
    public Person getWinner() {
        LOGGER.debug("DAO get winner");
        List<String> votesList = new ArrayList<>();
        Map<String, Integer> candidateIDvotes = new DualHashBidiMap<>();
        String winner = null;

        try (SqlSession sqlSession = MyBatisUtils.getSession()) {
            votesList = getElectionMapper(sqlSession).getVoterBag();
            Bag<String> votesBag = new HashBag<>();

            for (String string : votesList)
                votesBag.add(string);

            for (String string : votesBag.uniqueSet()) {
                candidateIDvotes.put(string, votesBag.getCount(string));
            }

            int max = Integer.MIN_VALUE;
            for (String string : candidateIDvotes.keySet()) {
                if (candidateIDvotes.get(string).intValue() > max) {
                    winner = string;
                    max = candidateIDvotes.get(string).intValue();
                }
            }

            return getVoterMapper(sqlSession).getPersonByID(Integer.valueOf(winner));

        }
    }

    @Override
    public void clear() {
        LOGGER.debug("DAO DELETE OFFERS");
        try (SqlSession sqlSession = MyBatisUtils.getSession()) {
            try {
                getElectionMapper(sqlSession).clear();
            } catch (RuntimeException ex) {
                LOGGER.info("CANT CLEAR DATABASE");
                sqlSession.rollback();
            }
            sqlSession.commit();
        }
    }


}
