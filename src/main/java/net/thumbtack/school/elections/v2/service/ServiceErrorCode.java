package net.thumbtack.school.elections.v2.service;

public enum ServiceErrorCode {
    WRONG_FIRST_NAME("INCORRECT FIRST NAME"),
    WRONG_LAST_NAME("INCORRECT LAST NAME"),
    WEEK_PASSWORD("PASSWORD IS SHORT. IT MUST BE EIGHT OR MORE CHARS"),
    WRONG_LOGIN("LOGIN IS SHORT. IT MUST BE EIGHT OR MORE CHARS"),
    WRONG_ACTIVATION("WRONG LOGIN OR PASSWORD"),
    WRONG_RAITING("Raiting can be 1, 2, 3, 4 or 5"),
    WRONG_OFFER("Offer is incorrect, must contain more then 10 chars"),
    VOTE_FOR_U("U CANNOT VOTE FOR USELF"),
    ELECTIONS_STARTED("U CAN ONLY VOTE"),
    ELECTIONS_NOT_STARTED("U CANT VOTE NOW"),
    TOKEN_IS_NOT_VALID("Token is not valid"),
    AUTHORS_BANS("U can't do it with current offer cuz u are author of the offer");

    private String message;

    ServiceErrorCode(String message) {
        this.message = message;
    }

    public String getErrorString() {
        return message;
    }
}
