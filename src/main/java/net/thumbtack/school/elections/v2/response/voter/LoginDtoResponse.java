package net.thumbtack.school.elections.v2.response.voter;

public class LoginDtoResponse {
    private String token;

    public LoginDtoResponse(String token) {
        this.token = token;
    }

    public String getToken() {
        return token;
    }

}
