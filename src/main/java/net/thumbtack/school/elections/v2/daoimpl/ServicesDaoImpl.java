package net.thumbtack.school.elections.v2.daoimpl;

import net.thumbtack.school.elections.v2.dao.ServicesDao;
import net.thumbtack.school.elections.v2.models.Offer;
import net.thumbtack.school.elections.v2.models.Person;
import net.thumbtack.school.elections.v2.utils.MyBatisUtils;
import org.apache.ibatis.session.SqlSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ServicesDaoImpl extends DaoImplBase implements ServicesDao {

    private static final Logger LOGGER = LoggerFactory.getLogger(ElectionDaoImpl.class);


    @Override
    public boolean validateToken(String token) {

        LOGGER.debug("DAO VALIDATE TOKEN {}", token);
        try (SqlSession sqlSession = MyBatisUtils.getSession()) {
            return getServicesMapper(sqlSession).validateToken(token).equals(token);
        }
    }

    public boolean validateAuthorOffer(String token, String offerString) {

        LOGGER.debug("DAO check that this person is not author of it offer");
        try (SqlSession sqlSession = MyBatisUtils.getSession()) {
            Offer offer = getOfferMapper(sqlSession).getOfferByOfferString(offerString);
            Person person = getVoterMapper(sqlSession).getPersonByUUID(token);
            return offer.getAuthor().getId() == person.getId();
        }
    }
    @Override
    public void startElections() {

    }
}
