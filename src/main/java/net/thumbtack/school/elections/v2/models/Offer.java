package net.thumbtack.school.elections.v2.models;


import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;


public class Offer implements Serializable {
    private int id;
    private String offerString;
    private Person author;
    private double rating;
    private List<Integer> rateList = new ArrayList<>();

    public Offer(int id, String offerString, Person author, double rating, List<Integer> rateList) {
        this.id = id;
        this.offerString = offerString;
        this.author = author;
        this.rating = rating;
        this.rateList = rateList;
    }

    public Offer(String offerString) {
        this.offerString = offerString;
    }

    public Offer() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    private double getMiddleValue() {
        Integer buff = 0;
        for (Integer integer : rateList)
            buff += integer;

        return ((double) buff / (double) rateList.size());
    }

    public void setRaiting() {
        this.rating = getMiddleValue();
    }

    public void addRaitingWithRaiter(String token, Integer raiting) {
        rateList.add(raiting);
        setRaiting();
    }


    public String getOfferString() {
        return offerString;
    }

    public void setOfferString(String offerString) {
        this.offerString = offerString;
    }


    public double getRating() {
        return rating;
    }

    public void setRating(double rating) {
        this.rating = rating;
    }

    public Person getAuthor() {
        return author;
    }

    public void setAuthor(Person author) {
        this.author = author;
    }

    public List<Integer> getRateList() {
        return rateList;
    }

    public void setRateList(List<Integer> arrayOfRaitings) {
        this.rateList = arrayOfRaitings;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Offer)) return false;

        Offer offer = (Offer) o;

        if (getId() != offer.getId()) return false;
        if (Double.compare(offer.getRating(), getRating()) != 0) return false;
        if (getOfferString() != null ? !getOfferString().equals(offer.getOfferString()) : offer.getOfferString() != null)
            return false;
        if (getAuthor() != null ? !getAuthor().equals(offer.getAuthor()) : offer.getAuthor() != null) return false;
        return getRateList() != null ? getRateList().equals(offer.getRateList()) : offer.getRateList() == null;

    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        result = getId();
        result = 31 * result + (getOfferString() != null ? getOfferString().hashCode() : 0);
        result = 31 * result + (getAuthor() != null ? getAuthor().hashCode() : 0);
        temp = Double.doubleToLongBits(getRating());
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        result = 31 * result + (getRateList() != null ? getRateList().hashCode() : 0);
        return result;
    }
}
