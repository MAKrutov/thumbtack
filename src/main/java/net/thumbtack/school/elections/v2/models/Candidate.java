package net.thumbtack.school.elections.v2.models;

import java.io.Serializable;
import java.util.Set;

public class Candidate implements Serializable {
    private String firstName, lastName, middleName, login, password, adress, home, apartment;
    private Set<Offer> offers;

    public Candidate(String firstName, String lastName, String middleName, String login, String password, String adress, String home, String apartment, Set<Offer> offers) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.middleName = middleName;
        this.login = login;
        this.password = password;
        this.adress = adress;
        this.home = home;
        this.apartment = apartment;
        this.offers = offers;
    }

    public Candidate() {
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getAdress() {
        return adress;
    }

    public void setAdress(String adress) {
        this.adress = adress;
    }

    public String getHome() {
        return home;
    }

    public void setHome(String home) {
        this.home = home;
    }

    public String getApartment() {
        return apartment;
    }

    public void setApartment(String apartment) {
        this.apartment = apartment;
    }

    public Set<Offer> getOffers() {
        return offers;
    }

    public void setOffers(Set<Offer> offers) {
        this.offers = offers;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Candidate)) return false;

        Candidate candidate = (Candidate) o;

        if (getFirstName() != null ? !getFirstName().equals(candidate.getFirstName()) : candidate.getFirstName() != null)
            return false;
        if (getLastName() != null ? !getLastName().equals(candidate.getLastName()) : candidate.getLastName() != null)
            return false;
        if (getMiddleName() != null ? !getMiddleName().equals(candidate.getMiddleName()) : candidate.getMiddleName() != null)
            return false;
        if (getLogin() != null ? !getLogin().equals(candidate.getLogin()) : candidate.getLogin() != null) return false;
        if (getPassword() != null ? !getPassword().equals(candidate.getPassword()) : candidate.getPassword() != null)
            return false;
        if (getAdress() != null ? !getAdress().equals(candidate.getAdress()) : candidate.getAdress() != null)
            return false;
        if (getHome() != null ? !getHome().equals(candidate.getHome()) : candidate.getHome() != null) return false;
        if (getApartment() != null ? !getApartment().equals(candidate.getApartment()) : candidate.getApartment() != null)
            return false;
        return getOffers() != null ? getOffers().equals(candidate.getOffers()) : candidate.getOffers() == null;

    }

    @Override
    public int hashCode() {
        int result = getFirstName() != null ? getFirstName().hashCode() : 0;
        result = 31 * result + (getLastName() != null ? getLastName().hashCode() : 0);
        result = 31 * result + (getMiddleName() != null ? getMiddleName().hashCode() : 0);
        result = 31 * result + (getLogin() != null ? getLogin().hashCode() : 0);
        result = 31 * result + (getPassword() != null ? getPassword().hashCode() : 0);
        result = 31 * result + (getAdress() != null ? getAdress().hashCode() : 0);
        result = 31 * result + (getHome() != null ? getHome().hashCode() : 0);
        result = 31 * result + (getApartment() != null ? getApartment().hashCode() : 0);
        result = 31 * result + (getOffers() != null ? getOffers().hashCode() : 0);
        return result;
    }
}
