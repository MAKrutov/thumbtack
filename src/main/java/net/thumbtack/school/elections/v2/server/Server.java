package net.thumbtack.school.elections.v2.server;

import net.thumbtack.school.elections.v2.service.CandidateService;
import net.thumbtack.school.elections.v2.service.VoterService;

import java.io.File;
import java.io.IOException;

public class Server {

    VoterService voterService = null;
    CandidateService candidateService = null;

    public void startServer(String savedDataFileName) throws IOException, ClassNotFoundException {
    }

    public void startServer(File file) throws IOException, ClassNotFoundException {
    }

    public void stopServer(String savedDataFileName) throws IOException {
    }

    public void stopServer(File file) throws IOException {
    }

}
