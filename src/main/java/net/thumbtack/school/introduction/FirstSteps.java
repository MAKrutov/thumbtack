package net.thumbtack.school.introduction;

public class FirstSteps {
    public int sum(int x, int y) {
        return (x + y);
    }

    public int mul(int x, int y) {
        return (x * y);
    }

    public int div(int x, int y) {
        return (x / y);
    }

    public int mod(int x, int y) {
        return (x % y);
    }

    public boolean isEqual(int x, int y) {
        return (x == y);
    }

    public boolean isGreater(int x, int y) {
        return (x > y);
    }

    public boolean isInsideRect(int xLeft, int yTop, int xRight, int yBottom, int x, int y) {
        return (((x >= xLeft) && (x <= xRight)) && ((y >= yTop) && (y <= yBottom)));
    }

    public int sum(int[] array) {
        if (array.length == 0) return 0;
        int temp = 0;
        for (int i : array)
            temp += i;

        return temp;
    }

    public int mul(int[] array) {
        if (array.length == 0) return 0;
        int temp = 1;
        for (int i : array)
            temp *= i;

        return temp;
    }

    public int min(int[] array) {
        if (array.length == 0) return Integer.MAX_VALUE;
        int temp = Integer.MAX_VALUE;
        for (int i : array) {
            temp = i < temp ? i : temp;
        }
        return temp;
    }

    public int max(int[] array) {
        if (array.length == 0) return Integer.MIN_VALUE;
        int temp = Integer.MIN_VALUE;
        for (int i : array)
            temp = i > temp ? i : temp;

        return temp;
    }

    public double average(int[] array) {
        if (array.length == 0) return 0;
        double result = 0;
        for (int i : array)
            result += i;

        return result / array.length;
    }

    public boolean isSortedDescendant(int[] array) {
        if (array.length == 0) return true;
        for (int i = 0; i < array.length - 1; i++) {
            if (array[i] <= array[i + 1]) return false;
        }
        return true;
    }

    public void cube(int[] array) {
        for (int i = 0; i < array.length; i++)
            array[i] *= array[i] * array[i];
    }

    public boolean find(int[]array, int value){
        for (int i : array)
            if (i == value) return true;
            return false;
    }


    public void reverse(int[] array) {
        int bufer;
        if (array.length >= 1)
        for (int i = 0; i < (array.length)/2; i++) {
            bufer = array[i];
            array[i] = array[array.length - 1-i];
            array[array.length - 1 - i] = bufer;
        }
    }

    public boolean isPalindrome(int[] array) {
        if (array.length == 0) return true;
        int[] buff = array.clone();
        reverse(buff);
        for (int i = 0; i < array.length; i++)
            if (buff[i] != array[i]) return false;
        return true;
    }

    public int sum(int[][] matrix) {
        int result = 0;
        for (int[] i : matrix)
            for (int j : i)
                result += j;
        return result;
    }

    public int max(int[][] matrix) {
        if ((matrix.length == 0)) return Integer.MIN_VALUE;
        int max = Integer.MIN_VALUE;
        for (int[] i : matrix)
            for (int j : i)
                max = j > max ? j : max;
        return max;
    }

    public int diagonalMax(int[][] matrix) {
        if ((matrix.length == 0)) return Integer.MIN_VALUE;
        int max = Integer.MIN_VALUE;
        for (int i = 0; i < matrix.length; i++)
            max = matrix[i][i] > max ? matrix[i][i] : max;

        return max;
    }

    public boolean isSortedDescendant(int[][] matrix){
        for (int[] i : matrix)
            if (!isSortedDescendant(i)) return false;

        return true;
    }

}