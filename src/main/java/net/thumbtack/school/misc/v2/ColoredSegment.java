package net.thumbtack.school.misc.v2;

import net.thumbtack.school.figures.v2.Point;
import net.thumbtack.school.iface.v2.Colored;


public class ColoredSegment extends Segment implements Colored {
    private int color;

    public ColoredSegment(int stX, int stY, int endX, int endY, int Width, int color) {
        super(stX, stY, endX, endY, Width);
        this.color = color;
    }

    public ColoredSegment(int Width, int color) {
        super(Width);
        this.color = color;
    }

    public ColoredSegment(Point Start, Point End, int Width, int color) {
        super(Start, End, Width);
        this.color = color;
    }
    public ColoredSegment(){
        super();
        color = 0;
    }
    @Override
    public int getColor() {
        return color;
    }

    @Override
    public void setColor(int color) {
        this.color = color;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        ColoredSegment that = (ColoredSegment) o;

        return color == that.color;

    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + color;
        return result;
    }
}
