package net.thumbtack.school.misc.v2;

import net.thumbtack.school.figures.v2.Figure;
import net.thumbtack.school.figures.v2.Point;

public class Segment extends Figure {
    private Point start = new Point(0, 0),
            end = new Point(0, 0);
    private int width = 1;

    public Segment(Point Start, Point End) {
        start = Start;
        end = End;

    }


    public Segment(Point Start, Point End, int Width) {
        this(Start,End);
        width = Width;
    }

    public Segment(int stX, int stY, int endX, int endY, int Width) {
        this(new Point(stX, stY), new Point(endX, endY), Width);
    }

    public Segment(int Width){
        end = new Point(1,0);
        width = Width;
    }

    public Segment(){
        this(1);
    }
    void setStart(Point point){
        start = point;
    }
    void setStart(int x, int y){
        start = new Point(x,y);
    }
    void setEnd(Point point){
        end = point;
    }
    void setEnd(int x, int y){
        end = new Point(x,y);
    }
    void setWidth(int width){
        this.width = width;
    }

    Point getStart (){
        return start;
    }

    Point getEnd(){
        return end;
    }
    int getWidth(){
        return width;
    }

    int getLenght(){
        return (int)(Math.sqrt((start.getX()-end.getX())*(start.getX()-end.getX())+(end.getY()-start.getY())*(end.getY()-start.getY())));
    }
    @Override
    public void moveRel(int x, int y) {
        start.moveRel(x, y);
        end.moveRel(x, y);
    }

    @Override
    public void moveTo(int x, int y) {
        int l = start.getX() - end.getX();
        int w = start.getY() - end.getY();
        start.moveTo(x, y);
        end.moveTo(start.getX() - l, start.getY() - w);
    }

    @Override
    public void moveTo(Point point) {
        moveTo(point.getX(), point.getY());
    }

    @Override
    public boolean isInside(int x, int y) {
        return ((x-start.getX())/(end.getX()-start.getX())==(y-start.getY())/(end.getY()-start.getY()));
    }

    @Override
    public double getArea() {
        return width*(Math.sqrt((start.getX()-end.getX())*(start.getX()-end.getX())+(start.getY()-end.getY())*(start.getY()-end.getY())));
    }

    @Override
    public double getPerimeter() {
        return 2*(Math.sqrt((start.getX()-end.getX())*(start.getX()-end.getX())+(start.getY()-end.getY())*(start.getY()-end.getY()))+width);
    }

    @Override
    public void resize(double ratio) {
    end.moveRel((int)(end.getX()-start.getX()*(ratio-1)),(int)((end.getY()-start.getY())*(ratio-1)));
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Segment segment = (Segment) o;

        if (width != segment.width) return false;
        if (!start.equals(segment.start)) return false;
        return end.equals(segment.end);

    }

    @Override
    public int hashCode() {
        int result = start.hashCode();
        result = 31 * result + end.hashCode();
        result = 31 * result + width;
        return result;
    }
}
