package net.thumbtack.school.misc.v2;

import net.thumbtack.school.figures.v2.Figure;
import net.thumbtack.school.figures.v2.Point;

public class Curve extends Figure {
    //Полуокружность с радиусом radius и центром в точке Center
    private Point Center = new Point(0,0);
    private int radius = 1;

    Curve (int x, int y, int radius){
        Center = new Point(x,y);
        this.radius = radius;
    }

    public Curve(Point point, int radius){
        this(point.getX(),point.getY(),radius);
    }

    public Curve(int radius){
        this(0,0,radius);
    }

    public Curve(){
        this(1);
    }

    public void setCenter(Point center) {
        Center.moveTo(center);
    }
    public void setCenter(int x, int y) {
        Center.moveTo(x,y);
    }
    public Point getCenter(){
        return Center;
    }

    public void setRadius(int radius) {
        this.radius = radius;
    }

    public int getRadius() {
        return radius;
    }

    @Override
    public void moveRel(int x, int y) {
    Center.moveRel(x,y);
    }

    @Override
    public void moveTo(int x, int y) {
    setCenter(new Point(x,y));
    }

    @Override
    public void moveTo(Point point) {
    Center.moveTo(point.getX(),point.getY());
    }

    @Override
    public boolean isInside(int x, int y) {
        double temp = (Center.getX() - x) * (Center.getX() - x) + (Center.getY() - y) * (Center.getY() - y);
        return ((getCenter().getY()<=y)&&(((int) (temp) <= (radius * radius))));
    }

    public boolean isInside(Point point) {
        return isInside(point.getX(),point.getY());
    }

    @Override
    public double getArea() {
        return Math.PI*radius*radius/2;
    }

    @Override
    public double getPerimeter() {
        return Math.PI*radius+2*radius;
    }

    @Override
    public void resize(double ratio) {
    setRadius((int)(radius*ratio));
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Curve curve = (Curve) o;

        if (radius != curve.radius) return false;
        return Center.equals(curve.Center);

    }

    @Override
    public int hashCode() {
        int result = Center.hashCode();
        result = 31 * result + radius;
        return result;
    }
}
