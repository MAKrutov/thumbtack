package net.thumbtack.school.misc.v3;

import net.thumbtack.school.colors.v3.Color;
import net.thumbtack.school.colors.v3.ColorException;
import net.thumbtack.school.figures.v3.Point;
import net.thumbtack.school.iface.v3.Colored;

import static net.thumbtack.school.colors.v3.Color.RED;
import static net.thumbtack.school.colors.v3.Color.colorFromString;


public class ColoredSegment extends Segment implements Colored {
    private Color color = RED;


    public ColoredSegment(Point Start, Point End, int Width, Color color) throws ColorException {
        super(Start, End, Width);
        this.color = color;
    }

    public ColoredSegment(int stX, int stY, int endX, int endY, int Width, Color color) throws ColorException {
        this(new Point(stX, stY), new Point(endX, endY), Width, color);
    }

    public ColoredSegment(int Width, Color color) throws ColorException {
        this(0, 0, 1, 0, Width, color);
    }

    public ColoredSegment(Point Start, Point End, int Width, String colorString) throws ColorException {
        super(Start, End, Width);
        this.color = colorFromString(colorString);
    }

    public ColoredSegment(int stX, int stY, int endX, int endY, int Width, String stringColor) throws ColorException {
        this(new Point(stX, stY), new Point(endX, endY), Width, colorFromString(stringColor));
    }

    public ColoredSegment(int Width, String colorString) throws ColorException {
        this(0, 0, 1, 0, Width, colorFromString(colorString));
    }

    public ColoredSegment() {
        super();
        color = null;
    }

    @Override
    public Color getColor() {
        return color;
    }

    @Override

    public void setColor(Color color) throws ColorException {
        if (color == null) throw new ColorException();
        this.color = color;
    }

    @Override
    public void setColor(String colorString) throws ColorException {
        color = colorFromString(colorString);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        ColoredSegment that = (ColoredSegment) o;

        return color == that.color;

    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (color != null ? color.hashCode() : 0);
        return result;
    }
}
