package net.thumbtack.school.iface.v2;

import net.thumbtack.school.figures.v2.Point;


public interface Movable {

    default void moveTo(Point point) {
    this.moveTo(point.getX(),point.getY());
    }

      void moveRel(int dx, int dy);

    void moveTo(int x, int y);



}