package net.thumbtack.school.ttschool;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Group implements Serializable {
    private String groupClass = null;
    private List<Trainee> groupList = new ArrayList<>();
    private String groupName = null;

    public Group(String name, String room) throws TrainingException {
        setName(name);
        setRoom(room);
    }

    public String getName() {
        return groupName;
    }

    public void setName(String name) throws TrainingException {
        if (name == null || name.equals("")) throw new TrainingException(TrainingErrorCode.GROUP_WRONG_NAME);
        else
            groupName = name;
    }

    public String getRoom() {
        return groupClass;
    }

    public void setRoom(String room) throws TrainingException {
        if (room == null || room.equals("")) throw new TrainingException(TrainingErrorCode.GROUP_WRONG_ROOM);
        else groupClass = room;
    }

    public List<Trainee> getTrainees() {
        return groupList;
    }

    public void addTrainee(Trainee trainee) {
        groupList.add(trainee);
    }

    public void removeTrainee(Trainee trainee) throws TrainingException {
        if (!groupList.remove(trainee)) {
            throw new TrainingException(TrainingErrorCode.TRAINEE_NOT_FOUND);
        }
    }

    public void removeTrainee(int index) throws TrainingException {
        if (index > groupList.size() - 1)
            throw new TrainingException(TrainingErrorCode.TRAINEE_NOT_FOUND);
        else groupList.remove(index);
    }

    public Trainee getTraineeByFirstName(String firstName) throws TrainingException {
        for (Trainee trainee : groupList) {
            if (trainee.getFirstName().equals(firstName))
                return trainee;
        }
        throw new TrainingException(TrainingErrorCode.TRAINEE_NOT_FOUND);
    }

    public Trainee getTraineeByFullName(String fullName) throws TrainingException {
        for (Trainee trainee : groupList) {
            if (trainee.getFullName().equals(fullName))
                return trainee;
        }
        throw new TrainingException(TrainingErrorCode.TRAINEE_NOT_FOUND);
    }

    public void sortTraineeListByFirstNameAscendant() {
        groupList.sort((o1, o2) -> o1.getFirstName().compareTo(o2.getFirstName()));
    }

    public void sortTraineeListByRatingDescendant() {
        groupList.sort((o1, o2) -> -(o1.getRating() - o2.getRating()));
    }

    public void reverseTraineeList() {
        Collections.reverse(groupList);
    }

    public void rotateTraineeList(int positions) {

        if (positions > 0)
            for (int i = 0; i < positions; i++) {
                groupList.add(groupList.size(), groupList.get(0));
                groupList.remove(0);
            }
        else for (int i = 0; i < positions; i++) {
            groupList.add(0, groupList.get(groupList.size() - 1));
            groupList.remove(groupList.size());
        }
    }

    public List<Trainee> getTraineesWithMaxRating() throws TrainingException{

        if (groupList.isEmpty()) throw new TrainingException(TrainingErrorCode.TRAINEE_NOT_FOUND);
        List<Trainee> maxRaiting = new ArrayList<>();
        sortTraineeListByRatingDescendant();
        int max = groupList.get(0).getRating();
        for (Trainee trainee : groupList) {
            if (trainee.getRating() == max) {
                maxRaiting.add(trainee);
            } else break;
        }
        return maxRaiting;
    }

    public boolean hasDuplicates() {

        for (int i=0; i < groupList.size(); i++)
            for(int j = 0; j < groupList.size(); j++)
                if(i!=j && groupList.get(i).equals(groupList.get(j))) return true;
        return false;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Group group = (Group) o;

        if (groupClass != null ? !groupClass.equals(group.groupClass) : group.groupClass != null) return false;
        if (groupList != null ? !groupList.equals(group.groupList) : group.groupList != null) return false;
        return groupName != null ? groupName.equals(group.groupName) : group.groupName == null;

    }

    @Override
    public int hashCode() {
        int result = groupClass != null ? groupClass.hashCode() : 0;
        result = 31 * result + (groupList != null ? groupList.hashCode() : 0);
        result = 31 * result + (groupName != null ? groupName.hashCode() : 0);
        return result;
    }
}
