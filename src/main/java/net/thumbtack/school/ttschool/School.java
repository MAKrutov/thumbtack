package net.thumbtack.school.ttschool;

import java.util.HashSet;
import java.util.Set;

public class School {

    private Set<Group> groupSet = new HashSet<>();
    private int year;
    private String schoolName;

     public School(String name, int year) throws TrainingException {
        setName(name);
        setYear(year);
    }

    public String getName() {
        return schoolName;
    }

    public void setName(String name) throws TrainingException {
        if (name == null || name.equals("")) throw new TrainingException(TrainingErrorCode.SCHOOL_WRONG_NAME);
        schoolName = name;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public Set<Group> getGroups() {
        return groupSet;
    }

    public void addGroup(Group group) throws TrainingException {
        if (!containsGroup(group))
            groupSet.add(group);
        else
            throw new TrainingException(TrainingErrorCode.DUPLICATE_GROUP_NAME);
    }

    public void removeGroup(Group group) throws TrainingException {
        if (groupSet.contains(group))
            groupSet.remove(group);
        else
            throw new TrainingException(TrainingErrorCode.GROUP_NOT_FOUND);

    }

    public void removeGroup(String name) throws TrainingException {
        boolean flag = true;
        for (Group group : groupSet) {
            if (group.getName().equals(name)) {
                groupSet.remove(group);
                flag = false;
                break;
            }
        }
        if (flag) throw new TrainingException(TrainingErrorCode.GROUP_NOT_FOUND);
    }

    public boolean containsGroup(Group group) {
        for (Group Group : groupSet)
            if (Group.getName().equals(group.getName()))
                return true;
        return false;
    }

}


