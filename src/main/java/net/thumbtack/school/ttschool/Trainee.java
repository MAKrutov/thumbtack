package net.thumbtack.school.ttschool;

import java.io.Serializable;

public class Trainee implements Serializable{
    private int rating;
    private String firstName, lastName;

    public Trainee(String firstName, String lastName, int rating) throws TrainingException {
        setFirstName(firstName);
        setLastName(lastName);
        setRating(rating);
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public int getRating() {
        return rating;
    }

    public String getFullName() {
        return (getFirstName() + " " + getLastName());
    }

    public void setFirstName(String firstName) throws TrainingException {
        checkFirstName(firstName);
        this.firstName = firstName;

    }

    public void setLastName(String lastName) throws TrainingException {
        checkLastName(lastName);
        this.lastName = lastName;

    }

    public void setRating(int rating) throws TrainingException {
        checkRaiting(rating);
        this.rating = rating;

    }

    public static void checkFirstName(String firstName) throws TrainingException {
        if (firstName == null || firstName.equals(""))
            throw new TrainingException(TrainingErrorCode.TRAINEE_WRONG_FIRSTNAME);
    }

    private static void checkLastName(String lastName) throws TrainingException {
        if (lastName == null || lastName.equals("")) {
            throw new TrainingException(TrainingErrorCode.TRAINEE_WRONG_LASTNAME);
        }
    }

    private static void checkRaiting(int rating) throws TrainingException {
        if (rating > 5 || rating < 1) {
            throw new TrainingException(TrainingErrorCode.TRAINEE_WRONG_RATING);
        }
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Trainee trainee = (Trainee) o;

        if (rating != trainee.rating) return false;
        if (firstName != null ? !firstName.equals(trainee.firstName) : trainee.firstName != null) return false;
        return lastName != null ? lastName.equals(trainee.lastName) : trainee.lastName == null;

    }

    @Override
    public int hashCode() {
        int result = firstName != null ? firstName.hashCode() : 0;
        result = 31 * result + (lastName != null ? lastName.hashCode() : 0);
        result = 31 * result + rating;
        return result;
    }

}
