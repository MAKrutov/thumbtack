package net.thumbtack.school.ttschool;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class TraineeMap {

    private Map<Trainee,String> traineeStringMap = new HashMap<>();


    public TraineeMap(){
        new HashMap<Trainee,String>();
    }

    public void addTraineeInfo(Trainee trainee, String institute) throws TrainingException {
        if (traineeStringMap.containsKey(trainee)) throw new TrainingException(TrainingErrorCode.DUPLICATE_TRAINEE);
        traineeStringMap.put(trainee,institute);
    }


    public void replaceTraineeInfo(Trainee trainee, String institute) throws TrainingException {
        if (traineeStringMap.containsKey(trainee)) {
            traineeStringMap.remove(trainee);
            traineeStringMap.put(trainee, institute);
        }
        else throw new TrainingException((TrainingErrorCode.TRAINEE_NOT_FOUND));
    }


    public void removeTraineeInfo(Trainee trainee) throws TrainingException{
        if (traineeStringMap.containsKey(trainee))
            traineeStringMap.remove(trainee);
        else throw new TrainingException(TrainingErrorCode.TRAINEE_NOT_FOUND);
    }


    public int getTraineesCount(){
        return traineeStringMap.size();
    }


    public String getInstituteByTrainee(Trainee trainee) throws TrainingException {
        if (traineeStringMap.containsKey(trainee))
           return traineeStringMap.get(trainee);
        else throw new TrainingException(TrainingErrorCode.TRAINEE_NOT_FOUND);

    }


    public Set<Trainee> getAllTrainees(){
        return traineeStringMap.keySet();
    }


    public Set<String> getAllInstitutes(){
        Set<String> strings = new HashSet<>();
        for (String string: traineeStringMap.values()) {
            strings.add(string);
        }
        return strings;
    }


    public boolean isAnyFromInstitute(String institute){
        for(String string:traineeStringMap.values()) {
            if(string.equals(institute))return true;
        }
        return false;
    }

}
