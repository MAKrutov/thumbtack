package net.thumbtack.school.ttschool;

enum TrainingErrorCode {
    DUPLICATE_GROUP_NAME("Duplicate group name"),
    DUPLICATE_TRAINEE("U can not put DUPLICATE of TRAINEE"),
    EMPTY_TRAINEE_QUEUE("QUEUE is empty"),
    GROUP_WRONG_ROOM("Group room can not be null"),
    GROUP_NOT_FOUND("Group not found"),
    GROUP_WRONG_NAME("Group name can not be null"),
    SCHOOL_WRONG_NAME("Name can not be null"),
    TRAINEE_WRONG_FIRSTNAME("First name can not be null"),
    TRAINEE_WRONG_LASTNAME("Last name cannot be null"),
    TRAINEE_WRONG_RATING("Raiting must be >1 & <5"),
    TRAINEE_NOT_FOUND("Trainee is not exsist");

    private String message;

    private TrainingErrorCode(String message) {
        this.message = message;
    }

    public String getErrorString() {
        return message;
    }
}
