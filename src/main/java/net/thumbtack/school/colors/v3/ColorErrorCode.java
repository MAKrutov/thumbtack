package net.thumbtack.school.colors.v3;

enum ColorErrorCode {
    WRONG_COLOR_STRING("Wrong color"),
    NULL_COLOR("Color is null");

    private String errorString = null;

    private ColorErrorCode(String message) {
        this.errorString = message;
    }

    public String getErrorString() {
        return errorString;
    }
}
