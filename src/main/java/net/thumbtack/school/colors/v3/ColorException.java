package net.thumbtack.school.colors.v3;

public class ColorException extends Exception {

    private ColorErrorCode colorErrorCode;

    public ColorException(ColorErrorCode errorCode,Throwable cause) {
        super(errorCode.getErrorString(),cause);
        this.colorErrorCode = errorCode;
    }

    public ColorException(ColorErrorCode errorCode) {
        super(errorCode.getErrorString());
    }

    public ColorException(String message) {
        super(message);
    }

    public ColorException() {
        super();
    }

    ColorErrorCode getErrorCode() {
        return colorErrorCode;
    }


}
