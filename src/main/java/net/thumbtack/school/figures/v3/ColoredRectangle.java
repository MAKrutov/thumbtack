package net.thumbtack.school.figures.v3;

import net.thumbtack.school.colors.v3.Color;
import net.thumbtack.school.colors.v3.ColorException;
import net.thumbtack.school.iface.v3.Colored;
import net.thumbtack.school.iface.v3.Stretchable;

import static net.thumbtack.school.colors.v3.Color.RED;
import static net.thumbtack.school.colors.v3.Color.colorFromString;


public class ColoredRectangle extends Rectangle implements Stretchable, Colored {

    private Color color = RED;

    public ColoredRectangle(Point leftTop, Point rightBottom, Color color) throws ColorException {
        super(leftTop, rightBottom);
        setColor(color);
    }

    public ColoredRectangle(int xLeft, int yTop, int xRight, int yBottom, Color color) throws ColorException {
        this(new Point(xLeft, yTop), new Point(xRight, yBottom), color);
    }


    public ColoredRectangle(int length, int width, Color color) throws ColorException {
        this(0, -width, length, 0, color);

    }

    public ColoredRectangle(Color color) throws ColorException {
        this(0, -1, 1, 0, color);
    }

    public ColoredRectangle(Point leftTop, Point rightBottom, String stringColor) throws ColorException {
        this(leftTop, rightBottom, colorFromString(stringColor));
    }

    public ColoredRectangle(int xLeft, int yTop, int xRight, int yBottom, String stringColor) throws ColorException {
        this(new Point(xLeft, yTop), new Point(xRight, yBottom), colorFromString(stringColor));
    }


    public ColoredRectangle(int length, int width, String stringColor) throws ColorException {
        this(0, -width, length, 0, colorFromString(stringColor));

    }

    public ColoredRectangle(String stringColor) throws ColorException {
        this(0, -1, 1, 0, colorFromString(stringColor));
    }


    @Override
    public Color getColor() {
        return color;
    }

    @Override
    public void setColor(Color color) throws ColorException {
        if (color == null) throw new ColorException("Color is null");
        this.color = color;
    }


    @Override
    public void setColor(String colorString) throws ColorException {
        this.color = colorFromString(colorString);
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        ColoredRectangle that = (ColoredRectangle) o;

        return color == that.color;

    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (color != null ? color.hashCode() : 0);
        return result;
    }
}
