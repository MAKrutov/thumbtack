package net.thumbtack.school.figures.v3;

import net.thumbtack.school.iface.v3.Stretchable;

public class Rectangle extends Figure implements Stretchable {

    private Point leftTop, rightBottom;

    public Rectangle(Point LeftTop, Point RightBottom) {
        this.leftTop = new Point(LeftTop.getX(), LeftTop.getY());
        this.rightBottom = new Point(RightBottom.getX(), RightBottom.getY());
    }

    public Rectangle(int xLeft, int yTop, int xRight, int yBottom) {
        this(new Point(xLeft, yTop), new Point(xRight, yBottom));
    }

    public Rectangle(int lenght, int width) {
        this(0, -width, lenght, 0);
    }

    public Rectangle() {
        this(0, -1, 1, 0);
    }

    public Point getTopLeft() {
        return leftTop;
    }

    public Point getBottomRight() {
        return rightBottom;
    }

    public Point setTopLeft(Point topLeft) {
        leftTop = topLeft;
        return leftTop;
    }

    public Point setBottomRight(Point bottomRight) {
        rightBottom = bottomRight;
        return rightBottom;
    }

    public int getLength() {
        return (rightBottom.getX() - leftTop.getX());
    }

    public int getWidth() {
        return (rightBottom.getY() - leftTop.getY());
    }

    public void moveTo(int x, int y) {
        int l = getLength();
        int w = getWidth();
        leftTop.moveTo(x, y);
        rightBottom.moveTo(x + w, y + w);
    }

    public void moveRel(int dx, int dy) {
        leftTop.moveRel(dx, dy);
        rightBottom.moveRel(dx, dy);
    }

    public void resize(double ratio) {
        rightBottom.moveTo((int) (rightBottom.getX() + getLength() * (ratio - 1)), (int) (rightBottom.getY() + getWidth() * (ratio - 1)));
    }

    public void stretch(double xRatio, double yRatio) {
        rightBottom.moveTo((int) (rightBottom.getX() + getLength() * (xRatio - 1)), (int) (rightBottom.getY() + getWidth() * (yRatio - 1)));
    }

    public double getArea() {
        return (getLength() * getWidth());
    }

    public double getPerimeter() {
        return (getLength() * 2 + getWidth() * 2);
    }

    public boolean isInside(int x, int y) {
        return ((x >= leftTop.getX()) && (x <= rightBottom.getX()) && (y >= leftTop.getY()) && (y <= rightBottom.getY()));
    }


    public boolean isIntersects(Rectangle rectangle) {
        return !((leftTop.getX() > rectangle.rightBottom.getX()) || (rightBottom.getX() < rectangle.leftTop.getX()) || (leftTop.getY() > rectangle.rightBottom.getY()) || (rightBottom.getY() < rectangle.leftTop.getY()));
    }

    public boolean isInside(Rectangle rectangle) {
        return (rectangle.leftTop.getX() >= this.leftTop.getX()) && (rectangle.rightBottom.getX() <= rightBottom.getX()) &&
                (rectangle.leftTop.getY() >= this.leftTop.getY()) && (rectangle.rightBottom.getY() <= this.rightBottom.getY());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Rectangle rectangle = (Rectangle) o;

        if (!leftTop.equals(rectangle.leftTop)) return false;
        return rightBottom.equals(rectangle.rightBottom);

    }

    @Override
    public int hashCode() {
        int result = leftTop.hashCode();
        result = 31 * result + rightBottom.hashCode();
        return result;
    }


}
