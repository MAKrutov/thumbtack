package net.thumbtack.school.figures.v1;

public class ColoredCircle extends Circle {

    private int color = 1;
    // Создает ColoredCircle по координатам центра, значению радиуса и цвету.
    public ColoredCircle(Point center, int radius, int color) {
        super(center, radius);
        setColor(color);
    }


    public ColoredCircle(int xCenter, int yCenter, int radius, int color)
//    Создает ColoredCircle по координатам центра, значению радиуса и цвету.
    {
        super(xCenter,yCenter,radius);
        setColor(color);
    }

        public ColoredCircle(int radius, int color)
//    Создает ColoredCircle  с центром в точке (0, 0) с указанными радиусом и цветом.
        {
            super(radius);
            setColor(color);
        }
    public ColoredCircle(int color)
//    Создает ColoredCircle  с центром в точке (0, 0) с радиусом 1 и указанным цветом.
    {
        super();
        setColor(color);
    }
    public ColoredCircle()
//    Создает ColoredCircle  с центром в точке (0, 0) с радиусом 1 и цветом 1.
    {
        super();
    }

    public int getColor()
//    Возвращает цвет ColoredCircle.
    {
        return color;
    }

    public void setColor(int color)
//    Устанавливает цвет ColoredCircle.
    {
        this.color = color;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        ColoredCircle that = (ColoredCircle) o;

        return color == that.color;

    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + color;
        return result;
    }
}
