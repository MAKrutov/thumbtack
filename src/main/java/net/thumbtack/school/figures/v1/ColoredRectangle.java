package net.thumbtack.school.figures.v1;

public class ColoredRectangle extends Rectangle {
    private int color = 1;

    public ColoredRectangle(Point leftTop, Point rightBottom, int color) {
        super(leftTop, rightBottom);
        setColor(color);
    }

    public ColoredRectangle(int xLeft, int yTop, int xRight, int yBottom, int color) {
        super(xLeft, yTop, xRight, yBottom);
        setColor(color);
    }

    //
    public ColoredRectangle(int length, int width, int color) {
        super(length, width);
        setColor(color);
    }

    public ColoredRectangle(int color) {
        super();
        setColor(color);
    }

    public ColoredRectangle() {
        this(1);
    }

    public int getColor() {
        return color;
    }

    public void setColor(int color) {
        this.color = color;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        ColoredRectangle that = (ColoredRectangle) o;

        return color == that.color;

    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + color;
        return result;
    }
}
