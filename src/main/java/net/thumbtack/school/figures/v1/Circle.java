package net.thumbtack.school.figures.v1;

public class Circle {


    private Point center;
    private int r;


    public Circle(Point center, int radius) {
        this.center = center;
        r = radius;
    }

    public Circle(int xcenter, int ycenter, int radius) {
        this(new Point(xcenter, ycenter), radius);
    }

    public Circle(int radius) {
        this(0, 0, radius);

    }

    public Circle() {
        this(0, 0, 1);
    }

    public Point getCenter() {
        return center;
    }


    public int getRadius() {
        return r;
    }


    public void setCenter(Point center) {
        this.center = center;
    }


    public void setRadius(int radius) {
        r = radius;
    }


    public void moveTo(int x, int y) {
        center.moveTo(x, y);
    }


    public void moveTo(Point point) {
        center.moveTo(point.getX(), point.getY());
    }


    public void moveRel(int dx, int dy) {
        center.moveRel(dx, dy);
    }


    public void resize(double ratio) {
        r = (int) (r * ratio);
    }


    public double getArea() {
        return Math.PI * r * r;
    }


    public double getPerimeter() {
        return 2 * Math.PI * r;
    }


    public boolean isInside(int x, int y) {
        double temp = (center.getX() - x) * (center.getX() - x) + (center.getY() - y) * (center.getY() - y);
        return ((int) (temp) <= (r * r));
    }

    public boolean isInside(Point point) {
        return isInside(point.getX(), point.getY());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Circle circle = (Circle) o;

        if (r != circle.r) return false;
        return center.equals(circle.center);

    }

    @Override
    public int hashCode() {
        int result = center.hashCode();
        result = 31 * result + r;
        return result;
    }
}
