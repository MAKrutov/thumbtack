package net.thumbtack.school.figures.v2;

import net.thumbtack.school.iface.v2.HasArea;
import net.thumbtack.school.iface.v2.Movable;
import net.thumbtack.school.iface.v2.Resizable;

public abstract class Figure implements HasArea, Resizable, Movable {



    public abstract void moveRel(int x, int y);

    public abstract void moveTo(int x, int y);
    public  void moveTo(Point point){
        this.moveTo(point.getX(),point.getY());
    }

    public boolean isInside(Point point) {
        return isInside(point.getX(), point.getY());
    }
    public abstract boolean isInside(int x, int y);

    public abstract double getArea();
    public abstract double getPerimeter();


}
