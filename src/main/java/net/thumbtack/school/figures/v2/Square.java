package net.thumbtack.school.figures.v2;

public class Square extends Figure{
    int size = 0;

    private Point leftTop, rightBottom;

    public Square(Point leftTop, int size) {
        this.size = size;
        this.leftTop = leftTop;
        rightBottom = new Point(leftTop.getX() + size, leftTop.getY() + size);
    }

    public Square(int xLeft, int yTop, int size) {
        this(new Point(xLeft, yTop), size);
    }

    public Square(int size) {
        leftTop = new Point(0, -size);
        rightBottom = new Point(size, 0);
        this.size = size;
    }

    public Square() {
        this(1);
    }

    public Point getTopLeft() {
        return leftTop;
    }


    public void setTopLeft(Point topLeft) {
        leftTop = topLeft;
        rightBottom = new Point(leftTop.getX() + size, leftTop.getY() + size);
    }

    public Point getBottomRight() {
        return rightBottom;
    }

    public int getLength() {
        return size;
    }

    public void moveTo(int x, int y) {
        leftTop.moveTo(x, y);
        rightBottom.moveTo(x + size, y + size);
    }

    public void moveTo(Point point) {
        moveTo(point.getX(), point.getY());
    }

    public void moveRel(int dx, int dy) {
        leftTop.moveRel(dx, dy);
        rightBottom.moveRel(dx, dy);
    }

    public void resize(double ratio) {
        rightBottom.moveTo((int) (rightBottom.getX() + getLength() * (ratio - 1)), (int) (rightBottom.getY() + getLength() * (ratio - 1)));
        size *= ratio;
    }

    public double getArea() {
        return size * size;
    }

    public double getPerimeter() {
        return size * 4;
    }

    
    public boolean isInside(int x, int y)
    {
        return (x >= leftTop.getX()) && (x <= rightBottom.getX()) && (y >= leftTop.getY()) && (y <= rightBottom.getY());
    }

    public boolean isIntersects(Square square) {
        return (leftTop.getX() <= square.rightBottom.getX()) && (rightBottom.getX() >= square.leftTop.getX()) && (leftTop.getY() <= square.rightBottom.getY()) && (rightBottom.getY() >= square.leftTop.getY());
    }


    public boolean isInside(Square Square) {
        int maxSize = Square.leftTop.getX() - this.leftTop.getX();
        if (Square.leftTop.getY() - this.leftTop.getY() > maxSize)
            maxSize = Square.leftTop.getY() - this.leftTop.getY();
        if (maxSize > Square.size) return false;
        return (this.leftTop.getX() <= Square.leftTop.getX()) &&
                (this.leftTop.getY() <= Square.leftTop.getY()) && (Square.size <= this.size);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Square square = (Square) o;

        if (size != square.size) return false;
        return leftTop.equals(square.leftTop);

    }

    @Override
    public int hashCode() {
        int result = size;
        result = 31 * result + leftTop.hashCode();
        return result;
    }

}
