package net.thumbtack.school.figures.v2;

import net.thumbtack.school.iface.v2.Stretchable;

public class Ellipse extends Figure implements  Stretchable{
    private Point center;
    private int xAxis, yAxis;

    public Ellipse(Point center, int xAxis, int yAxis) {
        this.center = center;
        this.xAxis = xAxis;
        this.yAxis = yAxis;
    }

    public Ellipse(int xCenter, int yCenter, int xAxis, int yAxis) {
        this(new Point(xCenter, yCenter), xAxis, yAxis);
    }//

    public Ellipse(int xAxis, int yAxis) {
        this(0, 0, xAxis, yAxis);
    }


    public Ellipse() {
        this(new Point(0, 0), 1, 1);
    }

    public Point getCenter() {
        return center;
    }


    public int getXAxis() {
        return xAxis;
    }

    public int getYAxis() {
        return yAxis;
    }

    public void setXAxis(int xAxis) {
        this.xAxis = xAxis;
    }

    public void setYAxis(int yAxis) {
        this.yAxis = yAxis;
    }

    public void setCenter(Point center) {
        this.center = center;
    }

    public void moveTo(int x, int y) {

        center.moveTo(x, y);
    }

    public void moveTo(Point point) {

        center.moveTo(point.getX(), point.getY());
    }

    public void moveRel(int dx, int dy) {
        center.moveRel(dx, dy);
    }

    
    public void resize(double ratio) {
        yAxis = (int) (ratio * yAxis);
        xAxis = (int) (ratio * xAxis);
    }


    public void stretch(double xRatio, double yRatio) {
        yAxis = (int) (yRatio * yAxis);
        xAxis = (int) (xRatio * xAxis);
    }

    public double getArea() {

        return Math.PI * yAxis / 2 * xAxis / 2;
    }


    public double getPerimeter() {
        return 2 * Math.PI * Math.sqrt((xAxis * xAxis + yAxis * yAxis) / 8.0);
    }


    public boolean isInside(int x, int y) {
        x -= center.getX();
        y -= center.getY();
        return (((x * x) / (xAxis / 2 * xAxis / 2) + (y * y) / (yAxis / 2 * yAxis / 2)) <= 1);
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Ellipse ellipse = (Ellipse) o;

        if (xAxis != ellipse.xAxis) return false;
        if (yAxis != ellipse.yAxis) return false;
        return center.equals(ellipse.center);

    }

    @Override
    public int hashCode() {
        int result = center.hashCode();
        result = 31 * result + xAxis;
        result = 31 * result + yAxis;
        return result;
    }
}
