package net.thumbtack.school.figures.v2;

import net.thumbtack.school.iface.v2.Stretchable;

public class Rectangle extends Figure implements Stretchable{

    private Point LeftTop, RightBottom;
    public Rectangle(Point LeftTop, Point RightBottom){
        this.LeftTop = new Point(LeftTop.getX(), LeftTop.getY());
        this.RightBottom = new Point(RightBottom.getX(), RightBottom.getY());
    }

    public Rectangle(int xLeft,int yTop,int xRight,int yBottom){
        this(new Point(xLeft, yTop), new Point(xRight, yBottom));
    }

    public Rectangle(int lenght, int width){
        this(0, -width, lenght, 0);
    }

    public Rectangle(){
        this(0, -1, 1, 0);
    }

    public Point getTopLeft(){
        return LeftTop;
    }

    public Point getBottomRight(){
        return RightBottom;
    }

    public Point setTopLeft(Point topLeft){
        LeftTop = topLeft;
        return LeftTop;
    }

    public Point setBottomRight(Point bottomRight){
        RightBottom = bottomRight;
        return RightBottom;
    }

    public int getLength(){
        return (RightBottom.getX() - LeftTop.getX());
    }

    public int getWidth(){
        return (RightBottom.getY() - LeftTop.getY());
    }

    public void moveTo(int x, int y){
        int l = getLength();
        int w = getWidth();
        LeftTop.moveTo(x, y);
        RightBottom.moveTo(x + l, y + w);
    }

    public void moveTo(Point point){
        int l = getLength();
        int w = getWidth();
        LeftTop.moveTo(point.getX(), point.getY());
        RightBottom.moveTo(point.getX() + l, point.getY() + w);
    }

    public void moveRel(int dx, int dy){
        LeftTop.moveRel(dx, dy);
        RightBottom.moveRel(dx, dy);
    }

    public void resize(double ratio){
        RightBottom.moveTo((int) (RightBottom.getX() + getLength() * (ratio - 1)), (int) (RightBottom.getY() + getWidth() * (ratio - 1)));
    }

    public void stretch(double xRatio, double yRatio){
        RightBottom.moveTo((int) (RightBottom.getX() + getLength() * (xRatio - 1)), (int) (RightBottom.getY() + getWidth() * (yRatio - 1)));
    }

    public double getArea(){
        return (getLength()*getWidth());
    }

    public double getPerimeter(){
        return(getLength()*2+getWidth()*2);
    }

    public boolean isInside(int x, int y){
        return ((x >= LeftTop.getX()) && (x <= RightBottom.getX()) && (y >= LeftTop.getY()) && (y <= RightBottom.getY()));
    }


    public boolean isIntersects(Rectangle rectangle){
        return !((LeftTop.getX() > rectangle.RightBottom.getX()) || (RightBottom.getX() < rectangle.LeftTop.getX()) || (LeftTop.getY() > rectangle.RightBottom.getY()) || (RightBottom.getY() < rectangle.LeftTop.getY()));
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Rectangle rectangle = (Rectangle) o;

        if (!LeftTop.equals(rectangle.LeftTop)) return false;
        return RightBottom.equals(rectangle.RightBottom);

    }

    @Override
    public int hashCode() {
        int result = LeftTop.hashCode();
        result = 31 * result + RightBottom.hashCode();
        return result;
    }

    public boolean isInside(Rectangle rectangle){
        return (rectangle.LeftTop.getX() >= this.LeftTop.getX()) && (rectangle.RightBottom.getX() <= RightBottom.getX()) &&
                (rectangle.LeftTop.getY() >= this.LeftTop.getY()) && (rectangle.RightBottom.getY() <= this.RightBottom.getY());
    }
}
