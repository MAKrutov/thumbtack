package net.thumbtack.school.base;

import java.text.DecimalFormat;

public class StringOperations {


    public static int getSummaryLength(String[] strings) {
        int sum = 0;
        for (String i : strings)
            sum += i.length();
        return sum;
    }

    public static String getFirstAndLastLetterString(String string) {
        char[] buff = {string.charAt(0), string.charAt(string.length() - 1)};
        return (new String(buff));
    }

    public static boolean isSameCharAtPosition(String string1, String string2, int index) {
        return string1.charAt(index) == string2.charAt(index);
    }//


    public static boolean isSameFirstCharPosition(String string1, String string2, char character) {
        return string1.indexOf(character) == string2.indexOf(character);
    }

    public static boolean isSameLastCharPosition(String string1, String string2, char character) {
        return string1.lastIndexOf(character) == string2.lastIndexOf(character);
    }



    public static boolean isSameFirstStringPosition(String string1, String string2, String str) {
        return string1.indexOf(str) == string2.indexOf(str);
    }


    public static boolean isSameLastStringPosition(String string1, String string2, String str) {
        return string1.lastIndexOf(str) == string2.lastIndexOf(str);
    }

    public static boolean isEqual(String string1, String string2) {
        return string1.equals(string2);
    }


    public static boolean isEqualIgnoreCase(String string1, String string2) {
        return string1.equalsIgnoreCase(string2);
    }

    public static boolean isLess(String string1, String string2) {
        return string1.compareTo(string2) < 0;
    }


    public static boolean isLessIgnoreCase(String string1, String string2) {
        return string1.compareToIgnoreCase(string2) < 0;
    }

    public static String concat(String string1, String string2) {
        return string1.concat(string2);
    }

    public static boolean isSamePrefix(String string1, String string2, String prefix) {
        return (string1.startsWith(prefix) && string2.startsWith(prefix));
    }


    public static boolean isSameSuffix(String string1, String string2, String suffix) {
        return (string1.endsWith(suffix) == string2.endsWith(suffix));
    }


    public static String getCommonPrefix(String string1, String string2) {
        int i = 0;
        for (i = 0; (i < string1.length() && i < string2.length()); i++) {
            if (string1.charAt(i) != string2.charAt(i)) break;
        }
        return string1.substring(0, i);
    }


    public static String reverse(String string) {
        return new StringBuilder(string).reverse().toString();
    }

    public static boolean isPalindrome(String string) {
        return reverse(string).equals(string);
    }

    
    public static boolean isPalindromeIgnoreCase(String string) {
        String temp = reverse(string.toLowerCase());
        return temp.equals(string.toLowerCase());
    }


    public static String getLongestPalindromeIgnoreCase(String[] strings) {
        int maxLen = 0, index = 0;
        for (int i = 0; i < strings.length; i++) {
            if (isPalindromeIgnoreCase(strings[i]) && (maxLen < strings[i].length())) {
                maxLen = strings[i].length();
                index = i;
            }
        }
        if (maxLen == 0) return "";
        else return strings[index];
    }

    public static boolean hasSameSubstring(String string1, String string2, int index, int length) {
        if (string1.length() >= length + index && string2.length() >= length + index)
            return (string1.substring(index, length).equals(string2.substring(index, length)));
        else return false;
    }

    //
    public static boolean isEqualAfterReplaceCharacters(String string1, char replaceInStr1, char replaceByInStr1, String string2, char replaceInStr2, char replaceByInStr2) {
        return isEqual(string1.replace(replaceInStr1, replaceByInStr1), string2.replace(replaceInStr2, replaceByInStr2));
    }

    //
    public static boolean isEqualAfterReplaceStrings(String string1, String replaceInStr1, String replaceByInStr1, String string2, String replaceInStr2, String replaceByInStr2) {
        return isEqual(string1.replaceAll(replaceInStr1, replaceByInStr1), string2.replaceAll(replaceInStr2, replaceByInStr2));
    }

    //
    public static boolean isPalindromeAfterRemovingSpacesIgnoreCase(String string) {
        return isPalindromeIgnoreCase(string.replace(" ", ""));
    }

    //
    public static boolean isEqualAfterTrimming(String string1, String string2) {
        return isEqual(string1.trim(), string2.trim());
    }

    public static String makeCsvStringFromInts(int[] array) {
        String string = "";
        for (int i : array)
            string += i + " ";

        string = string.trim();

        return string.replaceAll("\\s", ",");
    }

    //
    public static String makeCsvStringFromDoubles(double[] array) {
        String string = "";
        for (double i : array)
            string += new DecimalFormat("#0.00").format(i) + " ";

        string = string.trim();

        return string.replaceAll("\\s", ",");
    }

    public static StringBuilder makeCsvStringBuilderFromInts(int[] array) {
        return new StringBuilder(makeCsvStringFromInts(array));
    }

    
    public static StringBuilder makeCsvStringBuilderFromDoubles(double[] array) {
        return new StringBuilder(makeCsvStringFromDoubles(array));
    }


    public static StringBuilder removeCharacters(String string, int[] positions) {
        char[] buff = string.toCharArray();
        for (int i : positions)
            buff[i] = ' ';

        String res = new StringBuilder(" ").append(buff).toString().replaceAll(" ", "");

        return new StringBuilder(res);
    }

    public static StringBuilder insertCharacters(String string, int[] positions, char[] characters) {
        int j = 0;
        StringBuilder res = new StringBuilder(string);
        for (int i : positions) {
            res.insert(i + j, characters[j]);
            j++;
        }
        return res;
    }
}
