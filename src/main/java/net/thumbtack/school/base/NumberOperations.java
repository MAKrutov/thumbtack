package net.thumbtack.school.base;

import java.math.BigDecimal;
import java.math.BigInteger;

public class NumberOperations {

    public static Integer find(int[] array, int value) {
        for (int i = 0; i < array.length; i++) {
            if (array[i] == value) return i;
        }
        return null;
    }

    public static Integer find(double[] array, double value, double eps) {
        for (int i = 0; i < array.length; i++) {
            if (Math.abs(Math.abs(array[i]) - Math.abs(value)) < eps) return i;
        }
        return null;
    }

    public static Double calculateDensity(double weight, double volume, double min, double max) {
        Double p = weight / volume;
        if ((Double.max(p, max) == p) || Double.min(p, min) == p)
            return null;
        else return p;
    }

    public static Integer find(BigInteger[] array, BigInteger value) {
        for (int i = 0; i < array.length; i++)
            if (value.equals(array[i])) return i;

        return null;
    }

    //
    public static BigDecimal calculateDensity(BigDecimal weight, BigDecimal volume, BigDecimal min, BigDecimal max) {
        BigDecimal bd = weight.divide(volume);
        if ((bd.compareTo(max) > 0) || (bd.compareTo(min) < 0))
            return null;
        else return bd;
    }

}
