package net.thumbtack.school.matrix;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class MatrixNonSimilarRows {

    private int[][] matrix = null;

    public MatrixNonSimilarRows(int[][] matrix){
        this.matrix = matrix;
    }

    public Set<int[]> getNonSimilarRows(){
        Set<int[]> integerSet =new HashSet<>();
        Map<Set<Integer>,Integer> map = new HashMap<>();

        for(int i = 0; i < matrix.length; i++) {
            Set<Integer> set1 = new HashSet<>();

            for (int j : matrix[i])
                set1.add(j);

            if(!map.containsKey(set1))
                    map.put(new HashSet<>(set1),i);

        }
        for (Integer i: map.values()) {
            integerSet.add(matrix[i]);
        }
        return integerSet;
    }
}

