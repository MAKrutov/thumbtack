package net.thumbtack.school.file;

import com.google.gson.Gson;
import net.thumbtack.school.colors.v3.ColorException;
import net.thumbtack.school.figures.v3.ColoredRectangle;
import net.thumbtack.school.figures.v3.Rectangle;
import net.thumbtack.school.ttschool.Trainee;
import net.thumbtack.school.ttschool.TrainingException;

import java.io.*;
import java.nio.charset.StandardCharsets;

public class FileService {

    public static void writeByteArrayToBinaryFile(String fileName, byte[] array) throws IOException {
        try (BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream(new File(fileName)))) {
            bos.write(array);
        }

    }

    public static void writeByteArrayToBinaryFile(File file, byte[] array) throws IOException {
        writeByteArrayToBinaryFile(file.getAbsolutePath(), array);
    }

    public static byte[] readByteArrayFromBinaryFile(String fileName) throws IOException {
        byte[] array = null;
        try (BufferedInputStream bis = new BufferedInputStream(new FileInputStream(fileName))) {
            array = new byte[bis.available()];
            bis.read(array);
        }
        return array;
    }

    public static byte[] readByteArrayFromBinaryFile(File file) throws IOException {
        return readByteArrayFromBinaryFile(file.getAbsolutePath());
    }


    public static byte[] writeAndReadByteArrayUsingByteStream(byte[] array) throws IOException {

        byte[] resultArray = null;

        try (ByteArrayOutputStream baos = new ByteArrayOutputStream(array.length)) {
            baos.write(array);

            ByteArrayInputStream bais = new ByteArrayInputStream(baos.toByteArray());
            resultArray = new byte[baos.toByteArray().length / 2];
            for (int i = 0; i < resultArray.length; i++) {
                bais.read(resultArray, i, 1);
                bais.skip(1);
            }
        }

        return resultArray;
    }

    public static void writeByteArrayToBinaryFileBuffered(String fileName, byte[] array) throws IOException {
        writeByteArrayToBinaryFile(fileName, array);
    }

    public static void writeByteArrayToBinaryFileBuffered(File file, byte[] array) throws IOException {
        writeByteArrayToBinaryFileBuffered(file.getAbsolutePath(), array);
    }

    public static byte[] readByteArrayFromBinaryFileBuffered(String fileName) throws IOException {
        return readByteArrayFromBinaryFile(fileName);
    }

    public static byte[] readByteArrayFromBinaryFileBuffered(File file) throws IOException {
        return readByteArrayFromBinaryFileBuffered(file.getAbsolutePath());
    }

    public static void writeRectangleToBinaryFile(File file, Rectangle rect) throws IOException {
        try (DataOutputStream dos = new DataOutputStream(new FileOutputStream(file))) {
            dos.writeInt(rect.getTopLeft().getX());
            dos.writeInt(rect.getTopLeft().getY());
            dos.writeInt(rect.getBottomRight().getX());
            dos.writeInt(rect.getBottomRight().getY());
        }
    }


    public static Rectangle readRectangleFromBinaryFile(File file) throws IOException {
        Rectangle rect = null;
        try (DataInputStream dis = new DataInputStream(new FileInputStream(file))) {
            rect = new Rectangle(dis.readInt(), dis.readInt(), dis.readInt(), dis.readInt());
        }
        return rect;
    }

    public static void writeColoredRectangleToBinaryFile(File file, ColoredRectangle rect) throws IOException {
        try (DataOutputStream dos = new DataOutputStream(new FileOutputStream(file))) {
            dos.writeInt(rect.getTopLeft().getX());
            dos.writeInt(rect.getTopLeft().getY());
            dos.writeInt(rect.getBottomRight().getX());
            dos.writeInt(rect.getBottomRight().getY());
            dos.writeUTF(rect.getColor().toString());
        }
    }

    public static ColoredRectangle readColoredRectangleFromBinaryFile(File file) throws ColorException, IOException {
        ColoredRectangle colRect = null;
        try (DataInputStream dis = new DataInputStream(new FileInputStream(file))) {
            colRect = new ColoredRectangle(dis.readInt(), dis.readInt(), dis.readInt(), dis.readInt(), dis.readUTF());
        }

        return colRect;
    }

    public static void writeRectangleArrayToBinaryFile(File file, Rectangle[] rects) throws IOException {
        try (DataOutputStream dos = new DataOutputStream(new FileOutputStream(file))) {
            for (Rectangle r : rects) {
                dos.writeInt(r.getTopLeft().getX());
                dos.writeInt(r.getTopLeft().getY());
                dos.writeInt(r.getBottomRight().getX());
                dos.writeInt(r.getBottomRight().getY());
            }
        }

    }


    public static Rectangle[] readRectangleArrayFromBinaryFileReverse(File file) throws IOException {
        Rectangle[] rect = new Rectangle[(int) file.length() / 16];

        try (RandomAccessFile ras = new RandomAccessFile(file, "rw")) {
            for (int i = 0; i < rect.length; i++) {
                ras.seek(rect.length * 16 - 16 * (i + 1));
                rect[i] = new Rectangle(ras.readInt(), ras.readInt(), ras.readInt(), ras.readInt());
            }
        }
        return rect;
    }

    public static void writeRectangleToTextFileOneLine(File file, Rectangle rect) throws IOException {
        try (RandomAccessFile raf = new RandomAccessFile(file, "rw")) {
            raf.writeUTF((rect.getTopLeft().getX()) + " ");
            raf.writeUTF((rect.getTopLeft().getY()) + " ");
            raf.writeUTF((rect.getBottomRight().getX()) + " ");
            raf.writeUTF((rect.getBottomRight().getY()) + " ");
        }

    }


    public static Rectangle readRectangleFromTextFileOneLine(File file) throws IOException {
        Rectangle rect = null;
        try (RandomAccessFile raf = new RandomAccessFile(file, "rw")) {
            rect = new Rectangle(Integer.valueOf(raf.readUTF().replace(" ", "")), Integer.valueOf(raf.readUTF().replace(" ", "")), Integer.valueOf(raf.readUTF().replace(" ", "")), Integer.valueOf(raf.readUTF().replace(" ", "")));
        }
        return rect;
    }

    public static void writeRectangleToTextFileFourLines(File file, Rectangle rect) throws IOException {
        try (BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file)))) {
            bw.write(Integer.toString(rect.getTopLeft().getX()));
            bw.newLine();
            bw.write(Integer.toString(rect.getTopLeft().getY()));
            bw.newLine();
            bw.write(Integer.toString(rect.getBottomRight().getX()));
            bw.newLine();
            bw.write(Integer.toString(rect.getBottomRight().getY()));
        }
    }


    public static Rectangle readRectangleFromTextFileFourLines(File file) throws IOException {
        Rectangle rect = null;
        try (BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(file)))) {
            rect = new Rectangle(Integer.parseInt(br.readLine()), Integer.parseInt(br.readLine()), Integer.parseInt(br.readLine()), Integer.parseInt(br.readLine()));
        }
        return rect;
    }

    public static void writeTraineeToTextFileOneLine(File file, Trainee trainee) throws IOException {

        try (BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file), StandardCharsets.UTF_8))) {
            bw.write(trainee.getFirstName() + " ");
            bw.write(trainee.getLastName() + " ");
            bw.write(trainee.getRating());
        }
    }


    public static Trainee readTraineeFromTextFileOneLine(File file) throws IOException, TrainingException {
        Trainee trainee = null;
        try (BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(file), StandardCharsets.UTF_8))) {
            String[] s = br.readLine().split(" ");
            trainee = new Trainee(s[0], s[1], s[2].codePointAt(0));
        }
        return trainee;
    }

    public static void writeTraineeToTextFileThreeLines(File file, Trainee trainee) throws IOException {
        try (BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file), StandardCharsets.UTF_8))) {
            bw.write(trainee.getFirstName());
            bw.newLine();
            bw.write(trainee.getLastName());
            bw.newLine();
            bw.write(trainee.getRating());
        }
    }

    public static Trainee readTraineeFromTextFileThreeLines(File file) throws IOException, TrainingException {
        Trainee trainee = null;
        try (BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(file), StandardCharsets.UTF_8))) {
            trainee = new Trainee(br.readLine(), br.readLine(), br.readLine().codePointAt(0));
        }
        return trainee;
    }

    public static void serializeTraineeToBinaryFile(File file, Trainee trainee) throws IOException {
        try (ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(file))) {
            oos.writeObject(trainee);
        }
    }

    public static Trainee deserializeTraineeFromBinaryFile(File file) throws IOException, ClassNotFoundException {
        Trainee trainee = null;
        try (ObjectInputStream ois = new ObjectInputStream(new FileInputStream(file))) {
            trainee = (Trainee) ois.readObject();
        }
        return trainee;
    }

    public static String serializeTraineeToJsonString(Trainee trainee) {
        Gson gson = new Gson();
        return gson.toJson(trainee);
    }

    public static Trainee deserializeTraineeFromJsonString(String json) {
        Gson gson = new Gson();
        return gson.fromJson(json, Trainee.class);
    }

    public static void serializeTraineeToJsonFile(File file, Trainee trainee) throws IOException {
        String json = serializeTraineeToJsonString(trainee);
        try (DataOutputStream dos = new DataOutputStream(new FileOutputStream(file))) {
            dos.writeUTF(json);
        }
    }

    public static Trainee deserializeTraineeFromJsonFile(File file) throws IOException {
        Trainee trainee = null;
        try (DataInputStream dis = new DataInputStream(new FileInputStream(file))) {
            trainee = deserializeTraineeFromJsonString(dis.readUTF());
        }
        return trainee;
    }
}
