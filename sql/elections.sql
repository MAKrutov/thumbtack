drop database IF EXISTS elections;
create DATABASE elections;
USE elections;

create TABLE person (
id INT(50) NOT NULL AUTO_INCREMENT,
firstName varchar(20) not null,
middleName varchar(20) not null,
lastName varchar(20) not null,
login varchar(20) not null,
`password` varchar(20) not null,
adress varchar(20) not null,
home varchar(20) not null,
apartment varchar(20) default null,
isCandidate boolean,
unique key (firstName,
			middleName,
            LastName,
            adress,
            home,
            apartment),
unique key (login),
unique key (`password`),
PRIMARY KEY (id)
) ENGINE=INNODB DEFAULT CHARSET=utf8;

create table candidateOffer(
id int(20) not null auto_increment,
candidateID int(20) not null,
offerID int(20) not null,
foreign key (candidateID) references person(id) on delete cascade,
PRIMARY KEY (id),
KEY/*INDEX*/ candidateID(candidateID),
KEY/*INDEX*/ offerID (offerID)
)ENGINE=INNODB DEFAULT CHARSET=utf8;

create table personUUID(
id int(20) not null auto_increment,
personID int(20) not null,
UUID varchar(50) default null,
foreign key (personID) references person(id) on delete cascade,
PRIMARY KEY (id),
KEY/*INDEX*/ UUID (UUID),
KEY/*INDEX*/ personID (personID)
)ENGINE=INNODB DEFAULT CHARSET=utf8;

create table candidateID(
id int(20) not null auto_increment,
candidateID int(20) not null,
foreign key (candidateID) references person(id) on delete cascade,
PRIMARY KEY (id),
KEY/*INDEX*/ candidateID (candidateID)
)ENGINE=INNODB DEFAULT CHARSET=utf8;


create table offer(
id INT(50) NOT NULL AUTO_INCREMENT,
authorID int(20) default NULL,
offerString varchar(100) not null,
unique key (offerString),
foreign key (authorID) references person(id) on delete cascade,
PRIMARY KEY (id),
KEY/*INDEX*/ authorID (authorID)
)ENGINE=INNODB DEFAULT CHARSET=utf8;

create table offerRate(
id Int(20) not null auto_increment,
offerID int(20) not null,
rate int(20) default 5,
raterID int(20) not null,
unique key (raterID,offerID),
foreign key (raterID) references person(id) on delete cascade,
PRIMARY KEY (id),
KEY/*INDEX*/ rate (rate)
)ENGINE=INNODB DEFAULT CHARSET=utf8;



create table voterBag(
id int(20) not null auto_increment,
voterID int(20) not null,
candidateID int(20) not null,
foreign key (voterID) references person(id) on delete cascade,
unique key(voterID),
PRIMARY KEY (id)
)ENGINE=INNODB DEFAULT CHARSET=utf8