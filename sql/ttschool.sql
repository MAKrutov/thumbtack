DROP DATABASE IF EXISTS ttschool;
create DATABASE ttschool;
USE ttschool;


create TABLE school (
id INT(50) NOT NULL AUTO_INCREMENT,
year INT NOT NULL,
name varchar(50) NOT NULL,
PRIMARY KEY (id),
unique key name_year(name,year),
KEY/*INDEX*/ name (name),
KEY/*INDEX*/ year (year)
) ENGINE=INNODB DEFAULT CHARSET=utf8;


create TABLE `group` (
id INT(50) NOT NULL AUTO_INCREMENT,
name varchar(50) NOT NULL,
room varchar(50) NOT NULL,
schoolid INT(50) not null,
PRIMARY KEY (`id`),
KEY/*INDEX*/ name (name),
KEY/*INDEX*/ room (room),
foreign key (schoolid) references school(id) on delete cascade
) ENGINE=INNODB DEFAULT CHARSET=utf8;

create TABLE trainee (
id INT(50) NOT NULL  AUTO_INCREMENT,
firstName varchar(50)NOT NULL,
lastName varchar(50) NOT NULL,
rating INT(5) NOT NULL,
groupID INT(50) default null,
PRIMARY KEY (id),
constraint trainee_group foreign key (groupID) references `group`(id) 
			on delete SET null
			on update cascade,
KEY/*INDEX*/ firstName (firstName),
KEY/*INDEX*/ lastName (lastName),
KEY/*INDEX*/ rating (rating)
) ENGINE=INNODB DEFAULT CHARSET=utf8;

create TABLE `subject` (
id INT(50)  NOT NULL AUTO_INCREMENT,
name varchar(50)NOT NULL,
PRIMARY KEY (id),
unique key/*INDEX*/ name (name)
) ENGINE=INNODB DEFAULT CHARSET=utf8;


create TABLE subject_group(
id int not null AUTO_INCREMENT,
subjectID int not null,
groupID int not null,
name varchar(100) not null,
unique key name_group(name,groupID),
PRIMARY KEY (id),
foreign key (groupID) references `group`(id) on delete cascade,
foreign key (subjectID) references `subject`(id) on delete cascade
)ENGINE=INNODB DEFAULT CHARSET=utf8;



